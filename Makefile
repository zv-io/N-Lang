################################################################
# README (for Makefile)
#
# docker
# ------
#
# This Makefile supports several interesting use-cases. First is
# the portable "native" mode, where users call 'make TARGET' the
# usual way. All available TARGETs may be Dockerized. Simply use
# the following syntax, to enter "docker" mode:
#
#   $ make       TARGET         <-- traditional usage
#
#   $ make image-TARGET         <-- Dockerized  usage
#
# By default, it is not possible (nor desired) to be able to use
# Docker recursively. There are several safety checks for this.
#
# Several similar 'Dockerfile' files reside in the project tree;
# these are suffixed as follows: 'Dockerfile.DOCK' where DOCK is
# a special identifier that typically aligns with the base image
# used (e.g., Alpine Linux, Ubuntu, etc.). These images are ONLY
# used for building N, and do not correspond to N releases.
#
# To build the "default" (configurable) Docker image, run this:
#
#   $ make image
#
# Alternatively, a different Dockerfile may be specified:
#
#   $ make image DOCK=ubuntu    # uses 'Dockerfile.ubuntu' file
#
#
# version
# -------
#
# N language and compiler version information will automatically
# populate by the release script(s), and is stored in 'VERSION'.
# If this file does not exist, a default of '0(.0)*' is assumed.
#
# When Git versioning information is available, i.e., the source
# is being built in a valid Git repository and the 'git' command
# is available, the current commit hash will be used instead.
#
#
# safety
# ------
#
# In an effort to ensure that N builds consistently and properly
# on as many systems as possible, we adhere strictly to certain
# standards, include numerous sanity checks, among others. Until
# N is formally released, we cannot provide such guarantees. If
# you encounter any errors, warnings, odd behavior, or otherwise
# have difficulties in building and/or using N, contact us.
#
#
# important
# ---------
#
# Please refer to the 'README' file and documentation for more
# information. The above notes pertain only to this Makefile.
#
################################################################

.PHONY: all clean

#---------------------------------------------------------------
# defaults

				# default N Dockerfile.${DOCK}
DOCK ?= alpine

#---------------------------------------------------------------
# directories

# While N should be built in-tree (the only supported method) we
# operate (as scripts, code, etc.) in a strictly relative manner
# to this Makefile. The following variable facilitates this, for
# calling 'make -C /some/other/path/to/N-Lang' should also work.

				# absolute path to project root
HERE  = $(CURDIR)

# These variables are used internally by the N build system and
# should not require modification during normal usage. Sorted in
# the approximate order required for building N.

				# N-Lang project directories
export UDIR := ${HERE}/scripts
export IDIR := ${HERE}/include
export SDIR := ${HERE}/src
export LDIR := ${HERE}/lib
export BDIR := ${HERE}/bin
export TDIR := ${HERE}/test
export DDIR := ${HERE}/docs
export EDIR := ${HERE}/examples

				# extract N version information
NVER  = $(strip $(shell cat ${HERE}/VERSION))
				# otherwise provide defaults
ifeq (${NVER},)
NVER  = 0.0.0
endif

# FIXME: Should not hard-code paths into the binary, especially
# if only one path is supported (not a search path). Therefore,
# should N compiler use project's libn or the installed one when
# we cannot guarantee that it's been installed?

export NDIR := ${HERE}/libn

#---------------------------------------------------------------
# system

# This target will check whether a required tool can be found in
# the current user's PATH, and error if missing. Targets needing
# certain tools (e.g., 'docker') should add these dependencies.
# If multiple tools are needed, use $(patsubst %,need-%,a b c).

need-%:
	@if [ ! -e "$$(command -v $(@:need-%=%))" ]; then      \
		printf "ERROR: missing '%s'\n" $(@:need-%=%);  \
		exit 1;                                        \
	else                                                   \
		printf "%s\n" "$$($(@:need-%=%) --version)";   \
	fi

# Ensure that the C compiler is specified. If so, then check its
# existence. N aims to be compiler-agnostic.

ifeq (${CC},)
$(error No C compiler was specified; try 'CC=<tool> make')
else
# Flags for the N-generated C code bootstrap process
NC99  = ${CC} -std=c99 -pedantic -Wall -Wextra
NC99 += -O2 -static -fPIC
export NC99
endif

# Include directories

CFLAGS += -I${IDIR}

# Default C compiler flags

CFLAGS += -std=gnu11 -pedantic -Wall -Wextra
CFLAGS += -O2
CFLAGS += -static -fPIC

CFLAGS += -DNC_VERSION_STR=\"${NVER}\"
CFLAGS += -DN_LANG_SPEC_STR=\"${NVER}\"

# Default linker flags

LDFLAGS += -L${LDIR}

# This section determines system and compiler architectures for
# the purposes of cross-compilation and native optimization. If
# you'd like to tune to a particular CPU, this is the place.

				# compiler target machine
ARCH ?= $(shell $(CC) -dumpmachine)
				# build system architecture
MACH ?= $(shell uname -m)

ifneq ($(strip\
	$(findstring  x86,${MACH})\
	$(findstring m68k,${MACH})\
),)
EXTRAS += -march=native
else
EXTRAS += -mcpu=native
endif

# Export these variables so they are accessible in child files.

export CFLAGS LDFLAGS EXTRAS

#---------------------------------------------------------------
# default targets

				# default target
all: help
all_:

help:
	@printf \
"===========================================================\n"\
"  Welcome to N-Lang ${NVER}.         https://your-site.org/\n"\
"===========================================================\n"\
"                                                           \n"\
"Targets (type 'make TARGET' or 'make image-TARGET'):       \n"\
"                                                           \n"\
"  * lang       Build the N compiler, libraries, and tools  \n"\
"  * test       Test  the N compiler, libraries, and tools  \n"\
"  * docs       Generate up-to-date documentation to match  \n"\
"                                                           \n"\
"  * all        Build all of the above targets              \n"\
"  * clean      Clean all of the above targets              \n"\
"                                                           \n"\
"Docker-related:                                            \n"\
"                                                           \n"\
"  * image      Build the default Docker image (${DOCK})    \n"\
"  * image DOCK=foo Build the foo Docker image              \n"\
"  * image-KALE Execute target KALE inside of Docker image  \n"\
"                                                           \n"\
"Advanced usage:                                            \n"\
"                                                           \n"\
"  * TARGET-ACT Execute action ACT on target TARGET         \n"\
"                                                           \n"\
"===========================================================\n"\
;
help_:

#---------------------------------------------------------------
# docker

# The following variables govern the creation and tagging of all
# Docker images that may accompany N. By default, containers run
# a non-privileged shell but default user has 'sudo' privileges.
# Docker images are tagged according to variable NVER.

				# default Docker image username
USER := user
				# default Docker image repo
REPO := nlang
				# default Docker volume mapping
ZDIR ?= /app

# All targets within this Makefile, except those which may cause
# recursive loops, may be executed within an N Docker image. To
# use this functionality, simply use 'image-TARGET' where TARGET
# is the desired Makefile target, e.g., 'make image-tests'.

image-%: need-docker
	@docker run --rm -it \
		-v "${HERE}:${ZDIR}" \
		-w "${ZDIR}" \
		${REPO}/${DOCK} `# default 'latest'` \
		$(MAKE) $(@:image-%=%) $(MAKEFLAGS)

# It may be of interest to have the ability to enter an image in
# case something goes wrong, so we can drop you into a shell.

enter: need-docker
	@docker run --rm -it \
		-v "${HERE}:${ZDIR}" \
		-w "${ZDIR}" \
		${REPO}/${DOCK} `# default 'latest'`

# This target builds the default Docker image (derived by recipe
# contained within 'Dockerfile.DOCK'), otherwise variable DOCK
# may be overridden (e.g., 'make image DOCK=ubuntu') if desired.

image: Dockerfile.${DOCK}
	@docker build . -f $< \
		--build-arg USER=${USER} \
		-t ${REPO}/${DOCK}:${NVER}

#---------------------------------------------------------------
# release

# Cutting releases, per the design of this Makefile, refers just
# to the cleaning, checking, building, testing, packaging, etc.
# of the N source code, and _not_ the production or uploading of
# tarballs, packages, and/or Docker images.

release:
	@docker tag ${REPO}/${DOCK}:${NVER} ${REPO}/${DOCK}:latest

#---------------------------------------------------------------
# pre-build checks

script-%: ${UDIR}/%.sh
	@chmod +x $< && $<
script:
	@$(error Usage: make ${@}-TARGET)

#---------------------------------------------------------------
# N

.PHONY: lang
lang:   ${SDIR}
	@${MAKE} --no-print-directory -C $< all
lang-%: ${SDIR}
	@${MAKE} --no-print-directory -C $< $(@:lang-%=%)

#---------------------------------------------------------------
# libn

.PHONY: libn
libn:   ${NDIR}
	@${MAKE} --no-print-directory -C $< all
libn-%: ${NDIR}
	@${MAKE} --no-print-directory -C $< $(@:libn-%=%)

#---------------------------------------------------------------
# tests

.PHONY: test
test:   ${TDIR}
	@${MAKE} --no-print-directory -C $< all
test-%: ${TDIR}
	@${MAKE} --no-print-directory -C $< $(@:test-%=%)

#---------------------------------------------------------------
# documentation

.PHONY: docs
docs:   ${DDIR}
	@${MAKE} --no-print-directory -C $<
docs-%: ${DDIR}
	@${MAKE} --no-print-directory -C $< $(@:docs-%=%)

#---------------------------------------------------------------
# clean

clean: $(patsubst %,%-clean,lang libn test docs)
	@# FIXME: this should be cleaned up
