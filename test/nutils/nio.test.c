/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <EMU/EMUtest.h>
#include <nutils/nio.h>

// These log tests mostly just make sure we can write without crashing. Ouput is
// written to a log file.
EMU_TEST(nlogf)
{
    FILE* log_fp = fopen("build/test.log", "w+");
    EMU_ASSERT_NOT_NULL(log_fp);
    log_stream = &log_fp; // Set log_stream for testing.

    log_config = LOG_DEBUG | LOG_METRIC | LOG_ERROR | LOG_WARNING | LOG_ERROR | LOG_FATAL;
    int foo = 0;
    nlogf(LOG_DEBUG,   "This is a message created with LOG_DEBUG foo = %d", foo++);
    nlogf(LOG_METRIC,  "This is a message created with LOG_METRIC foo = %d", foo++);
    nlogf(LOG_INFO,    "This is a message created with LOG_INFO foo = %d", foo++);
    nlogf(LOG_WARNING, "This is a message created with LOG_WARNING foo = %d", foo++);
    nlogf(LOG_ERROR,   "This is a message created with LOG_ERROR foo = %d", foo++);
    nlogf(LOG_FATAL,   "This is a message created with LOG_FATAL foo = %d", foo++);
    nlogf(9999, "Message logged with an unknown log level"); // unknown garbage log level

    log_config = LOG_DEBUG | LOG_ERROR | LOG_FATAL;
    int bar = 0;
    nlogf(LOG_DEBUG,   "This is a message created with LOG_DEBUG bar = %d", bar++);
    nlogf(LOG_METRIC,  "This is a message created with LOG_METRIC bar = %d", bar++);
    nlogf(LOG_INFO,    "This is a message created with LOG_INFO bar = %d", bar++);
    nlogf(LOG_WARNING, "This is a message created with LOG_WARNING bar = %d", bar++);
    nlogf(LOG_ERROR,   "This is a message created with LOG_ERROR bar = %d", bar++);
    nlogf(LOG_FATAL,   "This is a message created with LOG_FATAL bar = %d", bar++);
    nlogf(9999, "Message logged with an unknown log level"); // unknown garbage log level

    fclose(log_fp);
    log_config = DEFAULT_LOG_CONFIG; // Restore log configuration.
    log_stream = DEFAULT_LOG_STREAM; // Restore log stream.
    EMU_END_TEST();
}

EMU_TEST(nlogf_flc)
{
    FILE* log_fp = fopen("build/test.log", "a");
    EMU_ASSERT_NOT_NULL(log_fp);
    log_stream = &log_fp; // Set log_stream for testing.

    char const* test_src = "test_src_file.c";
    size_t test_line = 42;
    size_t test_col = 101;

    log_config = LOG_DEBUG | LOG_ERROR | LOG_WARNING | LOG_ERROR | LOG_FATAL;
    int foo = 0;
    nlogf_flc(LOG_DEBUG,   test_src, test_line, test_col, "This is a message created with LOG_DEBUG foo = %d", foo++);
    nlogf_flc(LOG_INFO,    test_src, test_line, test_col, "This is a message created with LOG_INFO foo = %d", foo++);
    nlogf_flc(LOG_WARNING, test_src, test_line, test_col, "This is a message created with LOG_WARNING foo = %d", foo++);
    nlogf_flc(LOG_ERROR,   test_src, test_line, test_col, "This is a message created with LOG_ERROR foo = %d", foo++);
    nlogf_flc(LOG_FATAL,   test_src, test_line, test_col, "This is a message created with LOG_FATAL foo = %d", foo++);
    nlogf_flc(9999, test_src, test_line, test_col, "Message logged with an unknown log level"); // unknown garbage log level

    log_config = LOG_DEBUG | LOG_ERROR | LOG_FATAL;
    int bar = 0;
    nlogf_flc(LOG_DEBUG,   test_src, test_line, test_col, "This is a message created with LOG_DEBUG bar = %d", bar++);
    nlogf_flc(LOG_INFO,    test_src, test_line, test_col, "This is a message created with LOG_INFO bar = %d", bar++);
    nlogf_flc(LOG_WARNING, test_src, test_line, test_col, "This is a message created with LOG_WARNING bar = %d", bar++);
    nlogf_flc(LOG_ERROR,   test_src, test_line, test_col, "This is a message created with LOG_ERROR bar = %d", bar++);
    nlogf_flc(LOG_FATAL,   test_src, test_line, test_col, "This is a message created with LOG_FATAL bar = %d", bar++);
    nlogf_flc(9999, test_src, test_line, test_col, "Message logged with an unknown log level"); // unknown garbage log level

    fclose(log_fp);
    log_config = DEFAULT_LOG_CONFIG; // Restore log configuration.
    log_stream = DEFAULT_LOG_STREAM; // Restore log stream.
    EMU_END_TEST();
}

#define file_0_char           "./test/nutils/resources/file_0_char"
#define file_8_char           "./test/nutils/resources/file_8_char"
#define file_18_char          "./test/nutils/resources/file_18_char"
#define file_DOS_line_endings "./test/nutils/resources/file_DOS_line_endings" // 8 chars
#define existing_file         "./test/nutils/resources/existing_file"
#define nonexisting_file      "not_an_actual_file"

EMU_TEST(file_exist)
{

    EMU_EXPECT_TRUE(file_exist(existing_file));
    EMU_EXPECT_FALSE(file_exist(nonexisting_file));
    EMU_END_TEST();
}

EMU_TEST(file_read_into_buff)
{
    size_t const small_sz = 3;
    size_t const large_sz = 128;
    char buff_small[small_sz];
    char buff_large[large_sz];

    EMU_ASSERT_EQ_INT(file_read_into_buff(file_0_char, buff_small, small_sz), 0);
    EMU_EXPECT_STREQ(buff_small, "");
    EMU_ASSERT_EQ_INT(file_read_into_buff(file_0_char, buff_large, large_sz), 0);
    EMU_EXPECT_STREQ(buff_large, "");

    EMU_EXPECT_EQ_INT(file_read_into_buff(file_8_char, buff_small, small_sz), -1);
    EMU_ASSERT_EQ_INT(file_read_into_buff(file_8_char, buff_large, large_sz), 8);
    EMU_EXPECT_STREQ(buff_large, "12345678");

    EMU_ASSERT_EQ_INT(file_read_into_buff(file_DOS_line_endings, buff_large, large_sz), 8);
    EMU_ASSERT_EQ(strcmp(buff_large, "123456\r\n"), 0);
    EMU_EXPECT_STREQ(buff_large, "123456\r\n");

    EMU_EXPECT_EQ_INT(file_read_into_buff(nonexisting_file, buff_small, small_sz), -1);
    EMU_EXPECT_EQ_INT(file_read_into_buff(nonexisting_file, buff_large, large_sz), -1);
    EMU_END_TEST();
}

EMU_TEST(file_read_into_nstr)
{
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_0_char, &nstr), 0);
        EMU_EXPECT_STREQ(nstr.data, "");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_8_char, &nstr), 8);
        EMU_EXPECT_STREQ(nstr.data, "12345678");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_ASSERT_EQ_INT(file_read_into_nstr(file_DOS_line_endings, &nstr), 8);
        EMU_EXPECT_STREQ(nstr.data, "123456\r\n");
    }
    {
        nstr_t nstr_defer_fini nstr;
        nstr_init(&nstr);
        EMU_EXPECT_EQ_INT(file_read_into_nstr(nonexisting_file, &nstr), -1);
    }
    EMU_END_TEST();
}

EMU_TEST(file_size)
{

    EMU_EXPECT_EQ_INT(file_size(file_0_char), 0);
    EMU_EXPECT_EQ_INT(file_size(file_8_char), 8);
    EMU_EXPECT_EQ_INT(file_size(file_18_char), 18);

    // CR LF should not be converted to '\n' for file_size on windows so the
    // size should always be 8
    EMU_EXPECT_EQ_INT(file_size(file_DOS_line_endings), 8);

    EMU_EXPECT_EQ_INT(file_size(nonexisting_file), -1);
    EMU_END_TEST();
}

EMU_TEST(fmt_len)
{
    EMU_EXPECT_EQ_INT(fmt_len(""), 0);
    EMU_EXPECT_EQ_INT(fmt_len("foo"), 3);
    EMU_EXPECT_EQ_INT(fmt_len("foo%s", "bar"), 6);
    EMU_EXPECT_EQ_INT(
        fmt_len("Hello,%d%s%zu\n", 10, "world!", 999),
        strlen("Hello,") + 2 + strlen("world!") + 3 + 1
    );
    EMU_END_TEST();
}

EMU_GROUP(nio_h)
{
    EMU_ADD(nlogf);
    EMU_ADD(nlogf_flc);
    EMU_ADD(file_exist);
    EMU_ADD(file_read_into_buff);
    EMU_ADD(file_read_into_nstr);
    EMU_ADD(file_size);
    EMU_ADD(fmt_len);
    EMU_END_GROUP();
}
