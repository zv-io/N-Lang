/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <EMU/EMUtest.h>
#include <nutils/nstring.h>

EMU_TEST(cstr__cstr_cmp)
{
    EMU_EXPECT_EQ_INT(cstr_cmp("some str", "some str"), 0);
    EMU_EXPECT_NE_INT(cstr_cmp("some str", "SOME STR"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp("some str", "another str"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp("another str", "some str"), 0);
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cmp)
{
    EMU_EXPECT_EQ_INT(cstr_n_cmp("some str", "some str", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp("someFOOBAR", "some str", strlen("some")), 0);

    EMU_EXPECT_NE_INT(cstr_n_cmp("some str", "SOME STR", strlen("some str")), 0);
    EMU_EXPECT_NE_INT(cstr_n_cmp("someFOOBAR", "SOME str", strlen("some")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp("some str", "another str", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp("another str", "some str", strlen("another str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp("some str", "\0", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp("\0", "some str", strlen("some str")), 0);

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_eq)
{
    EMU_EXPECT_TRUE(cstr_eq("some str", "some str"));
    EMU_EXPECT_FALSE(cstr_eq("some str", "SOME STR"));
    EMU_EXPECT_FALSE(cstr_eq("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_ne)
{
    EMU_EXPECT_FALSE(cstr_ne("some str", "some str"));
    EMU_EXPECT_TRUE(cstr_ne("some str", "SOME STR"));
    EMU_EXPECT_TRUE(cstr_ne("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cmp_case)
{
    EMU_EXPECT_EQ_INT(cstr_cmp_case("some str", "some str"), 0);
    EMU_EXPECT_EQ_INT(cstr_cmp_case("some str", "SOME STR"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp_case("some str", "another str"), 0);
    EMU_EXPECT_GT_INT(cstr_cmp_case("some str", "ANOTHER STR"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp_case("another str", "some str"), 0);
    EMU_EXPECT_LT_INT(cstr_cmp_case("another str", "SOME STR"), 0);
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cmp_case)
{
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some str", "some str", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some str", "SOME STR", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some strFOOBAR", "some strBAZ", strlen("some str")), 0);
    EMU_EXPECT_EQ_INT(cstr_n_cmp_case("some strFOOBAR", "SOME STRbaz", strlen("some str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "another str", strlen("another str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "ANOTHER STR", strlen("another str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some strFOOBAR", "another strBAZ", strlen("some str")), 0);
    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some strFOOBAR", "ANOTHER STRbaz", strlen("some str")), 0);

    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another str", "some str", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another str", "SOME STR", strlen("another str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another strFOOBAR", "some strBAZ", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("another strFOOBAR", "SOME STRbaz", strlen("some str")), 0);

    EMU_EXPECT_GT_INT(cstr_n_cmp_case("some str", "\0", strlen("some str")), 0);
    EMU_EXPECT_LT_INT(cstr_n_cmp_case("\0", "some str", strlen("some str")), 0);

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_eq_case)
{
    EMU_EXPECT_TRUE(cstr_eq_case("some str", "some str"));
    EMU_EXPECT_TRUE(cstr_eq_case("some str", "SOME STR"));
    EMU_EXPECT_FALSE(cstr_eq_case("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_ne_case)
{
    EMU_EXPECT_FALSE(cstr_ne_case("some str", "some str"));
    EMU_EXPECT_FALSE(cstr_ne_case("some str", "SOME STR"));
    EMU_EXPECT_TRUE(cstr_ne_case("some str", "another str"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_len)
{
    EMU_EXPECT_EQ(cstr_len(""), strlen(""));
    EMU_EXPECT_EQ(cstr_len("some str"), strlen("some str"));
    EMU_EXPECT_EQ(cstr_len("foobar"), strlen("foobar"));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_len_ge)
{
    EMU_EXPECT_TRUE(cstr_len_ge("", 0));
    EMU_EXPECT_TRUE(cstr_len_ge("a", 0));
    EMU_EXPECT_TRUE(cstr_len_ge("a", 1));
    EMU_EXPECT_FALSE(cstr_len_ge("a", 2));
    EMU_EXPECT_FALSE(cstr_len_ge("a", 100));
    EMU_EXPECT_TRUE(cstr_len_ge("some str", strlen("some str")));
    EMU_EXPECT_TRUE(cstr_len_ge("some str", strlen("some str")-1));
    EMU_EXPECT_FALSE(cstr_len_ge("some str", strlen("some str")+1));
    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cat)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_cat(buff, "some str");
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_cat(buff, "another str");
    EMU_ASSERT_STREQ(buff, "some stranother str");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cat)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_n_cat(buff, "some str", cstr_len("some str"));
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_n_cat(buff, "another str", cstr_len("another"));
    EMU_ASSERT_STREQ(buff, "some stranother");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_cpy)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_cpy(buff, "some str");
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_cpy(buff, "another str");
    EMU_ASSERT_STREQ(buff, "another str");

    EMU_END_TEST();
}

EMU_TEST(cstr__cstr_n_cpy)
{
    char buff[128] = {0};
    EMU_ASSERT_EQ(strlen(buff), 0);

    cstr_n_cpy(buff, "some str", strlen("some str"));
    EMU_ASSERT_STREQ(buff, "some str");

    cstr_n_cpy(buff, "another str", strlen("another"));
    buff[strlen("another")] = '\0';
    EMU_ASSERT_STREQ(buff, "another");

    EMU_END_TEST();
}

EMU_GROUP(cstr)
{
    EMU_ADD(cstr__cstr_cmp);
    EMU_ADD(cstr__cstr_n_cmp);
    EMU_ADD(cstr__cstr_eq);
    EMU_ADD(cstr__cstr_ne);
    EMU_ADD(cstr__cstr_cmp_case);
    EMU_ADD(cstr__cstr_n_cmp_case);
    EMU_ADD(cstr__cstr_eq_case);
    EMU_ADD(cstr__cstr_ne_case);
    EMU_ADD(cstr__cstr_len);
    EMU_ADD(cstr__cstr_len_ge);
    EMU_ADD(cstr__cstr_cat);
    EMU_ADD(cstr__cstr_n_cat);
    EMU_ADD(cstr__cstr_cpy);
    EMU_ADD(cstr__cstr_n_cpy);
    EMU_END_GROUP();
}

EMU_TEST(nstr__nstr_init_and_nstr_fini)
{
    nstr_t nstr;
    nstr_init(&nstr);

    EMU_ASSERT_NOT_NULL(nstr.data);
    EMU_ASSERT_EQ_UINT(0, nstr.len);
    EMU_ASSERT_EQ_UINT(0, strlen(nstr.data));
    EMU_ASSERT_EQ_CHAR('\0', nstr.data[0]);

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_cstr)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "some str");

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));
    EMU_ASSERT_STREQ(nstrB.data, "some str");

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is much longer");

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is much longer"));
    EMU_ASSERT_STREQ(nstrC.data, "a string that is much longer");

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_nstr)
{
    nstr_t nstrA;
    nstr_t otherA;
    nstr_init_cstr(&otherA, "");
    nstr_init_nstr(&nstrA, &otherA);

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_t otherB;
    nstr_init_cstr(&otherB, "some str");
    nstr_init_nstr(&nstrB, &otherB);

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));
    EMU_ASSERT_STREQ(nstrB.data, "some str");

    nstr_t nstrC;
    nstr_t otherC;
    nstr_init_cstr(&otherC, "a string that is much longer");
    nstr_init_nstr(&nstrC, &otherC);

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is much longer"));
    EMU_ASSERT_STREQ(nstrC.data, "a string that is much longer");

    nstr_fini(&nstrA);
    nstr_fini(&otherA);
    nstr_fini(&nstrB);
    nstr_fini(&otherB);
    nstr_fini(&nstrC);
    nstr_fini(&otherC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_init_fmt)
{
    nstr_t nstrA;
    nstr_init_fmt(&nstrA, "");

    EMU_ASSERT_NOT_NULL(nstrA.data);
    EMU_ASSERT_EQ_UINT(nstrA.len, cstr_len(""));
    EMU_ASSERT_STREQ(nstrA.data, "");

    nstr_t nstrB;
    nstr_init_fmt(&nstrB, "%d %s", 10, "foo");

    EMU_ASSERT_NOT_NULL(nstrB.data);
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("10 foo"));
    EMU_ASSERT_STREQ(nstrB.data, "10 foo");

    nstr_t nstrC;
    nstr_init_fmt(&nstrC, "a longer formatted string %u %s", 8, "bar");

    EMU_ASSERT_NOT_NULL(nstrC.data);
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a longer formatted string 8 bar"));
    EMU_ASSERT_STREQ(nstrC.data, "a longer formatted string 8 bar");

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_defer_fini)
{
    nstr_t nstr_defer_fini nstr;
    // If nstr_fini is never called on nstr, then a memory leak will show when
    // the memcheck tests are run.
    nstr_init_cstr(&nstr, "The quick brown fox jumps over the lazy dog.");
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_len)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");
    EMU_ASSERT_EQ_UINT(nstrA.len, 0);

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "some str");
    EMU_ASSERT_EQ_UINT(nstrB.len, cstr_len("some str"));

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is longer");
    EMU_ASSERT_EQ_UINT(nstrC.len, cstr_len("a string that is longer"));

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_capacity)
{
    nstr_t nstrA;
    nstr_init_cstr(&nstrA, "");
    EMU_ASSERT_GE_UINT(nstrA.capacity, 1);

    nstr_t nstrB;
    nstr_init_cstr(&nstrB, "123456789012345");
    EMU_ASSERT_GE_UINT(
        nstrB.capacity,
        cstr_len("123456789012345")+NULL_TERMINATOR_LEN
    );

    nstr_t nstrC;
    nstr_init_cstr(&nstrC, "a string that is much longer");
    EMU_ASSERT_GE_UINT(
        nstrC.capacity,
        cstr_len("a string that is much longer")+NULL_TERMINATOR_LEN
    );

    nstr_fini(&nstrA);
    nstr_fini(&nstrB);
    nstr_fini(&nstrC);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_resize)
{
    nstr_t nstr;
    nstr_init(&nstr);

    // Expand, but still within a small string.
    nstr_resize(&nstr, 2);
    EMU_ASSERT_EQ_UINT(nstr.len, 2);
    // Underlying string wasn't changed, so the cstr_len should still be 0;
    EMU_ASSERT_STREQ(nstr.data, "");

    // Expand to heap allocated string..
    nstr_resize(&nstr, 100);
    EMU_ASSERT_EQ_UINT(nstr.len, 100);
    // Underlying string wasn't changed, so the cstr_len should still be 0;
    EMU_ASSERT_STREQ(nstr.data, "");

    // Change data and string. Appended null terminator should prevent overflow.
    nstr.data[0] = 'A';
    nstr.data[1] = 'B';
    nstr.data[2] = 'C';
    nstr.data[3] = 'D';
    nstr.data[4] = 'E';
    nstr_resize(&nstr, 3);
    EMU_ASSERT_EQ_UINT(nstr.len, 3);
    EMU_ASSERT_STREQ(nstr.data, "ABC");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_reserve)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "small str");

    nstr_reserve(&nstr, 1000);
    EMU_ASSERT_STREQ(nstr.data, "small str");
    EMU_ASSERT_GE(nstr.capacity, cstr_len("small str") + 1000);

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_cstr)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "foo");

    nstr_assign_cstr(&nstr, "bar");
    EMU_ASSERT_STREQ(nstr.data, "bar");

    // small str -> big str
    nstr_assign_cstr(&nstr, "everybody go bananas");
    EMU_ASSERT_STREQ(nstr.data, "everybody go bananas");

    // big str -> small str
    nstr_assign_cstr(&nstr, "baz");
    EMU_ASSERT_STREQ(nstr.data, "baz");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_nstr)
{
    nstr_t nstr;
    nstr_t bar;
    nstr_t bananas;
    nstr_t baz;
    nstr_init_cstr(&nstr, "foo");
    nstr_init_cstr(&bar, "bar");
    nstr_init_cstr(&bananas, "I wanna go bananas");
    nstr_init_cstr(&baz, "baz");

    nstr_assign_nstr(&nstr, &bar);
    EMU_ASSERT_STREQ(nstr.data, "bar");

    // small str -> big str
    nstr_assign_nstr(&nstr, &bananas);
    EMU_ASSERT_STREQ(nstr.data, "I wanna go bananas");

    // big str -> small str
    nstr_assign_nstr(&nstr, &baz);
    EMU_ASSERT_STREQ(nstr.data, "baz");

    nstr_fini(&nstr);
    nstr_fini(&bar);
    nstr_fini(&bananas);
    nstr_fini(&baz);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_assign_fmt)
{
    nstr_t nstr;
    nstr_init(&nstr);

    nstr_assign_fmt(&nstr, "%d %s", 10, "foo");
    EMU_ASSERT_STREQ(nstr.data, "10 foo");

    nstr_assign_fmt(&nstr, "a longer formatted string %u %s", 8, "bar");
    EMU_ASSERT_STREQ(nstr.data, "a longer formatted string 8 bar");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_clear)
{
    nstr_t nstr;
    nstr_init_cstr(&nstr, "apples");
    nstr_clear(&nstr);
    EMU_ASSERT_EQ(nstr.len, 0);
    EMU_ASSERT_STREQ(nstr.data, "");

    nstr_assign_cstr(&nstr, "everybody go bananas for nstrings!");
    nstr_clear(&nstr);
    EMU_ASSERT_EQ(nstr.len, 0);
    EMU_ASSERT_STREQ(nstr.data, "");

    nstr_fini(&nstr);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cmp)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_EQ_INT(nstr_cmp(&A, &A), 0);
    EMU_EXPECT_NE_INT(nstr_cmp(&A, &B), 0);
    EMU_EXPECT_GT_INT(nstr_cmp(&A, &C), 0);
    EMU_EXPECT_LT_INT(nstr_cmp(&C, &A), 0);

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_eq)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_TRUE(nstr_eq(&A, &A));
    EMU_EXPECT_FALSE(nstr_eq(&A, &B));
    EMU_EXPECT_FALSE(nstr_eq(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_ne)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_FALSE(nstr_ne(&A, &A));
    EMU_EXPECT_TRUE(nstr_ne(&A, &B));
    EMU_EXPECT_TRUE(nstr_ne(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cmp_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_t D;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");
    nstr_init_cstr(&D, "ANOTHER STR");

    EMU_EXPECT_EQ_INT(nstr_cmp_case(&A, &A), 0);
    EMU_EXPECT_EQ_INT(nstr_cmp_case(&A, &B), 0);
    EMU_EXPECT_GT_INT(nstr_cmp_case(&A, &C), 0);
    EMU_EXPECT_GT_INT(nstr_cmp_case(&A, &D), 0);
    EMU_EXPECT_LT_INT(nstr_cmp_case(&C, &A), 0);
    EMU_EXPECT_LT_INT(nstr_cmp_case(&C, &B), 0);

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    nstr_fini(&D);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_eq_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_TRUE(nstr_eq_case(&A, &A));
    EMU_EXPECT_TRUE(nstr_eq_case(&A, &B));
    EMU_EXPECT_FALSE(nstr_eq_case(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_ne_case)
{
    nstr_t A;
    nstr_t B;
    nstr_t C;
    nstr_init_cstr(&A, "some str");
    nstr_init_cstr(&B, "SOME STR");
    nstr_init_cstr(&C, "another str");

    EMU_EXPECT_FALSE(nstr_ne_case(&A, &A));
    EMU_EXPECT_FALSE(nstr_ne_case(&A, &B));
    EMU_EXPECT_TRUE(nstr_ne_case(&A, &C));

    nstr_fini(&A);
    nstr_fini(&B);
    nstr_fini(&C);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_cstr)
{
    nstr_t dest;
    nstr_init(&dest);

    nstr_cat_cstr(&dest, "some str");
    EMU_ASSERT_STREQ(dest.data, "some str");

    nstr_cat_cstr(&dest, "another str");
    EMU_ASSERT_STREQ(dest.data, "some stranother str");

    nstr_fini(&dest);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_nstr)
{
    nstr_t dest;
    nstr_t src;
    nstr_init(&dest);
    nstr_init_cstr(&src, "the quick brown fox");

    nstr_cat_nstr(&dest, &src);
    EMU_ASSERT_STREQ(dest.data, "the quick brown fox");

    nstr_cat_nstr(&dest, &src);
    EMU_ASSERT_STREQ(dest.data, "the quick brown foxthe quick brown fox");

    nstr_fini(&dest);
    nstr_fini(&src);
    EMU_END_TEST();
}

EMU_TEST(nstr__nstr_cat_fmt)
{
    nstr_t dest;
    nstr_init(&dest);

    nstr_cat_fmt(&dest, "%d %s", 10, "foo");
    EMU_ASSERT_STREQ(dest.data, "10 foo");

    nstr_cat_fmt(&dest, "a longer formatted string %u %s", 8, "bar");
    EMU_ASSERT_STREQ(dest.data, "10 fooa longer formatted string 8 bar");

    nstr_fini(&dest);
    EMU_END_TEST();
}

EMU_GROUP(nstr)
{
    EMU_ADD(nstr__nstr_init_and_nstr_fini);
    EMU_ADD(nstr__nstr_init_cstr);
    EMU_ADD(nstr__nstr_init_nstr);
    EMU_ADD(nstr__nstr_init_fmt);
    EMU_ADD(nstr__nstr_defer_fini);
    EMU_ADD(nstr__nstr_len);
    EMU_ADD(nstr__nstr_capacity);
    EMU_ADD(nstr__nstr_resize);
    EMU_ADD(nstr__nstr_reserve);
    EMU_ADD(nstr__nstr_assign_cstr);
    EMU_ADD(nstr__nstr_assign_nstr);
    EMU_ADD(nstr__nstr_assign_fmt);
    EMU_ADD(nstr__nstr_clear);
    EMU_ADD(nstr__nstr_cmp);
    EMU_ADD(nstr__nstr_eq);
    EMU_ADD(nstr__nstr_ne);
    EMU_ADD(nstr__nstr_cmp_case);
    EMU_ADD(nstr__nstr_eq_case);
    EMU_ADD(nstr__nstr_ne_case);
    EMU_ADD(nstr__nstr_cat_cstr);
    EMU_ADD(nstr__nstr_cat_nstr);
    EMU_ADD(nstr__nstr_cat_fmt);
    EMU_END_GROUP();
}

EMU_GROUP(nstring_h)
{
    EMU_ADD(cstr);
    EMU_ADD(nstr);
    EMU_END_GROUP();
}
