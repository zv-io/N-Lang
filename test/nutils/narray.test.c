/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <EMU/EMUtest.h>
#include <nutils/narray.h>

EMU_TEST(narr_alloc_and_narr_free)
{
    int* narrA = narr_alloc(0, sizeof(int));

    EMU_ASSERT_NOT_NULL(narrA);
    EMU_ASSERT_EQ_UINT(narr_length(narrA), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narrA), narr_length(narrA));
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narrA), sizeof(int));

    short* narrB = narr_alloc(13, sizeof(short));

    EMU_ASSERT_NOT_NULL(narrB);
    EMU_ASSERT_EQ_UINT(narr_length(narrB), 13);
    EMU_ASSERT_GE_UINT(narr_capacity(narrB), narr_length(narrB));
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narrB), sizeof(short));

    narr_free(narrA);
    narr_free(narrB);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__does_nothing_when_reserving_zero_elements)
{
    // Zero length array.
    int* narr_zero = narr_alloc(0, sizeof(int));
    size_t len = narr_length(narr_zero);
    size_t cap = narr_capacity(narr_zero);
    size_t elsz = narr_sizeof_elem(narr_zero);

    narr_zero = narr_reserve(narr_zero, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_zero), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_zero), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_zero), elsz);

    // Small array.
    int* narr_small = narr_alloc(10, sizeof(int));
    len = narr_length(narr_small);
    cap = narr_capacity(narr_small);
    elsz = narr_sizeof_elem(narr_small);

    narr_small = narr_reserve(narr_small, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_small), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_small), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_small), elsz);

    // Large array.
    int* narr_large = narr_alloc(2000, sizeof(int));
    len = narr_length(narr_large);
    cap = narr_capacity(narr_large);
    elsz = narr_sizeof_elem(narr_large);

    narr_large = narr_reserve(narr_large, 0);

    EMU_ASSERT_EQ_UINT(narr_length(narr_large), len);
    EMU_ASSERT_EQ_UINT(narr_capacity(narr_large), cap);
    EMU_ASSERT_EQ_UINT(narr_sizeof_elem(narr_large), elsz);

    narr_free(narr_zero);
    narr_free(narr_small);
    narr_free(narr_large);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__reserving_a_large_amount_should_change_capacity)
{
    int* narr = narr_alloc(0, sizeof(int));
    size_t const cap = narr_capacity(narr);
    narr = narr_reserve(narr, 5000);
    EMU_EXPECT_GT_UINT(narr_capacity(narr), cap);
    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_reserve__ensures_capacity_is_always_greater_or_equal_to_len)
{
    int* narr = narr_alloc(0, sizeof(int));

    narr = narr_reserve(narr, 0);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 10);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 100);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 1000);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr = narr_reserve(narr, 5000);
    EMU_EXPECT_GE(narr_capacity(narr), narr_length(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narr_reserve)
{
    EMU_ADD(narr_reserve__ensures_capacity_is_always_greater_or_equal_to_len);
    EMU_ADD(narr_reserve__does_nothing_when_reserving_zero_elements);
    EMU_ADD(narr_reserve__reserving_a_large_amount_should_change_capacity);
    EMU_END_GROUP();
}

EMU_TEST(narr_resize__correctly_changes_length_and_capacity)
{
    int* narr = narr_alloc(0, sizeof(int));

    narr = narr_resize(narr, 10);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 10);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 0);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 4096);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 4096);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, narr_length(narr)+1);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 4097);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 500);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 500);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr = narr_resize(narr, 0);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 0);
    EMU_ASSERT_GE_UINT(narr_capacity(narr), narr_length(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__resize_with_same_length_should_not_change_data_members)
{
    int* narr = narr_alloc(100, sizeof(int));
    narr[0] = 33;
    narr[50] = 44;
    narr[99] = 55;
    size_t const len = narr_length(narr);
    size_t const cap = narr_capacity(narr);
    size_t const elsz = narr_sizeof_elem(narr);

    narr = narr_resize(narr, 100);

    EMU_ASSERT_EQ(narr[0], 33);
    EMU_ASSERT_EQ(narr[50], 44);
    EMU_ASSERT_EQ(narr[99], 55);
    EMU_ASSERT_EQ(narr_length(narr), len);
    EMU_ASSERT_EQ(narr_capacity(narr), cap);
    EMU_ASSERT_EQ(narr_sizeof_elem(narr), elsz);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__downsizing_should_not_change_capacity)
{
    int* narr = narr_alloc(130, sizeof(int));
    size_t const cap = narr_capacity(narr);

    narr = narr_resize(narr, 10);

    EMU_ASSERT_EQ(narr_capacity(narr), cap);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_resize__upsizing_by_a_large_amount_should_change_capacity)
{
    int* narr = narr_alloc(50, sizeof(int));
    size_t const cap = narr_capacity(narr);

    narr = narr_resize(narr, 4096);

    EMU_ASSERT_GE(narr_capacity(narr), cap);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narr_resize)
{
    EMU_ADD(narr_resize__correctly_changes_length_and_capacity);
    EMU_ADD(narr_resize__resize_with_same_length_should_not_change_data_members);
    EMU_ADD(narr_resize__downsizing_should_not_change_capacity);
    EMU_ADD(narr_resize__upsizing_by_a_large_amount_should_change_capacity);
    EMU_END_GROUP();
}

EMU_TEST(narr_push)
{
    int* narr = narr_alloc(0, sizeof(int));

    int val = 3;
    narr = narr_push(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 1);
    EMU_ASSERT_EQ_INT(narr[narr_length(narr)-1], val);

    val = 100;
    narr = narr_push(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 2);
    EMU_ASSERT_EQ_INT(narr[narr_length(narr)-1], val);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_pop)
{
    int* narr = narr_alloc(100, sizeof(int));

    narr_pop(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 99);
    narr_pop(narr);
    narr_pop(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 97);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_unshift)
{
    int* narr = narr_alloc(0, sizeof(int));

    int val = 40;
    narr = narr_unshift(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 1);
    EMU_ASSERT_EQ_INT(narr[0], 40);

    val = 101;
    narr = narr_unshift(narr, &val);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 2);
    EMU_ASSERT_EQ_INT(narr[0], 101);
    EMU_ASSERT_EQ_INT(narr[1], 40);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_shift)
{
    int* narr = narr_alloc(100, sizeof(int));

    narr_shift(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 99);
    narr_shift(narr);
    narr_shift(narr);
    EMU_ASSERT_EQ_UINT(narr_length(narr), 97);

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_front)
{
    int* narr = narr_alloc(0, sizeof(int));

    for (size_t i = 0; i < 100; ++i)
    {
        int r = rand();
        narr = narr_push(narr, &r);
    }

    EMU_ASSERT_EQ(&narr[0], narr_front(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_TEST(narr_back)
{
    int* narr = narr_alloc(0, sizeof(int));

    for (size_t i = 0; i < 100; ++i)
    {
        int r = rand();
        narr = narr_push(narr, &r);
    }

    EMU_ASSERT_EQ(&narr[99], narr_back(narr));

    narr_free(narr);
    EMU_END_TEST();
}

EMU_GROUP(narray_h)
{
    EMU_ADD(narr_alloc_and_narr_free);
    EMU_ADD(narr_reserve);
    EMU_ADD(narr_resize);
    EMU_ADD(narr_push);
    EMU_ADD(narr_pop);
    EMU_ADD(narr_unshift);
    EMU_ADD(narr_shift);
    EMU_ADD(narr_front);
    EMU_ADD(narr_back);
    EMU_END_GROUP();
}
