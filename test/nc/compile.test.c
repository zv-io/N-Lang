/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_DECLARE(compile__comments);
EMU_DECLARE(compile__language_constructs);
EMU_DECLARE(compile__example_programs);

EMU_GROUP(compile_h)
{
    EMU_ADD(compile__comments);
    EMU_ADD(compile__language_constructs);
    EMU_ADD(compile__example_programs);
    EMU_END_GROUP();
}
