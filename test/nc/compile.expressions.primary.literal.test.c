/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

//============================================================================//
//      CHARACTER LITERALS                                                    //
//============================================================================//
EMU_TEST(literal__ascii__escape_characters)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    '\\0'a;                                                                  \n\
    '\\a'a;                                                                  \n\
    '\\b'a;                                                                  \n\
    '\\e'a;                                                                  \n\
    '\\f'a;                                                                  \n\
    '\\n'a;                                                                  \n\
    '\\t'a;                                                                  \n\
    '\\v'a;                                                                  \n\
    '\\\\'a;                                                                 \n\
    '\\\''a;                                                                 \n\
    '\\\"'a;                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__ascii__printible_ascii_characters_except_single_and_double_quote)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    ' 'a;                                                                    \n\
    '!'a;                                                                    \n\
    '#'a;                                                                    \n\
    '$'a;                                                                    \n\
    '%'a;                                                                    \n\
    '&'a;                                                                    \n\
    '('a;                                                                    \n\
    ')'a;                                                                    \n\
    '*'a;                                                                    \n\
    '+'a;                                                                    \n\
    ','a;                                                                    \n\
    '-'a;                                                                    \n\
    '.'a;                                                                    \n\
    '/'a;                                                                    \n\
    '0'a;                                                                    \n\
    '1'a;                                                                    \n\
    '2'a;                                                                    \n\
    '3'a;                                                                    \n\
    '4'a;                                                                    \n\
    '5'a;                                                                    \n\
    '6'a;                                                                    \n\
    '7'a;                                                                    \n\
    '8'a;                                                                    \n\
    '9'a;                                                                    \n\
    ':'a;                                                                    \n\
    ';'a;                                                                    \n\
    '<'a;                                                                    \n\
    '='a;                                                                    \n\
    '>'a;                                                                    \n\
    '\?'a;                                                                   \n\
    '@'a;                                                                    \n\
    'A'a;                                                                    \n\
    'B'a;                                                                    \n\
    'C'a;                                                                    \n\
    'D'a;                                                                    \n\
    'E'a;                                                                    \n\
    'F'a;                                                                    \n\
    'G'a;                                                                    \n\
    'H'a;                                                                    \n\
    'I'a;                                                                    \n\
    'J'a;                                                                    \n\
    'K'a;                                                                    \n\
    'L'a;                                                                    \n\
    'M'a;                                                                    \n\
    'N'a;                                                                    \n\
    'O'a;                                                                    \n\
    'P'a;                                                                    \n\
    'Q'a;                                                                    \n\
    'R'a;                                                                    \n\
    'S'a;                                                                    \n\
    'T'a;                                                                    \n\
    'U'a;                                                                    \n\
    'V'a;                                                                    \n\
    'W'a;                                                                    \n\
    'X'a;                                                                    \n\
    'Y'a;                                                                    \n\
    'Z'a;                                                                    \n\
    '['a;                                                                    \n\
    '\\\\'a;                                                                 \n\
    ']'a;                                                                    \n\
    '^'a;                                                                    \n\
    '_'a;                                                                    \n\
    '`'a;                                                                    \n\
    'a'a;                                                                    \n\
    'b'a;                                                                    \n\
    'c'a;                                                                    \n\
    'd'a;                                                                    \n\
    'e'a;                                                                    \n\
    'f'a;                                                                    \n\
    'g'a;                                                                    \n\
    'h'a;                                                                    \n\
    'i'a;                                                                    \n\
    'j'a;                                                                    \n\
    'k'a;                                                                    \n\
    'l'a;                                                                    \n\
    'm'a;                                                                    \n\
    'n'a;                                                                    \n\
    'o'a;                                                                    \n\
    'p'a;                                                                    \n\
    'q'a;                                                                    \n\
    'r'a;                                                                    \n\
    's'a;                                                                    \n\
    't'a;                                                                    \n\
    'u'a;                                                                    \n\
    'v'a;                                                                    \n\
    'w'a;                                                                    \n\
    'x'a;                                                                    \n\
    'y'a;                                                                    \n\
    'z'a;                                                                    \n\
    '{'a;                                                                    \n\
    '|'a;                                                                    \n\
    '}'a;                                                                    \n\
    '~'a;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(literal__ascii)
{
    EMU_ADD(literal__ascii__printible_ascii_characters_except_single_and_double_quote);
    EMU_ADD(literal__ascii__escape_characters);
    EMU_END_GROUP();
}

//============================================================================//
//      STRING LITERALS                                                       //
//============================================================================//
EMU_TEST(literal__string__empty_string)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    \"\"a;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__string__hello_world)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    \"Hello, world!\"a;                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(literal__string)
{
    EMU_ADD(literal__string__empty_string);
    EMU_ADD(literal__string__hello_world);
    EMU_END_GROUP();
}

//============================================================================//
//      FLOATING POINT LITERALS                                               //
//============================================================================//
EMU_TEST(literal__float__f32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    3.14159f32;                                                              \n\
    +3.14159f32;                                                             \n\
    -3.14159f32;                                                             \n\
    03.14159f32;                                                             \n\
                                                                             \n\
    3.14159e10f32;                                                           \n\
    3.14159e+10f32;                                                          \n\
    3.14159e-10f32;                                                          \n\
                                                                             \n\
    +3.14159e-10f32;                                                         \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__float__f64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    3.14159f64;                                                              \n\
    +3.14159f64;                                                             \n\
    -3.14159f64;                                                             \n\
    03.14159f64;                                                             \n\
                                                                             \n\
    3.14159e10f64;                                                           \n\
    3.14159e+10f64;                                                          \n\
    3.14159e-10f64;                                                          \n\
                                                                             \n\
    +3.14159e-10f64;                                                         \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(literal__float)
{
    EMU_ADD(literal__float__f32);
    EMU_ADD(literal__float__f64);
    EMU_END_GROUP();
}

//============================================================================//
//      NULL LITERAL                                                          //
//============================================================================//
EMU_TEST(literal__null)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : ^void = null;                                                  \n\
    let bar : ^mut u = null;                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

//============================================================================//
//      ARRAY LITERAL                                                         //
//============================================================================//
EMU_TEST(literal__array__fully_specified__simple)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 3s, 5s];                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__simple)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [10u]s = [1s, 3s, 5s, ..10u];                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fully_specified__two_dimensional)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u][4u]s = [                                                  \n\
        [01s, 03s, 05s, 07s],                                                \n\
        [09s, 11s, 13s, 15s],                                                \n\
        [17s, 19s, 21s, 23s]                                                 \n\
    ];                                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__two_dimensional)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u][4u]s = [                                                  \n\
        [1s,      ..4u],                                                     \n\
        [3s, 12s, ..4u],                                                     \n\
        ..3u                                                                 \n\
    ];                                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(literal__array__valid)
{
    EMU_ADD(literal__array__fully_specified__simple);
    EMU_ADD(literal__array__fill__simple);
    EMU_ADD(literal__array__fully_specified__two_dimensional);
    EMU_ADD(literal__array__fill__two_dimensional);
    EMU_END_GROUP();
}

EMU_TEST(literal__array__with_length_zero)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [];                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__with_mismatched_length__too_short)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 2s];                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__with_mismatched_length__too_long)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 2s, 3s, 4s];                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__with_no_filling_element)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [..3u];                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__with_no_more_elements_to_fill)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 2s, 3s, ..3u];                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__with_fill_less_than_the_array_length)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 2s, 3s, ..2u];                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(literal__array__fill__with_fill_mid_array)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [3u]s = [1s, 2s, ..3u, 3s];                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(literal__array__invalid)
{
    EMU_ADD(literal__array__with_length_zero);
    EMU_ADD(literal__array__with_mismatched_length__too_short);
    EMU_ADD(literal__array__with_mismatched_length__too_long);
    EMU_ADD(literal__array__fill__with_no_filling_element);
    EMU_ADD(literal__array__fill__with_no_more_elements_to_fill);
    EMU_ADD(literal__array__fill__with_fill_less_than_the_array_length);
    EMU_ADD(literal__array__fill__with_fill_mid_array);
    EMU_END_GROUP();
}

EMU_GROUP(literal__array)
{
    EMU_ADD(literal__array__valid);
    EMU_ADD(literal__array__invalid);
    EMU_END_GROUP();
}

//============================================================================//

EMU_GROUP(primary__literal)
{
    EMU_ADD(literal__ascii);
    EMU_ADD(literal__string);
    EMU_ADD(literal__float);
    EMU_ADD(literal__null);
    EMU_ADD(literal__array);
    EMU_END_GROUP();
}
