#include "nc.testutils.h"
#include <nc/nir.h>

#define PRINT_SIZEOF(type) \
    EMU_PRINT_INDENT(); \
    printf("sizeof %-30s : %zu\n", _EMU_STRINGIFY(type), sizeof(type));

EMU_DECLARE(dt);

EMU_GROUP(nir_h)
{
    PRINT_SIZEOF(struct id);
    PRINT_SIZEOF(struct qualifiers);
    PRINT_SIZEOF(struct dt);
    PRINT_SIZEOF(struct member_var);
    PRINT_SIZEOF(struct member_func);
    PRINT_SIZEOF(struct struct_def);
    PRINT_SIZEOF(struct func_sig);
    PRINT_SIZEOF(struct literal);
    PRINT_SIZEOF(struct expr);
    PRINT_SIZEOF(struct var_decl);
    PRINT_SIZEOF(struct func_decl);
    PRINT_SIZEOF(struct indicative_conditional);
    PRINT_SIZEOF(struct if_elif_else);
    PRINT_SIZEOF(struct loop);
    PRINT_SIZEOF(struct alias);
    PRINT_SIZEOF(struct stmt);
    PRINT_SIZEOF(struct symbol_table);
    PRINT_SIZEOF(struct symbol);
    PRINT_SIZEOF(struct scope);
    PRINT_SIZEOF(struct module);

    EMU_END_GROUP();
}
