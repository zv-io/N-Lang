/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(minimal_complete_program)
{
    COMPILE_TEST_EXAMPLE_PROG("minimal_complete_program.n");
    EMU_END_TEST();
}

EMU_TEST(hello_world)
{
    COMPILE_TEST_EXAMPLE_PROG("hello_world.n");
    EMU_END_TEST();
}

EMU_TEST(range_loop)
{
    COMPILE_TEST_EXAMPLE_PROG("range_loop.n");
    EMU_END_TEST();
}

EMU_TEST(deferred_execution)
{
    COMPILE_TEST_EXAMPLE_PROG("deferred_execution.n");
    EMU_END_TEST();
}

EMU_TEST(formatting)
{
    COMPILE_TEST_EXAMPLE_PROG("formatting.n");
    EMU_END_TEST();
}

EMU_TEST(fibonacci_iterative)
{
    COMPILE_TEST_EXAMPLE_PROG("fib_i.n");
    EMU_END_TEST();
}

EMU_TEST(fibonacci_recursive)
{
    COMPILE_TEST_EXAMPLE_PROG("fib_r.n");
    EMU_END_TEST();
}

EMU_TEST(ackermann)
{
    COMPILE_TEST_EXAMPLE_PROG("ackermann.n");
    EMU_END_TEST();
}

EMU_TEST(bf)
{
    COMPILE_TEST_EXAMPLE_PROG("bf.n");
    EMU_END_TEST();
}

EMU_TEST(letter_frequency)
{
    COMPILE_TEST_EXAMPLE_PROG("letter_frequency.n");
    EMU_END_TEST();
}

EMU_TEST(rot13)
{
    COMPILE_TEST_EXAMPLE_PROG("rot13.n");
    EMU_END_TEST();
}

EMU_GROUP(compile__example_programs)
{
    EMU_ADD(minimal_complete_program);
    EMU_ADD(hello_world);
    EMU_ADD(deferred_execution);
    EMU_ADD(range_loop);
    EMU_ADD(formatting);
    EMU_ADD(fibonacci_iterative);
    EMU_ADD(fibonacci_recursive);
    EMU_ADD(ackermann);
    EMU_ADD(bf);
    EMU_ADD(letter_frequency);
    EMU_ADD(rot13);
    EMU_END_GROUP();
}
