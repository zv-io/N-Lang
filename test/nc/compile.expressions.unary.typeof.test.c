/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_typeof__integer_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let xu8 : u8 = void;                                                     \n\
    let xu8_dup : typeof(xu8) = void;                                        \n\
                                                                             \n\
    let xu16 : u16 = void;                                                   \n\
    let xu16_dup : typeof(xu16) = void;                                      \n\
                                                                             \n\
    let xu32 : u32 = void;                                                   \n\
    let xu32_dup : typeof(xu32) = void;                                      \n\
                                                                             \n\
    let xu64 : u64 = void;                                                   \n\
    let xu64_dup : typeof(xu64) = void;                                      \n\
                                                                             \n\
    let xu : u = void;                                                       \n\
    let xu_dup : typeof(xu) = void;                                          \n\
                                                                             \n\
    let xs8 : s8 = void;                                                     \n\
    let xs8_dup : typeof(xs8) = void;                                        \n\
                                                                             \n\
    let xs16 : s16 = void;                                                   \n\
    let xs16_dup : typeof(xs16) = void;                                      \n\
                                                                             \n\
    let xs32 : s32 = void;                                                   \n\
    let xs32_dup : typeof(xs32) = void;                                      \n\
                                                                             \n\
    let xs64 : s64 = void;                                                   \n\
    let xs64_dup : typeof(xs64) = void;                                      \n\
                                                                             \n\
    let xs : s = void;                                                       \n\
    let xs_dup : typeof(xs) = void;                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__integer_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let xu8  : typeof(10u8) = void;                                          \n\
    let xu16 : typeof(10u16) = void;                                         \n\
    let xu32 : typeof(10u32) = void;                                         \n\
    let xu64 : typeof(10u64) = void;                                         \n\
    let xu   : typeof(10u) = void;                                           \n\
    let xs8  : typeof(10s8) = void;                                          \n\
    let xs16 : typeof(10s16) = void;                                         \n\
    let xs32 : typeof(10s32) = void;                                         \n\
    let xs64 : typeof(10s64) = void;                                         \n\
    let xs   : typeof(10s) = void;                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__integer_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let xu8  : typeof(u8) = void;                                            \n\
    let xu16 : typeof(u16) = void;                                           \n\
    let xu32 : typeof(u32) = void;                                           \n\
    let xu64 : typeof(u64) = void;                                           \n\
    let xu   : typeof(u) = void;                                             \n\
    let xs8  : typeof(s8) = void;                                            \n\
    let xs16 : typeof(s16) = void;                                           \n\
    let xs32 : typeof(s32) = void;                                           \n\
    let xs64 : typeof(s64) = void;                                           \n\
    let xs   : typeof(s) = void;                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__bool_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : bool = void;                                                     \n\
    let x_dup : typeof(x) = void;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__bool_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x_true : typeof(true) = void;                                        \n\
    let x_false : typeof(false) = void;                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__bool_data_type)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof(bool) = void;                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__ascii_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : ascii = void;                                                    \n\
    let x_dup : typeof(x) = void;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__ascii_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof('A'a) = void;                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__ascii_data_type)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof(ascii) = void;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__pointer_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : ^s = void;                                                       \n\
    let x_dup : typeof(x) = void;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__pointer_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof(^s32) = void;                                             \n\
    let y : typeof(^^() -> void) = void;                                     \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__array_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : [10u]s = void;                                                   \n\
    let x_dup : typeof(x) = void;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__array_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof([10u, 11u, 12u]) = void;                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_typeof__array_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : typeof([10u]s32) = void;                                         \n\
    let y : typeof([10u]^s) = void;                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_typeof__valid)
{
    EMU_ADD(unary_typeof__integer_variables);
    EMU_ADD(unary_typeof__integer_literals);
    EMU_ADD(unary_typeof__integer_data_types);
    EMU_ADD(unary_typeof__bool_variables);
    EMU_ADD(unary_typeof__bool_literals);
    EMU_ADD(unary_typeof__bool_data_type);
    EMU_ADD(unary_typeof__ascii_variables);
    EMU_ADD(unary_typeof__ascii_literals);
    EMU_ADD(unary_typeof__ascii_data_type);
    EMU_ADD(unary_typeof__pointer_variables);
    EMU_ADD(unary_typeof__pointer_data_types);
    EMU_ADD(unary_typeof__array_variables);
    EMU_ADD(unary_typeof__array_literals);
    EMU_ADD(unary_typeof__array_data_types);
    EMU_END_GROUP();
}

EMU_GROUP(unary_typeof)
{
    EMU_ADD(unary_typeof__valid);
    EMU_END_GROUP();
}
