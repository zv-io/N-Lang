/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_sizeof__integer_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let xu8 : u8 = void;                                                     \n\
    sizeof(xu8);                                                             \n\
                                                                             \n\
    let xu16 : u16 = void;                                                   \n\
    sizeof(xu16);                                                            \n\
                                                                             \n\
    let xu32 : u32 = void;                                                   \n\
    sizeof(xu32);                                                            \n\
                                                                             \n\
    let xu64 : u64 = void;                                                   \n\
    sizeof(xu64);                                                            \n\
                                                                             \n\
    let xu : u = void;                                                       \n\
    sizeof(xu);                                                              \n\
                                                                             \n\
    let xs8 : s8 = void;                                                     \n\
    sizeof(xs8);                                                             \n\
                                                                             \n\
    let xs16 : s16 = void;                                                   \n\
    sizeof(xs16);                                                            \n\
                                                                             \n\
    let xs32 : s32 = void;                                                   \n\
    sizeof(xs32);                                                            \n\
                                                                             \n\
    let xs64 : s64 = void;                                                   \n\
    sizeof(xs64);                                                            \n\
                                                                             \n\
    let xs : s = void;                                                       \n\
    sizeof(xs);                                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__integer_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(10u8);                                                            \n\
    sizeof(10u16);                                                           \n\
    sizeof(10u32);                                                           \n\
    sizeof(10u64);                                                           \n\
    sizeof(10u);                                                             \n\
    sizeof(10s8);                                                            \n\
    sizeof(10s16);                                                           \n\
    sizeof(10s32);                                                           \n\
    sizeof(10s64);                                                           \n\
    sizeof(10s);                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__integer_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(u8);                                                              \n\
    sizeof(u16);                                                             \n\
    sizeof(u32);                                                             \n\
    sizeof(u64);                                                             \n\
    sizeof(u);                                                               \n\
    sizeof(s8);                                                              \n\
    sizeof(s16);                                                             \n\
    sizeof(s32);                                                             \n\
    sizeof(s64);                                                             \n\
    sizeof(s);                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__bool_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : bool = void;                                                     \n\
    sizeof(x);                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__bool_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(true);                                                            \n\
    sizeof(false);                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__bool_data_type)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(bool);                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__ascii_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : ascii = void;                                                    \n\
    sizeof(x);                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__ascii_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof('A'a);                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__ascii_data_type)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(ascii);                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__pointer_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : ^s = void;                                                       \n\
    sizeof(x);                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__array_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : [10u]s = void;                                                   \n\
    sizeof(x);                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__pointer_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof(^s32);                                                            \n\
    sizeof(^^() -> void);                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__array_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof([10u, 11u, 12u]);                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_sizeof__array_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    sizeof([10u]s32);                                                        \n\
    sizeof([10u]^s);                                                         \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_sizeof__valid)
{
    EMU_ADD(unary_sizeof__integer_variables);
    EMU_ADD(unary_sizeof__integer_literals);
    EMU_ADD(unary_sizeof__integer_data_types);
    EMU_ADD(unary_sizeof__bool_variables);
    EMU_ADD(unary_sizeof__bool_literals);
    EMU_ADD(unary_sizeof__bool_data_type);
    EMU_ADD(unary_sizeof__ascii_variables);
    EMU_ADD(unary_sizeof__ascii_literals);
    EMU_ADD(unary_sizeof__ascii_data_type);
    EMU_ADD(unary_sizeof__pointer_variables);
    EMU_ADD(unary_sizeof__pointer_data_types);
    EMU_ADD(unary_sizeof__array_variables);
    EMU_ADD(unary_sizeof__array_literals);
    EMU_ADD(unary_sizeof__array_data_types);
    EMU_END_GROUP();
}

EMU_GROUP(unary_sizeof)
{
    EMU_ADD(unary_sizeof__valid);
    EMU_END_GROUP();
}
