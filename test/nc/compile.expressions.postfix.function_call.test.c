/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(simple_function_call)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo();                                                                   \n\
}                                                                            \n\
    ");

    COMPILE_TEST_VALID_PROG("                                                \n\
func foo : () -> s                                                           \n\
{                                                                            \n\
    return 1s;                                                               \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo();                                                                   \n\
}                                                                            \n\
    ");

    COMPILE_TEST_VALID_PROG("                                                \n\
func foo : (bar : s) -> s                                                    \n\
{                                                                            \n\
    return bar + 1s;                                                         \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo(1s);                                                                 \n\
}                                                                            \n\
    ");

    COMPILE_TEST_VALID_PROG("                                                \n\
func foo : (bar : s, baz : s) -> s                                           \n\
{                                                                            \n\
    return bar + baz + 1s;                                                   \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo(1s, 2s);                                                             \n\
}                                                                            \n\
    ");

    EMU_END_TEST();
}

EMU_GROUP(function_call__valid)
{
    EMU_ADD(simple_function_call);
    EMU_END_GROUP();
}

EMU_TEST(function_call__as_global_initializer)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> s                                                           \n\
{                                                                            \n\
    return 1s;                                                               \n\
}                                                                            \n\
                                                                             \n\
let bar : s = foo();                                                         \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_call__fewer_arguments_than_parameters)
{

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func foo : (A : s, B : s) -> void                                            \n\
{                                                                            \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo(1s);                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_call__more_arguments_than_parameters)
{

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func foo : (A : s, B : s) -> void                                            \n\
{                                                                            \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    foo(1s, 2s, 3s, 4s, 5s, 6s);                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_call__invalid_struct_qualifiers_for_member_func)
{

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
struct foo;                                                                  \n\
func foo_member_func : (this : ^mut foo) -> void{}                           \n\
                                                                             \n\
struct foo                                                                   \n\
{                                                                            \n\
    let A : s32;                                                             \n\
    let B : s64;                                                             \n\
    func member_func = foo_member_func;                                      \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    # Declare a const foo.                                                   \n\
    let bar : foo = uninit;                                                  \n\
                                                                             \n\
    # Pass const foo to non-const member_func.                               \n\
    bar.member_func();                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(function_call__invalid)
{
    EMU_ADD(function_call__as_global_initializer);
    EMU_ADD(function_call__fewer_arguments_than_parameters);
    EMU_ADD(function_call__more_arguments_than_parameters);
    EMU_ADD(function_call__invalid_struct_qualifiers_for_member_func);
    EMU_END_GROUP();
}

EMU_GROUP(postfix__function_call)
{
    EMU_ADD(function_call__valid);
    EMU_ADD(function_call__invalid);
    EMU_END_GROUP();
}
