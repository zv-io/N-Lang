/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

static struct n_integer_value val;

static char const* LOREM_IPSUM = "lorem ipsum dolor sit amet";

//============================================================================//
//      CHARACTER                                                             //
//============================================================================//
EMU_TEST(parse_n_ascii__should_parse_all_printible_ascii_characters_except_single_and_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii(" ", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("!", &c), 0);
    EMU_EXPECT_LE(parse_n_ascii("\"", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("#", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("$", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("%", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("&", &c), 0);
    EMU_EXPECT_NE(parse_n_ascii("\'", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("(", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(")", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("*", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("+", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(",", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("-", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(".", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("/", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("0", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("1", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("2", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("3", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("4", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("5", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("6", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("7", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("8", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("9", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(":", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(";", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("<", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("=", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii(">", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("\?", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("@", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("A", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("B", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("C", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("D", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("E", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("F", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("G", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("H", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("I", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("J", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("K", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("L", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("M", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("N", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("O", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("P", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Q", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("R", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("S", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("T", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("U", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("V", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("W", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("X", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Y", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("Z", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("[", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("\\\\", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("]", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("^", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("_", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("`", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("b", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("c", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("d", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("e", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("f", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("g", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("h", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("i", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("j", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("k", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("l", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("m", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("n", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("o", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("p", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("q", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("r", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("s", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("t", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("u", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("v", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("w", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("x", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("y", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("z", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("{", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("|", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("}", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii("~", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_null)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\0", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_alert)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_backspace)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\b", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_escape)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\e", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_formfeed)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\f", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_newline)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\n", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_carriage_return)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\r", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_horizontal_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\t", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_vertical_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\v", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_backslash)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\\", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_single_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\'", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_parse_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii("\\\"", &c), 0);
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii__should_parse_all_escape_characters)
{
    EMU_ADD(parse_n_ascii__should_parse_null);
    EMU_ADD(parse_n_ascii__should_parse_alert);
    EMU_ADD(parse_n_ascii__should_parse_backspace);
    EMU_ADD(parse_n_ascii__should_parse_escape);
    EMU_ADD(parse_n_ascii__should_parse_formfeed);
    EMU_ADD(parse_n_ascii__should_parse_newline);
    EMU_ADD(parse_n_ascii__should_parse_carriage_return);
    EMU_ADD(parse_n_ascii__should_parse_horizontal_tab);
    EMU_ADD(parse_n_ascii__should_parse_vertical_tab);
    EMU_ADD(parse_n_ascii__should_parse_backslash);
    EMU_ADD(parse_n_ascii__should_parse_single_quote);
    EMU_ADD(parse_n_ascii__should_parse_double_quote);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii__should_correctly_populate_the_length_of_the_string)
{
    n_ascii c;
    ssize_t len;

    len = parse_n_ascii("t", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("t"), (size_t) len);

    len = parse_n_ascii("\\t", &c);
    EMU_ASSERT_EQ(strlen("\\t"), len);

    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_not_parse_unescaped_single_quote)
{
    n_ascii c;

    EMU_ASSERT_EQ(
        PARSE_N_ASCII_LITERAL_BAD_CHAR,
        parse_n_ascii("\'", &c)
    );
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii__should_not_parse_unescaped_double_quote)
{
    n_ascii c;

    EMU_ASSERT_EQ(
        PARSE_N_ASCII_LITERAL_BAD_CHAR,
        parse_n_ascii("\"", &c)
    );
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii__should_not_parse_unescaped_quotes)
{
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_single_quote);
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_double_quote);
    EMU_END_GROUP();
}

EMU_GROUP(parse_n_ascii)
{
    EMU_ADD(parse_n_ascii__should_parse_all_printible_ascii_characters_except_single_and_double_quote);
    EMU_ADD(parse_n_ascii__should_parse_all_escape_characters);
    EMU_ADD(parse_n_ascii__should_correctly_populate_the_length_of_the_string);
    EMU_ADD(parse_n_ascii__should_not_parse_unescaped_quotes);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_to_parse_garbage)
{
    n_ascii c;

    EMU_EXPECT_LT(parse_n_ascii_literal(LOREM_IPSUM, &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_all_printible_ascii_characters_except_single_and_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\' \'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'!\'a", &c), 0);
    EMU_EXPECT_LE_INT(parse_n_ascii_literal("\'\"\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'#\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'$\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'%\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'&\'a", &c), 0);
    EMU_EXPECT_LE_INT(parse_n_ascii_literal("\'\'\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'(\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\')\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'*\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'+\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\',\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'-\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'.\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'/\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'0\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'1\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'2\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'3\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'4\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'5\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'6\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'7\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'8\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'9\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\':\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\';\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'<\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'=\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'>\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\?\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'@\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'A\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'B\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'C\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'D\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'E\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'F\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'G\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'H\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'I\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'J\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'K\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'L\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'M\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'N\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'O\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'P\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Q\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'R\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'S\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'T\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'U\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'V\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'W\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'X\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Y\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'Z\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'[\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\\\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\']\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'^\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'_\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'`\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'a\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'b\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'c\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'d\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'e\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'f\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'g\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'h\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'i\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'j\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'k\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'l\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'m\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'n\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'o\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'p\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'q\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'r\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'s\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'t\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'u\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'v\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'w\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'x\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'y\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'z\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'{\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'|\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'}\'a", &c), 0);
    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'~\'a", &c), 0);

    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_null)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\0\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_alert)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\a\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_backspace)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\b\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_escape)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\e\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_formfeed)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\f\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_newline)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\n\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_carriage_return)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\r\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_horizontal_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\t\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_vertical_tab)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\v\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_backslash)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\\\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_single_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\'\'a", &c), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_parse_double_quote)
{
    n_ascii c;

    EMU_EXPECT_GE_INT(parse_n_ascii_literal("\'\\\"\'a", &c), 0);
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal__should_parse_all_escape_characters)
{
    EMU_ADD(parse_n_ascii_literal__should_parse_null);
    EMU_ADD(parse_n_ascii_literal__should_parse_alert);
    EMU_ADD(parse_n_ascii_literal__should_parse_backspace);
    EMU_ADD(parse_n_ascii_literal__should_parse_escape);
    EMU_ADD(parse_n_ascii_literal__should_parse_formfeed);
    EMU_ADD(parse_n_ascii_literal__should_parse_newline);
    EMU_ADD(parse_n_ascii_literal__should_parse_carriage_return);
    EMU_ADD(parse_n_ascii_literal__should_parse_horizontal_tab);
    EMU_ADD(parse_n_ascii_literal__should_parse_vertical_tab);
    EMU_ADD(parse_n_ascii_literal__should_parse_backslash);
    EMU_ADD(parse_n_ascii_literal__should_parse_single_quote);
    EMU_ADD(parse_n_ascii_literal__should_parse_double_quote);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_opening_quote_is_missing)
{
    n_ascii c;

    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_OPEN_QUOTE,
        parse_n_ascii_literal("t\'a", &c)
    );
    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_OPEN_QUOTE,
        parse_n_ascii_literal("\\t\'a", &c)
    );
    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_closing_quote_is_missing)
{
    n_ascii c;

    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE,
        parse_n_ascii_literal("\'t", &c)
    );
    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE,
        parse_n_ascii_literal("\'\\t", &c)
    );
    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal__should_fail_when_quotes_are_missing)
{
    EMU_ADD(parse_n_ascii_literal__should_fail_when_opening_quote_is_missing);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_closing_quote_is_missing);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_ascii_literal__should_fail_when_type_is_missing)
{
    n_ascii c;

    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_TYPE,
        parse_n_ascii_literal("\'k\'", &c)
    );

    EMU_EXPECT_EQ(
        PARSE_N_ASCII_LITERAL_MISSING_TYPE,
        parse_n_ascii_literal("\'\\n\'", &c)
    );

    EMU_END_TEST();
}

EMU_TEST(parse_n_ascii_literal__should_correctly_populate_the_length_of_the_string)
{
    n_ascii c;
    ssize_t len;

    len = parse_n_ascii_literal("\'t\'a", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("\'t\'a"), (size_t) len);

    len = parse_n_ascii_literal("\'\\t\'a", &c);
    EMU_ASSERT_GT_INT(len, 0);
    EMU_ASSERT_EQ_UINT(strlen("\'\\t\'a"), (size_t) len);

    EMU_END_TEST();
}

EMU_GROUP(parse_n_ascii_literal)
{
    EMU_ADD(parse_n_ascii_literal__should_fail_to_parse_garbage);
    EMU_ADD(parse_n_ascii_literal__should_parse_all_printible_ascii_characters_except_single_and_double_quote);
    EMU_ADD(parse_n_ascii_literal__should_parse_all_escape_characters);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_quotes_are_missing);
    EMU_ADD(parse_n_ascii_literal__should_fail_when_type_is_missing);
    EMU_ADD(parse_n_ascii_literal__should_correctly_populate_the_length_of_the_string);
    EMU_END_GROUP();
}

//============================================================================//
//      STRING                                                                //
//============================================================================//
EMU_TEST(parse_n_string_literal__should_fail_to_parse_garbage)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);

    EMU_EXPECT_LE_INT(parse_n_astr_literal(LOREM_IPSUM, &nstr), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_opening_double_quote_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);

    EMU_EXPECT_EQ_INT(
        PARSE_N_ASTR_LITERAL_MISSING_OPEN_QUOTE,
        parse_n_astr_literal("foobar\"a", &nstr)
    );
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_closing_double_quote_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);

    EMU_EXPECT_EQ_INT(
        PARSE_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE,
        parse_n_astr_literal("\"foobara", &nstr)
    );
    EMU_END_TEST();
}

EMU_TEST(parse_n_string_literal__should_fail_when_type_is_missing)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);

    EMU_EXPECT_EQ_INT(
        PARSE_N_ASTR_LITERAL_MISSING_TYPE,
        parse_n_astr_literal("\"foobar\"", &nstr)
    );
    EMU_END_TEST();
}

EMU_GROUP(parse_n_string_literal__should_fail_when_quotes_are_missing)
{
    EMU_ADD(parse_n_string_literal__should_fail_when_opening_double_quote_is_missing);
    EMU_ADD(parse_n_string_literal__should_fail_when_closing_double_quote_is_missing);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_string_literal__should_correctly_populate_the_length_of_the_string)
{
    nstr_t nstr_defer_fini nstr;
    nstr_init(&nstr);
    ssize_t len;

    len = parse_n_astr_literal("\"abcde\"a", &nstr);
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("\"abcde\"a"), len);
    EMU_EXPECT_TRUE(cstr_eq("abcde", nstr.data));

    len = parse_n_astr_literal("\"\\a\\b\\t\\v\"a", &nstr);
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("\"\\a\\b\\t\\v\"a"), len);
    EMU_EXPECT_TRUE(cstr_eq("\a\b\t\v", nstr.data));

    len = parse_n_astr_literal("\"\\\'\\\"\"a", &nstr);
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("\"\\\'\\\"\"a"), len);
    EMU_EXPECT_TRUE(cstr_eq("\'\"", nstr.data));

    EMU_END_TEST();
}

EMU_GROUP(parse_n_string_literal)
{
    EMU_ADD(parse_n_string_literal__should_fail_to_parse_garbage);
    EMU_ADD(parse_n_string_literal__should_fail_when_quotes_are_missing);
    EMU_ADD(parse_n_string_literal__should_fail_when_type_is_missing);
    EMU_ADD(parse_n_string_literal__should_correctly_populate_the_length_of_the_string);
    EMU_END_GROUP();
}

//============================================================================//
//      NUMERIC                                                               //
//============================================================================//
//====================================//
//      INTEGER                       //
//====================================//

EMU_TEST(parse_n_integer_literal__should_fail_to_parse_garbage)
{
    EMU_EXPECT_LE(parse_n_integer_literal(LOREM_IPSUM, &val), 0);
    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_fail_to_parse_string_prefixed_by_a_letter)
{
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10u8", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10u16", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10u32", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10u64", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10u", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10s8", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10s16", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10s32", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10s64", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("A10s", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("foobar10u", &val));

    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_fail_to_parse_string_prefixed_by_an_underscore)
{
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10u8", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10u16", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10u32", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10u64", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10u", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10s8", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10s16", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10s32", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10s64", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("_10s", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_PREFIX, parse_n_integer_literal("___10u", &val));

    EMU_END_TEST();
}

EMU_GROUP(parse_n_integer_literal__prefix_failures)
{
    EMU_ADD(parse_n_integer_literal__should_fail_to_parse_string_prefixed_by_a_letter);
    EMU_ADD(parse_n_integer_literal__should_fail_to_parse_string_prefixed_by_an_underscore);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_integer_literal__should_fail_if_integer_is_followed_by_an_unknown_letter)
{
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u8A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u16A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u32A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u64A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10uA", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s8A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s16A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s32A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s64A", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10sA", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10uu", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10ufoobar", &val));

    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_fail_if_integer_is_followed_by_an_underscore)
{
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u8_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u16_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u32_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u64_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s8_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s16_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s32_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s64_", &val));
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s_", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u___", &val));

    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_fail_if_integer_suffix_does_not_match_a_know_type)
{
    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10u80", &val));

    EMU_EXPECT_EQ(PARSE_N_INTEGER_INVALID_SUFFIX, parse_n_integer_literal("10s101", &val));

    EMU_END_TEST();
}

EMU_GROUP(parse_n_integer_literal__suffix_failures)
{
    EMU_ADD(parse_n_integer_literal__should_fail_if_integer_is_followed_by_an_unknown_letter);
    EMU_ADD(parse_n_integer_literal__should_fail_if_integer_is_followed_by_an_underscore);
    EMU_ADD(parse_n_integer_literal__should_fail_if_integer_suffix_does_not_match_a_know_type);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_integer_literal__should_recognize_no_prefix)
{
    EMU_ASSERT_GE(parse_n_integer_literal("10u", &val), 0);
    EMU_EXPECT_EQ_UINT(val.u, 10);

    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_recognize_hex_prefixes)
{
    EMU_ASSERT_GE(parse_n_integer_literal("0x10u", &val), 0);
    EMU_EXPECT_EQ_UINT(val.u, 16);

    EMU_ASSERT_GE(parse_n_integer_literal("0X10u", &val), 0);
    EMU_EXPECT_EQ_UINT(val.u, 16);

    EMU_END_TEST();
}

EMU_TEST(parse_n_integer_literal__should_recognize_binary_prefixes)
{
    EMU_ASSERT_GE(parse_n_integer_literal("0b10u", &val), 0);
    EMU_EXPECT_EQ_UINT(val.u, 2);

    EMU_ASSERT_GE(parse_n_integer_literal("0B10u", &val), 0);
    EMU_EXPECT_EQ_UINT(val.u, 2);

    EMU_END_TEST();
}

EMU_GROUP(parse_n_integer_literal__should_recognize_prefixes)
{
    EMU_ADD(parse_n_integer_literal__should_recognize_no_prefix);
    EMU_ADD(parse_n_integer_literal__should_recognize_hex_prefixes);
    EMU_ADD(parse_n_integer_literal__should_recognize_binary_prefixes);
    EMU_END_GROUP();
}

EMU_TEST(parse_n_integer_literal__should_correctly_populate_the_length_of_the_string)
{
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("1234567890u64"), parse_n_integer_literal("1234567890u64", &val));
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("-1234567890s64"), parse_n_integer_literal("-1234567890s64", &val));
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("0xdeadbeefu32"), parse_n_integer_literal("0xdeadbeefu32", &val));
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("-0xdeadbeefs32"), parse_n_integer_literal("-0xdeadbeefs32", &val));
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("0b10010110u8"), parse_n_integer_literal("0b10010110u8", &val));
    EMU_ASSERT_EQ_INT((ssize_t) cstr_len("-0b10010110s8"), parse_n_integer_literal("-0b10010110s8", &val));

    EMU_END_TEST();
}

EMU_GROUP(parse_n_integer_literal)
{
    EMU_ADD(parse_n_integer_literal__should_fail_to_parse_garbage);
    EMU_ADD(parse_n_integer_literal__prefix_failures);
    EMU_ADD(parse_n_integer_literal__suffix_failures);
    EMU_ADD(parse_n_integer_literal__should_recognize_prefixes);
    EMU_ADD(parse_n_integer_literal__should_correctly_populate_the_length_of_the_string);
    EMU_END_GROUP();
}

//====================================//
//      FLOATING POINT                //
//====================================//
EMU_TEST(match_n_float_literal__should_fail_to_parse_garbage)
{
    EMU_EXPECT_LE(match_n_float_literal(LOREM_IPSUM, NULL, NULL), 0);
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_to_parse_string_prefixed_by_a_letter)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_DIGIT, match_n_float_literal("A10.0f32", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_DIGIT, match_n_float_literal("A10.0f64", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_to_parse_string_prefixed_by_an_underscore)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_DIGIT, match_n_float_literal("_10.0f32", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_DIGIT, match_n_float_literal("_10.0f64", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_float_is_followed_by_an_unknown_letter)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f32A", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f64A", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_float_is_followed_by_an_underscore)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f32_", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f64_", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_float_suffix_does_not_match_a_know_type)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f100", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_INVALID_SUFFIX, match_n_float_literal("10.0f40", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_there_are_no_digits_before_the_decimal_point)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_NO_DIGITS_BEFORE_DECIMAL_POINT, match_n_float_literal(".0f32", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_NO_DIGITS_BEFORE_DECIMAL_POINT, match_n_float_literal(".0f64", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_there_are_no_digits_after_the_decimal_point)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_NO_DIGITS_AFTER_DECIMAL_POINT, match_n_float_literal("10.f32", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_NO_DIGITS_AFTER_DECIMAL_POINT, match_n_float_literal("10.f64", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_fail_if_there_is_no_decimal_point)
{
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_MISSING_DECIMAL_POINT, match_n_float_literal("10f32", NULL, NULL));
    EMU_EXPECT_EQ_INT(MATCH_N_FLOAT_MISSING_DECIMAL_POINT, match_n_float_literal("10f64", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_correctly_return_the_length_of_the_string)
{
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("10.0f32"), match_n_float_literal("10.0f32", NULL, NULL));
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("10.0e+1f32"), match_n_float_literal("10.0e+1f32", NULL, NULL));
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("10.0e-1f32"), match_n_float_literal("10.0e-1f32", NULL, NULL));
    EMU_EXPECT_EQ_INT((ssize_t) cstr_len("3.14159f32"), match_n_float_literal("3.14159f32", NULL, NULL));
    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_correctly_populate_the_float_type)
{
    enum n_float_type floattype;

    match_n_float_literal("10.0f32", &floattype, NULL);
    EMU_ASSERT_EQ_INT(N_FLOAT_F32, floattype);

    match_n_float_literal("10.0f64", &floattype, NULL);
    EMU_ASSERT_EQ_INT(N_FLOAT_F64, floattype);

    EMU_END_TEST();
}

EMU_TEST(match_n_float_literal__should_correctly_populate_the_float_format)
{
    enum n_float_fmt floatfmt;

    match_n_float_literal("10.0f32", NULL, &floatfmt);
    EMU_ASSERT_EQ_INT(N_FLOAT_FMT_DECIMAL, floatfmt);

    match_n_float_literal("10.0e1f32", NULL, &floatfmt);
    EMU_ASSERT_EQ_INT(N_FLOAT_FMT_SCIENTIFIC, floatfmt);

    EMU_END_TEST();
}

EMU_GROUP(match_n_float_literal)
{
    EMU_ADD(match_n_float_literal__should_fail_to_parse_garbage);
    EMU_ADD(match_n_float_literal__should_fail_to_parse_string_prefixed_by_a_letter);
    EMU_ADD(match_n_float_literal__should_fail_to_parse_string_prefixed_by_an_underscore);
    EMU_ADD(match_n_float_literal__should_fail_if_float_is_followed_by_an_unknown_letter);
    EMU_ADD(match_n_float_literal__should_fail_if_float_is_followed_by_an_underscore);
    EMU_ADD(match_n_float_literal__should_fail_if_float_suffix_does_not_match_a_know_type);
    EMU_ADD(match_n_float_literal__should_fail_if_there_are_no_digits_before_the_decimal_point);
    EMU_ADD(match_n_float_literal__should_fail_if_there_are_no_digits_after_the_decimal_point);
    EMU_ADD(match_n_float_literal__should_fail_if_there_is_no_decimal_point);
    EMU_ADD(match_n_float_literal__should_correctly_return_the_length_of_the_string);
    EMU_ADD(match_n_float_literal__should_correctly_populate_the_float_type);
    EMU_ADD(match_n_float_literal__should_correctly_populate_the_float_format);
    EMU_END_GROUP();
}


//============================================================================//

EMU_GROUP(primitives_h)
{
    EMU_ADD(parse_n_ascii);
    EMU_ADD(parse_n_ascii_literal);
    EMU_ADD(parse_n_string_literal);
    EMU_ADD(parse_n_integer_literal);
    EMU_ADD(match_n_float_literal);
    EMU_END_GROUP();
}
