/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(std__import__std_n)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
import \"cstd/cstd.n\"a;                                                      \n\
import \"posix/posix.n\"a;                                                    \n\
import \"std/std.n\"a;                                                        \n\
func entry : () -> void {}                                                   \n\
");
    EMU_END_TEST();
}

EMU_GROUP(std)
{
    EMU_ADD(std__import__std_n);
    EMU_END_GROUP();
}
