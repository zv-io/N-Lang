# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define the Ackermann function, A(m, n) as such:
#   For all integers m, n greater than or equal to zero.
#   A(m, n) = { n + 1                 if m == 0           }
#           = { A(m - 1, 1)           if m > 0 and n == 0 }
#           = { A(m - 1, A(m, n - 1)) if m > 0 and n > 0  }
func ackermann : (m : u, n : u) -> u
{
    # m == 0
    if m == 0u
    {
        return n + 1u;
    }

    # m > 0 and n == 0
    if n == 0u
    {
        return ackermann(m - 1u, 1u);
    }

    # m > 0 and n > 0
    return ackermann(m - 1u, ackermann(m, n - 1u));
}

func entry : (argc : u, argv : ^^ascii) -> u
{
    return ackermann(3u, 3u);
}
