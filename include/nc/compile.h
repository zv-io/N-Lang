/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "nir.h"

#define COMPILE_SUCCESS                   (0)
#define COMPILE_FAILURE_LEXICAL_ANALYSIS  (1)
#define COMPILE_FAILURE_SYNTAX_ANALYSIS   (2)
#define COMPILE_FAILURE_SEMANTIC_ANALYSIS (3)
#define COMPILE_FAILURE_CODEGEN           (4)
#define COMPILE_FAILURE_LINKING           (5)
#define COMPILE_FAILURE_OTHER             (6)
//! @param src_path
//!     File path of the @p module source.
//! @param module
//!     Module to be populated with the results of compilation.
//! @return
//!     #COMPILE_SUCCESS on success.
//! @return
//!     #COMPILE_FAILURE_LEXICAL_ANALYSIS if one or more errors occurred
//!     during lexical analysis.
//! @return
//!     #COMPILE_FAILURE_SYNTAX_ANALYSIS if one or more errors occurred
//!     during syntax analysis.
//! @return
//!     #COMPILE_FAILURE_SEMANTIC_ANALYSIS if one or more errors occurred
//!     during semantic analysis.
//! @return
//!     #COMPILE_FAILURE_CODEGEN if one or more errors occurred during code
//!     generation.
//! @return
//!     #COMPILE_FAILURE_LINKING if one or more errors occurred at link time.
//! @return
//!     #COMPILE_FAILURE_OTHER if one or more errors occurred outside of the
//!     compiler phases.
int compile(char const* src_path, struct module* const module);
//! Compile the primary module passed into the compiler.
//! Performs setup functions, calls #compile on the primary module, and then
//! performs teardown functions.
int compilep(char const* src_path, struct module* const module);
