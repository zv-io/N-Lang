/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "token.h"

//============================================================================//
//      ASCII CHARACTER                                                       //
//============================================================================//
//! Character type of the N language.
//! Valid characters cover the 7-bit ASCII character encoding within numeric
//! integer range [#N_ASCII_MIN, #N_ASCII_MAX].
typedef char n_ascii;
#define N_ASCII_MIN ((n_ascii)0x00)
#define N_ASCII_MAX ((n_ascii)0x7F)
#define N_ASCII_ESC ((n_ascii)0x1B) // '\e'

//! @return
//!     #true if the value of @p ch is within the range of valid characters.
bool n_ascii_is_valid(n_ascii ch);

#define N_ASCII_PRINTABLE_MIN 0x20
#define N_ASCII_PRINTABLE_MAX 0x7e
//! @return
//!     #true if the value of @p ch corresponds to a printable character.
bool n_ascii_is_printable(n_ascii ch);

//! Translate an N character into the cstring corresponding to that character's
//! representation in an N source file.
char const* n_ascii_to_n_source_char(n_ascii ch);

//! Translate an N character into the cstring corresponding to that character's
//! representation in a C source file.
char const* n_ascii_to_c_source_char(n_ascii ch);

//! As in C, unescaped characters in N are represented by their actual printable
//! ASCII characters. The character `F` in an N source file is `F`. Character
//! literals have the quotes around them, but those are character **literals**,
//! not characters.
#define N_ASCII_LEN_UNESCAPED (1)
//! Length of an unescaped character plus the escaping backslash `\`.
#define N_ASCII_LEN_ESCAPED (1 + N_ASCII_LEN_UNESCAPED)

#define MATCH_N_ASCII_FAILURE (-1)
//! Attempt to match an N character from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #MATCH_N_ASCII_FAILURE on failure.
ssize_t match_n_ascii(char const* start);

#define PARSE_N_ASCII_FAILURE MATCH_N_ASCII_FAILURE
//! Attempt to parse an N character from the provided cstring.
//! @param start
//!     Start of the string to parse.
//! @param nch
//!     Populated with the parsed character on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     #PARSE_N_ASCII_FAILURE on failure.
ssize_t parse_n_ascii(char const* start, n_ascii* nch);

#define MATCH_N_ASCII_LITERAL_BAD_CHAR              (-1)
#define MATCH_N_ASCII_LITERAL_MISSING_OPEN_QUOTE    (-2)
#define MATCH_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE (-3)
#define MATCH_N_ASCII_LITERAL_MISSING_TYPE          (-4)
//! Attempt to match an N character literal from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #MATCH_N_ASCII_LITERAL_BAD_CHAR if the character inside the single
//!     quotes was not parsed.
//! @return
//!     #MATCH_N_ASCII_LITERAL_MISSING_OPEN_QUOTE on failure due to a missing
//!     opening single quote.
//! @return
//!     #MATCH_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE on failure due to a missing
//!     closing single quote.
ssize_t match_n_ascii_literal(char const* start);

#define PARSE_N_ASCII_LITERAL_BAD_CHAR              MATCH_N_ASCII_LITERAL_BAD_CHAR
#define PARSE_N_ASCII_LITERAL_MISSING_OPEN_QUOTE    MATCH_N_ASCII_LITERAL_MISSING_OPEN_QUOTE
#define PARSE_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE MATCH_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE
#define PARSE_N_ASCII_LITERAL_MISSING_TYPE          MATCH_N_ASCII_LITERAL_MISSING_TYPE
//! Attempt to parse an N character literal from the provided cstring.
//! @param start
//!     Start of the string to parse.
//! @param nch
//!     Populated with the parsed N character on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     #PARSE_N_ASCII_LITERAL_BAD_CHAR if the character inside the single
//!     quotes was not parsed.
//! @return
//!     #PARSE_N_ASCII_LITERAL_MISSING_OPEN_QUOTE on failure due to a missing
//!     opening single quote.
//! @return
//!     #PARSE_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE on failure due to a missing
//!     closing single quote.
ssize_t parse_n_ascii_literal(char const* start, n_ascii* nch);

//============================================================================//
//      ASCII STRING                                                          //
//============================================================================//
#define MATCH_N_ASTR_LITERAL_BAD_CHAR              MATCH_N_ASCII_LITERAL_BAD_CHAR
#define MATCH_N_ASTR_LITERAL_MISSING_OPEN_QUOTE    MATCH_N_ASCII_LITERAL_MISSING_OPEN_QUOTE
#define MATCH_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE MATCH_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE
#define MATCH_N_ASTR_LITERAL_MISSING_TYPE          MATCH_N_ASCII_LITERAL_MISSING_TYPE
//! Attempt to match an N string literal from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #MATCH_N_ASTR_LITERAL_BAD_CHAR if some character inside the string
//!     failed to be parsed correctly.
//! @return
//!     #MATCH_N_ASTR_LITERAL_MISSING_OPEN_QUOTE on failure due to a missing
//!     opening double quote.
//! @return
//!     #MATCH_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE on failure due to a
//!     missing closing double quote.
ssize_t match_n_astr_literal(char const* start);

#define PARSE_N_ASTR_LITERAL_BAD_CHAR              MATCH_N_ASTR_LITERAL_BAD_CHAR
#define PARSE_N_ASTR_LITERAL_MISSING_OPEN_QUOTE    MATCH_N_ASTR_LITERAL_MISSING_OPEN_QUOTE
#define PARSE_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE MATCH_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE
#define PARSE_N_ASTR_LITERAL_MISSING_TYPE          MATCH_N_ASTR_LITERAL_MISSING_TYPE
//! Attempt to parse an N string literal from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @param nstr
//!     Populated with the characters of the parsed N string literal (without
//!     the opening and closing quotes) on success.
//! @return
//!     Number of characters (non-negative) from @p start parsed on success.
//! @return
//!     #PARSE_N_ASTR_LITERAL_BAD_CHAR if some character inside the string
//!     failed to be parsed correctly.
//! @return
//!     #PARSE_N_ASTR_LITERAL_MISSING_OPEN_QUOTE on failure due to a missing
//!     opening double quote.
//! @return
//!     #PARSE_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE on failure due to a
//!     missing closing double quote.
ssize_t parse_n_astr_literal(char const* start, nstr_t* nstr);

//============================================================================//
//      NUMERIC                                                               //
//============================================================================//
//! @return
//!     #true if @p ch is a valid decimal digit.
//! @return
//!     #false otherwise.
bool is_digit_dec(char ch);
//! @return
//!     #true if @p ch is a valid hexadecimal digit.
//! @return
//!     #false otherwise.
bool is_digit_hex(char ch);
//! @return
//!     #true if @p char is a valid binary digit.
//! @return
//!     #false otherwise.
bool is_digit_bin(char ch);

//! Check whether the character prefixing a number indicates that the number
//! should be negative. This macro is intended to be used to check the first
//! character of a numeric string being matched/parsed.
//! @param p_curr
//!     Pointer to the current character being being parsed.
//!     @br
//!     `p_curr` is iterated by one character if `*p_curr` is `+` or `-` such
//!     that after any call to #IS_DIGIT_NEGATIVE_INDICATIVE the
//!     matching/parsing algorithm using this macro will have consumed the
//!     character.
//! @return
//!     #true if `*p_curr` is the negative sign `-`.
//! @return
//!     #false if `*p_curr` is the positive sign `+`.
//! @return
//!     #false otherwise. Traditionally the absence of a either a positive
//!     or negative sign means that a number is implicitly positive.
#define IS_DIGIT_NEGATIVE_INDICATIVE(p_curr)                                   \
__extension__({                                                                \
    bool _is_negative;                                                         \
    if (*p_curr == '+')                                                        \
    {                                                                          \
        _is_negative = false;                                                  \
        p_curr += 1;                                                           \
    }                                                                          \
    else if (*p_curr == '-')                                                   \
    {                                                                          \
        _is_negative = true;                                                   \
        p_curr += 1;                                                           \
    }                                                                          \
    else                                                                       \
    {                                                                          \
        _is_negative = false;                                                  \
    }                                                                          \
    /*return*/_is_negative;                                                    \
})

//! Convert the provided, decimal, hexadecimal, or binary digit to an integer
//! value.
//! @param digit
//!     Digit to convert.
//! @return
//!     Integer representation of @p digit on success.
//! @return
//!     `-1` if digit is not a valid decimal, hexadecimal, or binary digit.
int digit_to_int(char digit);

//====================================//
//      INTEGER                       //
//====================================//
typedef uint8_t  n_u8;
typedef uint16_t n_u16;
typedef uint32_t n_u32;
typedef uint64_t n_u64;
typedef int8_t   n_s8;
typedef int16_t  n_s16;
typedef int32_t  n_s32;
typedef int64_t  n_s64;

#define U8_BITS  8
#define U16_BITS 16
#define U32_BITS 32
#define U64_BITS 64
#define S8_BITS  8
#define S16_BITS 16
#define S32_BITS 32
#define S64_BITS 64

// Data types `u` and `s` are machine dependent sizes, but will be stored in the
// maximum bit width integer within the C language for machine independent
// processing/manipulation.
typedef uintmax_t n_u; //!< @note The actual size of #n_u is machine dependent.
typedef intmax_t  n_s; //!< @note The actual size of #n_s is machine dependent.
static_assert(
    sizeof(n_u) == sizeof(n_s),
    "u and s types must have the same bit width"
);

enum n_integer_type
{
    N_INTEGER_U8,
    N_INTEGER_U16,
    N_INTEGER_U32,
    N_INTEGER_U64,
    N_INTEGER_U,
    N_INTEGER_S8,
    N_INTEGER_S16,
    N_INTEGER_S32,
    N_INTEGER_S64,
    N_INTEGER_S,
};
#define N_INTEGER_U8_STR  "u8"
#define N_INTEGER_U16_STR "u16"
#define N_INTEGER_U32_STR "u32"
#define N_INTEGER_U64_STR "u64"
#define N_INTEGER_U_STR   "u"
#define N_INTEGER_S8_STR  "s8"
#define N_INTEGER_S16_STR "s16"
#define N_INTEGER_S32_STR "s32"
#define N_INTEGER_S64_STR "s64"
#define N_INTEGER_S_STR   "s"

#define N_INTEGER_U8_STR_LEN  2 //!< u8
#define N_INTEGER_U16_STR_LEN 3 //!< u16
#define N_INTEGER_U32_STR_LEN 3 //!< u32
#define N_INTEGER_U64_STR_LEN 3 //!< u64
#define N_INTEGER_U_STR_LEN   1 //!< u
#define N_INTEGER_S8_STR_LEN  2 //!< s8
#define N_INTEGER_S16_STR_LEN 3 //!< s16
#define N_INTEGER_S32_STR_LEN 3 //!< s32
#define N_INTEGER_S64_STR_LEN 3 //!< s64
#define N_INTEGER_S_STR_LEN   1 //!< s

enum n_integer_fmt
{
    N_INTEGER_FMT_DEC,
    N_INTEGER_FMT_HEX,
    N_INTEGER_FMT_BIN
};
#define N_INTEGER_HEX_PREFIX_LOWER "0x"
#define N_INTEGER_HEX_PREFIX_UPPER "0X"
#define N_INTEGER_BIN_PREFIX_LOWER "0b"
#define N_INTEGER_BIN_PREFIX_UPPER "0B"
#define N_INTEGER_HEX_PREFIX_LEN 2 //!< 0x or 0X
#define N_INTEGER_BIN_PREFIX_LEN 2 //!< 0b or 0B

//! Get the suffix length of an integer of the provided n integer type.
size_t n_integer_suffix_len(enum n_integer_type inttype);

//! Variant representing one of the N-lang integer types.
struct n_integer_value
{
    //! Variant tag.
    enum n_integer_type tag;

    //! Actual value of the integer.
    union
    {
        n_u8  u8;
        n_u16 u16;
        n_u32 u32;
        n_u64 u64;
        n_u   u;
        n_s8  s8;
        n_s16 s16;
        n_s32 s32;
        n_s64 s64;
        n_s   s;
    };
};

#define MATCH_N_INTEGER_INVALID_PREFIX   (-1)
#define MATCH_N_INTEGER_INVALID_SUFFIX   (-2)
#define MATCH_N_INTEGER_INVALID_DIGIT    (-3)
#define MATCH_N_INTEGER_NO_DIGITS_PARSED (-4)
#define MATCH_N_INTEGER_INVALID_NEGATIVE (-5)
//! Attempt to match an N integer from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @param inttype @nullable
//!     Will be populated with the integer type if non-`NULL`.
//! @param intfmt @nullable
//!     Will be populated with the integer format if non-`NULL`.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #MATCH_N_INTEGER_INVALID_PREFIX if the integer starts with an invalid
//!     prefix. Valid prefixes are "0x", "0X", "0b", "0B", or no prefix.
//!     An example of an integer string with an invalid prefix is "bar123u8".
//! @return
//!     #MATCH_N_INTEGER_INVALID_SUFFIX if the integer ends with an invalid
//!     suffix.
//! @return
//!     #MATCH_N_INTEGER_INVALID_DIGIT if the digit portion of the integer
//!     contains a character that is not a valid digit matching the prefix-type
//!     of the integer.
//! @return
//!     #MATCH_N_INTEGER_NO_DIGITS_PARSED if the digit portion of the integer
//!     contains no digits. An integer must contain at least one digit.
//! @return
//!     #MATCH_N_INTEGER_INVALID_NEGATIVE if the integer is of an unsigned type
//!     but is prefixed with a negative sign.
ssize_t match_n_integer_literal(
    char const* start,
    enum n_integer_type* inttype,
    enum n_integer_fmt* intfmt
);

#define PARSE_N_INTEGER_INVALID_PREFIX   MATCH_N_INTEGER_INVALID_PREFIX
#define PARSE_N_INTEGER_INVALID_SUFFIX   MATCH_N_INTEGER_INVALID_SUFFIX
#define PARSE_N_INTEGER_INVALID_DIGIT    MATCH_N_INTEGER_INVALID_DIGIT
#define PARSE_N_INTEGER_NO_DIGITS_PARSED MATCH_N_INTEGER_NO_DIGITS_PARSED
#define PARSE_N_INTEGER_INVALID_NEGATIVE PARSE_N_INTEGER_NO_DIGITS_PARSED
//! Attempt to parse an N integer from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @param intval
//!     Populated with the value of the parsed integer.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #PARSE_N_INTEGER_INVALID_PREFIX if the integer starts with an invalid
//!     prefix. Valid prefixes are "0x", "0X", "0b", "0B", or no prefix.
//!     An example of an integer string with an invalid prefix is "bar123u8".
//! @return
//!     #PARSE_N_INTEGER_INVALID_SUFFIX if the integer ends with an invalid
//!     suffix.
//! @return
//!     #PARSE_N_INTEGER_INVALID_DIGIT if the digit portion of the integer
//!     contains a character that is not a valid digit matching the prefix-type
//!     of the integer.
//! @return
//!     #PARSE_N_INTEGER_NO_DIGITS_PARSED if the digit portion of the integer
//!     contains no digits. An integer must contain at least one digit.
//! @return
//!     #PARSE_N_INTEGER_INVALID_NEGATIVE if the integer is of an unsigned type
//!     but is prefixed with a negative sign.
ssize_t parse_n_integer_literal(
    char const* start,
    struct n_integer_value* intval
);

//====================================//
//      FLOATING POINT                //
//====================================//
typedef float  n_f32;
typedef double n_f64;

#define F32_BITS 32
#define F64_BITS 64

static_assert(
    sizeof(n_f32) == 32/CHAR_BITS,
    "f32 must have a bit width of 32."
);
static_assert(
    sizeof(n_f64) == 64/CHAR_BITS,
    "f64 must have a bit width of 64."
);

enum n_float_type
{
    N_FLOAT_F32,
    N_FLOAT_F64
};
#define N_FLOAT_F32_STR "f32"
#define N_FLOAT_F64_STR "f64"

#define N_FLOAT_F32_STR_LEN 3 //!< f32
#define N_FLOAT_F64_STR_LEN 3 //!< f64
//! The suffix length of both types of floating point numbers is the same.
#define N_FLOAT_SUFFIX_LEN 3 //!< f32 or f64

enum n_float_fmt
{
    N_FLOAT_FMT_DECIMAL,
    N_FLOAT_FMT_SCIENTIFIC
};

#define MATCH_N_FLOAT_INVALID_SUFFIX                 (-1)
#define MATCH_N_FLOAT_INVALID_DIGIT                  (-2)
#define MATCH_N_FLOAT_INVALID_SCIENTIFIC_NOTATION    (-3)
#define MATCH_N_FLOAT_MISSING_DECIMAL_POINT          (-4)
#define MATCH_N_FLOAT_NO_DIGITS_BEFORE_DECIMAL_POINT (-5)
#define MATCH_N_FLOAT_NO_DIGITS_AFTER_DECIMAL_POINT  (-6)
//! Attempt to match an N floating point number from the provided cstring.
//! @param start
//!     Start of the string to match.
//! @param floattype @nullable
//!     Will be populated with the float type if non-`NULL`.
//! @param floatfmt @nullable
//!     Will be populated with the float format if non-`NULL`.
//! @return
//!     Number of characters (non-negative) from @p start matched on success.
//! @return
//!     #MATCH_N_FLOAT_INVALID_SUFFIX if the float ends with an invalid suffix.
//! @return
//!     #MATCH_N_FLOAT_INVALID_DIGIT if any digit portion of the float contains
//!     a character that is not a valid digit.
//! @return
//!     #MATCH_N_FLOAT_INVALID_SCIENTIFIC_NOTATION if the optional scientific
//!     notation portion of the float is misformatted.
//! @return
//!     #MATCH_N_FLOAT_MISSING_DECIMAL_POINT if the float does not contain a
//!     decimal point.
//! @return
//!     #MATCH_N_FLOAT_NO_DIGITS_BEFORE_DECIMAL_POINT if the float does not have
//!     at least one digit before the decimal point.
//! @return
//!     #MATCH_N_FLOAT_NO_DIGITS_AFTER_DECIMAL_POINT if the float does not have
//!     at least one digit after the decimal point.
ssize_t match_n_float_literal(
    char const* start,
    enum n_float_type* floattype,
    enum n_float_fmt* floatfmt
);

//============================================================================//

//// Forward declarations of structs/enums describing the N-grammar/IR.
//// These structures, declared here and defined below, sufficiently describe
//// an in-memory representation of the N language.
// IDENTIFIER
struct id;
// QUALIFIERS
struct qualifiers;
// DATA TYPES
struct dt;
struct member_var;
struct member_func;
struct struct_def;
struct func_sig;
// LITERAL VALUE
struct literal;
// EXPRESSION
struct expr;
// IN-SOURCE IDENTIFIER DECLARATION
struct var_decl;
struct func_decl;
struct struct_decl;
// CONTROL CONSTRUCTS
struct if_elif_else;
struct loop;
// ALIAS
struct alias;
// STATEMENT
struct stmt;
// LEXICAL SCOPE
struct symbol_table;
struct symbol;
struct scope;
// MODULE
struct module;

#define NIR_INIT_FINI_COUNTER_VERIFY_BALANCED()                                \
    INIT_FINI_COUNTER_VERIFY_BALANCED(id);                                     \
    INIT_FINI_COUNTER_VERIFY_BALANCED(qualifiers);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(dt);                                     \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_var);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(member_func);                            \
    INIT_FINI_COUNTER_VERIFY_BALANCED(struct_def);                             \
    INIT_FINI_COUNTER_VERIFY_BALANCED(func_sig);                               \
    INIT_FINI_COUNTER_VERIFY_BALANCED(literal);                                \
    INIT_FINI_COUNTER_VERIFY_BALANCED(expr);                                   \
    INIT_FINI_COUNTER_VERIFY_BALANCED(var_decl);                               \
    INIT_FINI_COUNTER_VERIFY_BALANCED(func_decl);                              \
    INIT_FINI_COUNTER_VERIFY_BALANCED(indicative_conditional);                 \
    INIT_FINI_COUNTER_VERIFY_BALANCED(if_elif_else);                           \
    INIT_FINI_COUNTER_VERIFY_BALANCED(loop);                                   \
    INIT_FINI_COUNTER_VERIFY_BALANCED(alias);                                  \
    INIT_FINI_COUNTER_VERIFY_BALANCED(stmt);                                   \
    INIT_FINI_COUNTER_VERIFY_BALANCED(symbol_table);                           \
    INIT_FINI_COUNTER_VERIFY_BALANCED(symbol);                                 \
    INIT_FINI_COUNTER_VERIFY_BALANCED(scope);                                  \
    INIT_FINI_COUNTER_VERIFY_BALANCED(module);

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
//! Identifier.
struct id
{
    //! Pointer to the first character of this identifier in the source file it
    //! was parsed from.
    //! @note
    //!     It is the responsibility of the programmer to ensure the memory
    //!     pointed to by #start is valid.
    char const* start;

    //! Number of characters that make up the identifier.
    size_t length;

    //! Pointer to the token that this identifier was parsed from.
    //! #NULL if this identifier was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(id);
//! @init
void id_init(struct id* id);
//! @fini
void id_fini(struct id* id);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `dest->srctok` to #NULL if #true, indicating that @p dest was
//!     created via a transformation.
void id_assign(
    struct id* dest,
    struct id const* src,
    bool clear_srctok
);
//! @eq
//! @return
//!     #true if strings that make up identifiers @p A and @p B are equivalent.
bool id_eq(
    struct id const* A,
    struct id const* B
);

//============================================================================//
//      QUALIFIERS                                                            //
//============================================================================//
struct qualifiers
{
    //! Indicates the qualified subject is modifiable.
    bool is_mut;

    //! Pointer to the first token the qualifiers were parsed from.
    //! For most cases this will be the first qualifier in the list of
    //! qualifiers within a data type.
    //! #NULL if the qualifiers were created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(qualifiers);
//! @init
void qualifiers_init(struct qualifiers* q);
//! @fini
void qualifiers_fini(struct qualifiers* q);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `dest->srctok` to #NULL if #true, indicating that @p dest was
//!     created via a transformation.
void qualifiers_assign(
    struct qualifiers* dest,
    struct qualifiers const* src,
    bool clear_srctok
);
//! @eq
//! @return
//!     #true if qualifiers @p A and @p B are equivalent.
bool qualifiers_eq(
    struct qualifiers const* A,
    struct qualifiers const* B
);

//! Populates @p dest with the union of the qualifier sets @p A and @p B.
//! This function will NOT change the srctok of @p dest.
void qualifers_union(
    struct qualifiers* dest,
    struct qualifiers const* A,
    struct qualifiers const* B
);

//! Check if the provided qualifiers are equivalent to default qualifiers on
//! a type.
//! @return
//!     #true if @p q is default qualifiers.
bool qualifiers_is_default(struct qualifiers const* q);

//! Check whether the qualifier set @p sub is a non-strict subset of the
//! qualifier set @p sup.
//! @return
//!     #true if @p sub is a non-strict subset of @p sup.
bool qualifiers_is_subset(
    struct qualifiers const* sub,
    struct qualifiers const* sup
);

//! Check whether the qualifier transformation @p from --> @p to is a
//! restricting conversion.
//! @return
//!     #true if @p from is equally or less restrictive than @p to.
bool qualifiers_is_restricting(
    struct qualifiers const* from,
    struct qualifiers const* to
);

//============================================================================//
//      DATA TYPES                                                            //
//============================================================================//
//====================================//
//      DATA TYPE                     //
//====================================//
struct dt
{
    enum
    {
        DT_UNSET,

        DT_VOID,  //!< `void`
        DT_U8,    //!< `u8`
        DT_U16,   //!< `u16`
        DT_U32,   //!< `u32`
        DT_U64,   //!< `u64`
        DT_U,     //!< `u`
        DT_S8,    //!< `s8`
        DT_S16,   //!< `s16`
        DT_S32,   //!< `s32`
        DT_S64,   //!< `s64`
        DT_S,     //!< `s`
        DT_F16,   //!< `f16`
        DT_F32,   //!< `f32`
        DT_F64,   //!< `f64`
        DT_F128,  //!< `f128`
        DT_BOOL,  //!< `bool`
        DT_ASCII, //!< `ascii`

        DT_STRUCT, //!< `struct`

        DT_FUNCTION,

        DT_POINTER,
        DT_ARRAY,

        DT_UNRESOLVED_ID,
        DT_UNRESOLVED_TYPEOF_EXPR,
        DT_UNRESOLVED_TYPEOF_DT
    } tag;

    //! Qualifiers of this data type.
    struct qualifiers qualifiers;

    union
    {
        //! #DT_STRUCT
        struct struct_decl* struct_ref;

        //! #DT_FUNCTION
        /*!*/struct func_sig* func_sig;

        //! #DT_ARRAY
        struct
        {
            size_t length;
        } array;

        union
        {
            //! #DT_UNRESOLVED_ID
            struct id id;

            //! #DT_UNRESOLVED_TYPEOF_EXPR
            /*!*/struct expr* typeof_expr;

            //! #DT_UNRESOLVED_TYPEOF_DT
            /*!*/struct dt* typeof_dt;
        } unresolved;
    };

    //! Pointer to the first token the data type base was parsed from.
    //! #NULL if this data type base was created via a transformation.
    struct token const* srctok;

    //! Pointer to the next inner layer of this data type.
    /*!*/struct dt* inner;
};
INIT_FINI_COUNTER_HEADER(dt);
//! @init
void dt_init(struct dt* dt);
//! @fini
void dt_fini(struct dt* dt);
//! @reset
void dt_reset(struct dt* dt);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` members in @p dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void dt_assign(
    struct dt* dest,
    struct dt const* src,
    bool clear_srctok
);

//! Number of #dt structs that make up the @p dt, including the base type and
//! all data type modifiers.
size_t dt_length_all(struct dt const* dt);
//! Number of #dt structs that make up the modifiers of @p dt.
//! The expression `dt_length_modifiers(foo)` is equivalent to the expression
//! `dt_length_all(foo)-1`.
size_t dt_length_modifiers(struct dt const* dt);

//! @p modifier should be an initialized #dt allocated via #nalloc.
//! `modifier->inner` should NOT be set, as it will be set automatically.
void dt_push_modifier(struct dt* dt, struct dt* modifier);
//! Pop and free the outermost modifier of @p dt.
void dt_pop_modifier(struct dt* dt);

//! @return
//!     #true if dt is the `void` base type.
bool dt_is_void(struct dt const* dt);
//! @return
//!     #true if dt is one of the integer base types.
bool dt_is_integer(struct dt const* dt);
//! @return
//!     #true if dt is one of the unsigned integer base types.
bool dt_is_integer_unsigned(struct dt const* dt);
//! @return
//!     #true if dt is one of the signed integer base types.
bool dt_is_integer_signed(struct dt const* dt);
//! @return
//!     #true if dt is one of the floating point base types.
bool dt_is_float(struct dt const* dt);
//! @return
//!     #true if dt is a `bool` base type.
bool dt_is_bool(struct dt const* dt);
//! @return
//!     #true if dt is a `char` base type.
bool dt_is_char(struct dt const* dt);
//! @return
//!     #true if dt is a `struct` base type.
bool dt_is_struct(struct dt const* dt);
//! @return
//!     #true if dt is a function base type.
bool dt_is_function(struct dt const* dt);
//! @return
//!     #true if outermost modifier of @p dt is a pointer.
bool dt_is_pointer(struct dt const* dt);
//! @return
//!     #true if outermost modifier of the #dt is an array.
bool dt_is_array(struct dt const* dt);

//! @return
//!     #true if @p dt is solely a base type, i.e. dt has no modifiers.
bool dt_is_base(struct dt const* dt);
//! @return
//!     #true if @p dt modifies a base type.
bool dt_is_modifier(struct dt const* dt);

//! @return
//!     #true if dt is one of the integer or floating point base types.
bool dt_is_numeric(struct dt const* dt);
//! @return
//!     #true if dt is a void, arithmetic, bool, or character type.
bool dt_is_primitive(struct dt const* dt);
//! @return
//!     #true if dt is a type composed from a combination of other types.
bool dt_is_composite(struct dt const* dt);
//! @return
//!     #true if dt is a pointer to `void`.
bool dt_is_void_pointer(struct dt const* dt);

//! Returns a newly initialized #nstr_t containing @p dt formatted as a data
//! type within the N programming language.
nstr_t dt_to_n_source_nstr(struct dt const* dt);

//! Returns a newly initialized #nstr_t containing @p dt formatted as a data
//! type within the C programming language.
//! @param dt
//!     Target #dt.
//! @param id @nullable
//!     The identifier associated with @p dt.
//!     Parsing of data types in C is weird since portions of the data type go
//!     on either side of the identifier.
//!     To combat this jank, #dt_to_c_source_nstr can generate a type alongside
//!     its identifier to make variable and parameter declarations easier to
//!     translate.
//!     @br
//!     If #NULL then an identifier will not be included with the type.
nstr_t dt_to_c_source_nstr(struct dt const* dt, struct id const* id);

//! Check if data type @p from can be converted to data type @p to without
//! requiring an explicit cast from the user.
//!
//! ### List of allowable implicit conversions:
//!
//! #### Identity conversion
//!
//! By definition, any data type can be implicitly converted to itself.
//!
//! #### Qualifier restricting conversion
//!
//! Data type `Ta` can be converted to data type `Tb` if the qualifiers second
//! outer most modifier `Ta` are stricter then the corresponding qualifiers of
//! `Tb`, and all other inner qualifiers of `Ta` and `Tb` are equivalent.
//! This conversion requires `Ta` and `Tb` to have the same base type and
//! modifier types.
//!
//! ```
//! Valid qualifier restricting conversion examples:
//!
//! char          --> mut char
//! mut char      --> char
//! mut char      --> volatile char
//! mut char      --> volatile mut char
//! ^mut char     --> ^char
//! mut ^mut char --> ^char
//!
//! ----------------------------------------------
//!
//! Invalid qualifier restricting conversion examples:
//!
//! **char --> **mut char
//! ```
//!
//! #### Pointer conversion to `void` pointer
//!
//! Let `* Qa T` represent the data type 'pointer to `T`' where `Qa` is the set
//! of qualifiers on `T`.
//! @br
//! The implicit conversion `^ Qa T --> ^ Qb void` is allowed where qualifiers
//! `Qa` are equally or less restrictive than qualifiers `Qb`.
//!
//! #### Pointer conversion from `void` pointer
//!
//! Let `^ Qa void` represent the data type 'pointer to `void`' where `Qa` is
//! the set of qualifiers on `void`.
//! @br
//! The implicit conversion `^ Qa void --> ^ Qb T` is allowed where qualifiers
//! `Qa` are equally or less restrictive than qualifiers `Qb`.
//!
//! @return
//!     #true if data type @p from can be implicitly converted to data type
//!     @p to.
//! @note
//!     Conversions from `void` and conversions to `void` are always illegal,
//!     implicit or explicit.
bool dt_can_implicitly_convert(
    struct dt const* from,
    struct dt const* to
);

//! Similar to #dt_can_implicitly_convert, but accounts for array initialization
//! which is the exception to implicit conversion rules.
bool dt_can_implicitly_convert__variable_init(
    struct dt const* from,
    struct dt const* to
);

//! Check if data type @p from can be converted to data type @p to via an
//! explicit cast from the user.
//!
//! ### List of allowable explicit conversions:
//!
//! #### Any implicit conversion
//!
//! If an explicit conversion would perform the same conversion an implicit
//! conversion, then that conversion is obviously allowed.
//!
//! #### Integer to integer conversion
//!
//! Integers of any bitwidth may be converted to integers of any other bitwidth.
//! Downsizing truncates excess high bits.
//!
//! #### Char to integer conversion
//!
//! Characters may be converted to integers of any bitwidth.
//!
//! #### Integer to char conversion
//!
//! Integers of any bitwidth may be converted to a character.
//! Downsizing truncates excess high bits.
//!
//! #### Integer to floating point conversion.
//!
//! Integers of any bitwidth may be converted to any floating point type.
//! Precision may be lost as a result of the conversion.
//!
//! #### Floating point to integer conversion.
//!
//! Any bitwidth floating point type may be converted to any integer type.
//! The fractional component of the floating point value will be truncated.
//! Undefined behavior will result if the range of the integer type does not
//! contain the value of a floating point value.
//!
//! #### Pointer to integer conversion
//!
//! Convert a pointer to an integer of any bitwidth.
//! Downsizing truncates excess high bits.
//!
//! #### Integer to pointer conversion
//!
//! Convert an integer of any bitwidth to a pointer.
//! Downsizing truncates excess high bits.
//!
//! #### Pointer to pointer conversion
//!
//! Convert a pointer of any type to a pointer of any other type.
//!
//! @return
//!     #true if data type @p from can be explicitly converted to data type
//!     @p to.
//! @note
//!     Conversions from `void` and conversions to `void` are always illegal,
//!     implicit or explicit.
bool dt_can_explicitly_convert(
    struct dt const* from,
    struct dt const* to
);

#define DECLARE_DT_NSTR(_identifier, _p_dt) \
    nstr_t nstr_defer_fini _identifier = dt_to_n_source_nstr(_p_dt)

//====================================//
//      MEMBER VARIABLE               //
//====================================//
struct member_var
{
    //! Identifier of the member variable.
    struct id id;

    //! Data type of the member variable.
    struct dt dt;

    //! Pointer to the first token the member variable was parsed from.
    //! #NULL if this member variable was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_var);
//! @init
void member_var_init(struct member_var* memb);
//! @fini
void member_var_fini(struct member_var* memb);

//====================================//
//      MEMBER FUNCTION               //
//====================================//
struct member_func
{
    //! Identifier of the member function.
    struct id id;

    //! Data type of the member function.
    //! Set during semantic analysis.
    struct dt dt;

    struct
    {
        //! Identifier of the target function.
        //! Set during syntax analysis.
        struct id id;
    } target;

    //! Pointer to the first token the member function was parsed from.
    //! #NULL if this member function was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(member_func);
//! @init
void member_func_init(struct member_func* memb);
//! @fini
void member_func_fini(struct member_func* memb);

//====================================//
//      STRUCT DEFINITION             //
//====================================//
struct struct_def
{
    /*!*/narr_t(struct member_var) member_vars;

    /*!*/narr_t(struct member_func) member_funcs;

    //! Pointer to the first token the struct definition was parsed from.
    //! #NULL if this struct definition was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(struct_def);
//! @init
void struct_def_init(struct struct_def* def);
//! @fini
void struct_def_fini(struct struct_def* def);

#define MEMBER_VAR_NOT_FOUND (-1)
//! @return
//!     Non-negative index of the #member_var with identifier @p id in the
//!     member_vars list of @p def if it exists.
//! @return
//!     #MEMBER_VAR_NOT_FOUND if no member variable with identifier @p id exists
//!     within the member_vars list of @p def.
ssize_t struct_def_member_var_idx(
    struct struct_def const* def,
    struct id const* mid
);
#define MEMBER_FUNC_NOT_FOUND (-1)
//! @return
//!     Non-negative index of the #member_func with identifier @p id in the
//!     member_funcs list of @p def if it exists.
//! @return
//!     #MEMBER_FUNC_NOT_FOUND if no member function with identifier @p id
//!     exists within the member_funcs list of @p def.
ssize_t struct_def_member_func_idx(
    struct struct_def const* def,
    struct id const* id
);

//====================================//
//      FUNCTION SIGNATURE            //
//====================================//
//! Function signature.
struct func_sig
{
    struct dt rtn_dt;
    /*!*/narr_t(struct dt) param_dts;
};
INIT_FINI_COUNTER_HEADER(func_sig);
//! @init
void func_sig_init(struct func_sig* func_sig);
//! @fini
void func_sig_fini(struct func_sig* func_sig);
//! @reset
void func_sig_reset(struct func_sig* func_sig);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void func_sig_assign(
    struct func_sig* dest,
    struct func_sig const* src,
    bool clear_srctok
);

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
struct literal
{
    enum
    {
        LITERAL_UNSET,

        LITERAL_U8,
        LITERAL_U16,
        LITERAL_U32,
        LITERAL_U64,
        LITERAL_U,
        LITERAL_S8,
        LITERAL_S16,
        LITERAL_S32,
        LITERAL_S64,
        LITERAL_S,
        LITERAL_F32,
        LITERAL_F64,
        LITERAL_BOOL,
        LITERAL_CHAR,
        LITERAL_STR,
        LITERAL_NULL,
        LITERAL_ARRAY,
        LITERAL_ARRAY_FILL //!< Special value marking '..' in an array literal.
    } tag;

    union
    {
        n_u8   u8;
        n_u16  u16;
        n_u32  u32;
        n_u64  u64;
        n_u    u;
        n_s8   s8;
        n_s16  s16;
        n_s32  s32;
        n_s64  s64;
        n_s    s;
        struct
        {
            char const* start;
            size_t len;
        } f32;
        struct
        {
            char const* start;
            size_t len;
        } f64;
        nstr_t f64_ref;
        bool   boolean;
        n_ascii character;
        nstr_t str;
        /*!*/narr_t(struct literal) array;
        size_t array_fill_len;
    };

    //! Pointer to the first token the literal was parsed from.
    //! #NULL if this literal was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(literal);
//! @init
void literal_init(struct literal* literal);
//! @fini
void literal_fini(struct literal* literal);
//! @reset
void literal_reset(struct literal* literal);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void literal_assign(
    struct literal* dest,
    struct literal const* src,
    bool clear_srctok

);

//! Get the `typeof` the provided literal value.
//! @param literal
//!     Target literal.
struct dt literal_get_dt(struct literal const* literal);

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//
struct expr
{
    enum
    {
        EXPR_UNSET = 0,

        __EXPR_PRIMARY_BEGIN__,
        EXPR_PRIMARY_IDENTIFIER,
        EXPR_PRIMARY_LITERAL,
        EXPR_PRIMARY_PAREN,
        __EXPR_PRIMARY_END__,

        __EXPR_POSTFIX_BEGIN__,
        EXPR_POSTFIX_FUNCTION_CALL,
        EXPR_POSTFIX_SUBSCRIPT,
        EXPR_POSTFIX_ACCESS_DOT,
        EXPR_POSTFIX_ACCESS_ARROW,
        __EXPR_POSTFIX_END__,

        __EXPR_UNARY_BEGIN__,
        EXPR_UNARY_ADDR_OF,      //!< `?EXPR`
        EXPR_UNARY_DEREF,        //!< `@EXPR`
        EXPR_UNARY_DECAY,        //!< `$EXPR`
        EXPR_UNARY_PLUS,         //!< `+EXPR`
        EXPR_UNARY_MINUS,        //!< `-EXPR`
        EXPR_UNARY_BITWISE_NOT,  //!< `~EXPR`
        EXPR_UNARY_LOGICAL_NOT,  //!< `!EXPR`
        EXPR_UNARY_COUNTOF_EXPR, //!< `countof EXPR`
        EXPR_UNARY_SIZEOF_EXPR,  //!< `sizeof EXPR`
        EXPR_UNARY_COUNTOF_DT,   //!< `countof(DATA_TYPE)`
        EXPR_UNARY_SIZEOF_DT,    //!< `sizeof(DATA_TYPE)`
        __EXPR_UNARY_END__,

        __EXPR_BINARY_BEGIN__,
        EXPR_BINARY_ASSIGN,      //!< `EXPR = EXPR`
        EXPR_BINARY_LOGICAL_OR,  //!< `EXPR || EXPR`
        EXPR_BINARY_LOGICAL_AND, //!< `EXPR && EXPR`
        EXPR_BINARY_BITWISE_OR,  //!< `EXPR | EXPR`
        EXPR_BINARY_BITWISE_XOR, //!< `EXPR ^ EXPR`
        EXPR_BINARY_BITWISE_AND, //!< `EXPR & EXPR`
        EXPR_BINARY_REL_EQ,      //!< `EXPR == EXPR`
        EXPR_BINARY_REL_NE,      //!< `EXPR != EXPR`
        EXPR_BINARY_REL_LT,      //!< `EXPR < EXPR`
        EXPR_BINARY_REL_GT,      //!< `EXPR > EXPR`
        EXPR_BINARY_REL_LE,      //!< `EXPR <= EXPR`
        EXPR_BINARY_REL_GE,      //!< `EXPR >= EXPR`
        EXPR_BINARY_SHIFT_L,     //!< `EXPR << EXPR`
        EXPR_BINARY_SHIFT_R,     //!< `EXPR >> EXPR`
        EXPR_BINARY_PLUS,        //!< `EXPR + EXPR`
        EXPR_BINARY_MINUS,       //!< `EXPR - EXPR`
        EXPR_BINARY_PTR_DIFF,    //!< `EXPR -** EXPR`
        EXPR_BINARY_PTR_PLUS,    //!< `EXPR +* EXPR`
        EXPR_BINARY_PTR_MINUS,   //!< `EXPR -* EXPR`
        EXPR_BINARY_MULT,        //!< `EXPR * EXPR`
        EXPR_BINARY_DIV,         //!< `EXPR / EXPR`
        EXPR_BINARY_CAST,        //!< `EXPR as DATA_TYPE`
        __EXPR_BINARY_END__
    } tag;

    union
    {
        //! #EXPR_PRIMARY_IDENTIFIER
        struct id id;

        //! #EXPR_PRIMARY_LITERAL
        /*!*/struct literal* literal;

        //! #EXPR_PRIMARY_PAREN
        /*!*/struct expr* paren;

        //! Postfix expression.
        struct expr_postfix
        {
            //! Expr with function type in the case of a function call.
            //! @br
            //! Expr with array in the case of a subscript.
            //! @br
            //! Expr resolving to a stuct, union, or enum in the case of a
            //! member dot or member arrow.
            /*!*/struct expr* lhs;

            union
            {
                //! #EXPR_POSTFIX_FUNCTION_CALL
                /*!*/narr_t(struct expr) args;

                //! #EXPR_POSTFIX_SUBSCRIPT
                /*!*/struct expr* subscript;

                //! #EXPR_POSTFIX_ACCESS_DOT
                //! #EXPR_POSTFIX_ACCESS_ARROW
                struct
                {
                    //! Member identifier.
                    struct id id;

                    //! #true if accessing a #member_func.
                    //! #false if accessing a #member_var.
                    //! Set during semantic analysis.
                    bool is_member_func;
                } access;
            };
        } postfix;

        //! Unary expression operand.
        union expr_unary
        {
            //! #EXPR_UNARY_ADDR_OF
            //! #EXPR_UNARY_DEREF
            //! #EXPR_UNARY_DECAY
            //! #EXPR_UNARY_PLUS
            //! #EXPR_UNARY_MINUS
            //! #EXPR_UNARY_BITWISE_NOT
            //! #EXPR_UNARY_LOGICAL_NOT
            //! #EXPR_UNARY_COUNTOF_EXPR
            //! #EXPR_UNARY_SIZEOF_EXPR
            /*!*/struct expr* rhs_expr;

            //! #EXPR_UNARY_COUNTOF_DT
            //! #EXPR_UNARY_SIZEOF_DT
            /*!*/struct dt* rhs_dt;
        } unary;

        //! Binary expression operands.
        struct expr_binary
        {
            /*!*/struct expr* lhs_expr;
            union
            {
                //! #EXPR_BINARY_ASSIGN
                //! #EXPR_BINARY_LOGICAL_OR
                //! #EXPR_BINARY_LOGICAL_AND
                //! #EXPR_BINARY_BITWISE_OR
                //! #EXPR_BINARY_BITWISE_XOR
                //! #EXPR_BINARY_BITWISE_AND
                //! #EXPR_BINARY_REL_EQ
                //! #EXPR_BINARY_REL_NE
                //! #EXPR_BINARY_REL_LT
                //! #EXPR_BINARY_REL_GT
                //! #EXPR_BINARY_REL_LE
                //! #EXPR_BINARY_REL_GE
                //! #EXPR_BINARY_SHIFT_L
                //! #EXPR_BINARY_SHIFT_R
                //! #EXPR_BINARY_PLUS
                //! #EXPR_BINARY_MINUS
                //! #EXPR_BINARY_PTR_DIFF
                //! #EXPR_BINARY_PTR_PLUS
                //! #EXPR_BINARY_PTR_MINUS
                //! #EXPR_BINARY_MULT
                //! #EXPR_BINARY_DIV
                /*!*/struct expr* rhs_expr;

                //! #EXPR_BINARY_CAST
                /*!*/struct dt* rhs_dt;
            };
        } binary;
    };

    //! The `typeof` this expression when evaluated.
    struct dt dt;

    //! The value category of this expression.
    enum value_category
    {
        VALCAT_NONE = 0,
        VALCAT_RVALUE,
        VALCAT_LVALUE
    } valcat;

    //! #true if this expression fits the definition of `constexpr`.
    bool is_constexpr;

    //! Pointer to the relevant first token the expression was parsed from.
    //! #NULL if this expression was created via a transformation.
    //! @note
    //!     For primary expressions `srctok` should be the first token of the
    //!     primary expression.
    //! @note
    //!     For function call expressions `srctok` should be the function
    //!     identifier or the first token of the expression that evaluates to
    //!     the function.
    //! @note
    //!     For unary expressions `srctok` should be the unary operator token.
    //! @note
    //!     For binary expressions `srctok` should be the binary operator token.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(expr);
//! @init
void expr_init(struct expr* expr);
//! @fini
void expr_fini(struct expr* expr);
//! @reset
void expr_reset(struct expr* expr);
//! @assign
//! @param dest
//!     Instance being assigned to.
//! @param src
//!     Instance being assigned from.
//! @param clear_srctok
//!     Sets `srctok` variables in the dest to #NULL if #true, indicating that
//!     @p dest was created via a transformation.
void expr_assign(
    struct expr* dest,
    struct expr const* src,
    bool clear_srctok
);

bool expr_is_primary(struct expr const* expr);
bool expr_is_postfix(struct expr const* expr);
bool expr_is_unary(struct expr const* expr);
bool expr_is_binary(struct expr const* expr);

bool expr_is_lvalue(struct expr const* expr);
bool expr_is_rvalue(struct expr const* expr);

//============================================================================//
//      IN-SOURCE IDENTIFIER DECLARATION                                      //
//============================================================================//
//====================================//
//      VARIABLE DECLARATION          //
//====================================//
//! Variable declaration.
struct var_decl
{
    //! Identifier of the declared variable.
    struct id id;

    //! Data type of the declared variable.
    struct dt dt;

    //! Initial definition of the declared variable.
    //! @br
    //! If #NULL, this variable was declared as uninitialized:
    //! ```
    //! var <identifier> : <data-type> = uninit;
    //! ```
    //! @br
    //! If non-#NULL, this variable was declared with a definition:
    //! ```
    //! var <identifier> : <data-type> = <expr>;
    //! ```
    /*!*/struct expr* def;

    bool is_export;
    bool is_extern;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(var_decl);
//! @init
void var_decl_init(struct var_decl* decl);
//! @fini
void var_decl_fini(struct var_decl* decl);

//====================================//
//      FUNCTION DECLARATION          //
//====================================//
//! Function declaration.
struct func_decl
{
    //! Identifier of the declared function.
    struct id id;

    //! Data type of the declared function.
    struct dt dt;

    //! Identifiers of each parameter in the function's #dt.
    narr_t(struct id) param_ids;

    //! Definition/body of the declared function.
    //! @br
    //! If #NULL, this function was forward declared:
    //! ```
    //! func <identifier> : <data-type-with-params>;
    //! ```
    //! @br
    //! If non-#NULL, this function was declared with a definition/body:
    //! ```
    //! func <identifier> : <data-type-with-params> { <stmts> }
    //! ```
    /*!*/struct scope* def;

    bool is_export;
    bool is_extern;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(func_decl);
//! @init
void func_decl_init(struct func_decl* decl);
//! @fini
void func_decl_fini(struct func_decl* decl);

//====================================//
//      STRUCT DECLARATION            //
//====================================//
//! Struct declaration.
struct struct_decl
{
    struct id id;

    bool is_export;
    bool is_extern;

    //! Definition of the declared struct.
    //! @br
    //! If #NULL, this struct was forward declared:
    //! ```
    //! struct <identifier>;
    //! ```
    //! @br
    //! If non-#NULL, this struct was declared with a definition:
    //! ```
    //! struct <identifier> <struct-definition>
    //! ```
    /*!*/struct struct_def* def;

    //! Pointer to the first token the declaration was parsed from.
    //! #NULL if this declaration was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(struct_decl);
//! @init
void struct_decl_init(struct struct_decl* decl);
//! @fini
void struct_decl_fini(struct struct_decl* decl);

//============================================================================//
//      CONTROL CONSTRUCTS                                                    //
//============================================================================//
//====================================//
//      INDICATIVE CONDITIONAL        //
//====================================//
//! Represents the abstract concept of `if <condition> then <action>`.
struct indicative_conditional
{
    struct expr condition;
    /*!*/struct scope* body;
};
INIT_FINI_COUNTER_HEADER(indicative_conditional);
//! @init
void indicative_conditional_init(struct indicative_conditional* cond);
//! @fini
void indicative_conditional_fini(struct indicative_conditional* cond);

//====================================//
//      IF-ELIF-ELSE                  //
//====================================//
struct if_elif_else
{
    //! @note
    //!     if_elif_ind_cond[0] is the original `if` conditional.
    //!     if_elif_ind_cond[1..n-1] are `elif` conditionals after the `if`.
    /*!*/narr_t(struct indicative_conditional) if_elif_ind_cond;

    //! Body of the final else statement.
    //! `else_body == NULL` indicates no else statement was present.
    /*!*/struct scope* else_body;
};
INIT_FINI_COUNTER_HEADER(if_elif_else);
//! @init
void if_elif_else_init(struct if_elif_else* if_elif_else);
//! @fini
void if_elif_else_fini(struct if_elif_else* if_elif_else);

//====================================//
//      LOOP                          //
//====================================//
struct loop
{
    enum loop_tag
    {
        LOOP_UNSET,

        LOOP_CONDITIONAL,
        LOOP_RANGE
    } tag;

    union
    {
        //! #LOOP_CONDITIONAL
        struct
        {
            /*!*/struct expr* expr;
        } cond;

        //! #LOOP_RANGE
        struct
        {
            //! @note
            //!     This variable declaration is parsed using loop variable
            //!     declaration syntax, and as such should not be parsed via
            //!     #prod_var_decl. Processing via #proc_var_decl is expected
            //!     however.
            struct var_decl var_decl;
#define LOWER_INCLUSIVE '['
#define LOWER_EXCLUSIVE '('
            //! `[` : inclusive
            //! @br
            //! `(` : exclusive
            char lower_clusivity;
#define UPPER_INCLUSIVE ']'
#define UPPER_EXCLUSIVE ')'
            //! `]` : inclusive
            //! @br
            //! `)` : exclusive
            char upper_clusivity;
            /*!*/struct expr* lower_bound;
            /*!*/struct expr* upper_bound;
        } range;
    };

    /*!*/struct scope* body;

    //! Pointer to the first token the loop was parsed from.
    //! #NULL if this loop was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(loop);
//! @init
void loop_init(struct loop* loop);
//! @fini
void loop_fini(struct loop* loop);

//============================================================================//
//      ALIAS                                                                 //
//============================================================================//
struct alias
{
    enum
    {
        ALIAS_UNSET,

        ALIAS_DT
    } tag;

    struct id new_id;

    union
    {
        struct dt dt;
    } aliased;

    bool is_export;

    //! Pointer to the first token the alias was parsed from.
    //! #NULL if this alias was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(alias);
//! @init
void alias_init(struct alias* alias);
//! @fini
void alias_fini(struct alias* alias);

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
struct stmt
{
    enum
    {
        STMT_UNSET,

        STMT_EMPTY,
        STMT_VARIABLE_DECLARATION,
        STMT_FUNCTION_DECLARATION,
        STMT_STRUCT_DECLARATION,
        STMT_IF_ELIF_ELSE,
        STMT_LOOP,
        STMT_DEFER,
        STMT_BREAK,
        STMT_CONTINUE,
        STMT_RETURN,
        STMT_EXPR,
        STMT_SCOPE,
        STMT_IMPORT,
        STMT_ALIAS
    } tag;

    union
    {
        //! #STMT_VARIABLE_DECLARATION
        /*!*/struct var_decl* var_decl;

        //! #STMT_FUNCTION_DECLARATION
        /*!*/struct func_decl* func_decl;

        //! #STMT_STRUCT_DECLARATION
        /*!*/struct struct_decl* struct_decl;

        //! #STMT_IF_ELIF_ELSE
        /*!*/struct if_elif_else* if_elif_else;

        //! #STMT_LOOP
        /*!*/struct loop* loop;

        //! #STMT_DEFER
        struct
        {
            size_t idx;
            /*!*/struct scope* scope;
        } defer;

        //! #STMT_RETURN
        //! @note
        //!     `rtn_expr == NULL` indicates the return statement does not have
        //!     a return expression.
        /*!*/struct expr* rtn_expr;

        //! #STMT_EXPR
        /*!*/struct expr* expr;

        //! #STMT_SCOPE
        /*!*/struct scope* scope;

        //! #STMT_IMPORT
        nstr_t import_string;

        //! #STMT_ALIAS
        /*!*/struct alias* alias;
    };

    //! Pointer to the first token the statement was parsed from.
    //! #NULL if this statement was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(stmt);
//! @init
void stmt_init(struct stmt* stmt);
//! @fini
void stmt_fini(struct stmt* stmt);

//============================================================================//
//      LEXICAL SCOPE                                                         //
//============================================================================//
//====================================//
//      SYMBOL                        //
//====================================//
struct symbol
{
    enum
    {
        SYMBOL_UNSET,

        SYMBOL_VARIABLE,
        SYMBOL_FUNCTION,
        SYMBOL_STRUCT,
        SYMBOL_ALIAS
    } tag;

    union
    {
        //! #SYMBOL_VARIABLE
        struct var_decl* var_decl;

        //! #SYMBOL_FUNCTION
        struct func_decl* func_decl;

        //! #SYMBOL_STRUCT
        struct struct_decl* struct_decl;

        //! #SYMBOL_ALIAS
        struct alias* alias;
    } ref;

    //! Reference to the symbol's identifier.
    //! For #SYMBOL_VARIABLE, #SYMBOL_FUNCTION, and #SYMBOL_STRUCT this is the
    //! identifier of the symbol introduced via a declaration.
    //! For #SYMBOL_ALIAS this is the identifier of the new symbol created as
    //! an alias for the older symbol.
    struct id* id;

    //! Reference to the symbol's data type.
    //! For #SYMBOL_VARIABLE this is the data type of the variable.
    //! For #SYMBOL_FUNCTION this is the data type of the function's signature.
    //! For #SYMBOL_STRUCT this is the data type of the struct.
    //! For #SYMBOL_ALIAS this is the data type being aliased.
    //! Ownership:
    //! + `SYMBOL_STRUCT == tag`
    /*?*/struct dt* dt;

    //! Reference to the token that this symbol was created from.
    struct token const* srctok;

    //! Module in the #module_cache that this symbol belongs to.
    size_t module_idx;

    //! Number of times that this symbol referred to by identifier.
    uint32_t uses;

    bool is_export;
    bool is_defined;
};
INIT_FINI_COUNTER_HEADER(symbol);
//! @init
void symbol_init(struct symbol* symbol);
//! @init
void symbol_init_variable(
    struct symbol* symbol,
    struct var_decl* var_decl
);
//! @init
void symbol_init_function(
    struct symbol* symbol,
    struct func_decl* func_decl
);
//! @init
void symbol_init_struct(
    struct symbol* symbol,
    struct struct_decl* struct_decl
);
//! @init
void symbol_init_alias(
    struct symbol* symbol,
    struct alias* alias
);
//! @fini
void symbol_fini(struct symbol* symbol);

void symbol_mark_use(struct symbol* symbol);

//====================================//
//      SYMBOL TABLE                  //
//====================================//
struct _stdict_slot
{
    struct symbol symbol;
    bool in_use;
    uint32_t hash;
};

struct symbol_table
{
    //! Insert-only dictionary containing all symbols in this symbol table.
    struct _stdict_slot* _dict;
    //! Index into the internal table of primes used for calculating the size of
    //! the symbol table.
    uint32_t _dict_prime_idx;
    //! Number of slots in use in the symbol table's dictionary.
    uint32_t _dict_slots_in_use;

    //! Pointer to the most recent symbol in this symbol table.
    //! This is a pointer to dynamic memory and should be considered invalid
    //! after any other addition to the symbol table.
    //! @br
    //! #NULL if no symbols have been added to the symbol table.
    struct symbol* most_recent_symbol;

    //! Module in the #module_cache that this symbol table belongs to.
    size_t module_idx;
};
INIT_FINI_COUNTER_HEADER(symbol_table);
//! @init
void symbol_table_init(struct symbol_table* symbol_table);
//! @fini
void symbol_table_fini(struct symbol_table* symbol_table);

//! Insert the provided symbol to the symbol table.
//! @return
//!     Pointer to the added symbol on success.
//!     This is a pointer to dynamic memory and should be considered invalid
//!     after any other addition to the symbol table.
//! @return
//!     #NULL on failure.
struct symbol* symbol_table_insert(
    struct symbol_table* symbol_table,
    struct symbol const* symbol
);

//! @return
//!     Pointer to the symbol table symbol if a symbol with the provided symbol
//!     string exists in the table.
//!     This is a pointer to dynamic memory and should be considered invalid
//!     after any other addition to the symbol table.
//! @return
//!     #NULL if an symbol does not exist in the table.
struct symbol* symbol_table_find(
    struct symbol_table* symbol_table,
    char const* symstr,
    size_t symstr_length
);

//====================================//
//      SCOPE                         //
//====================================//
#define GLOBAL_SCOPE_PARENT_PTR ((struct scope const*)NULL)
//! Default value for the parent pointer that can be set during syntax analysis
//! before it is later updated during semantic analysis.
//! Since this can be a valid address of a pointer this macro should not be
//! used for a comparison, and should instead only be used during debugging.
#define UNDEFINED_SCOPE_PARENT ((struct scope const*)0xBAD)
struct scope
{
    uint32_t uid;
    struct scope const* parent;
    /*!*/struct symbol_table* symbol_table;
    /*!*/narr_t(struct stmt) stmts;

    //! List of pointers to deferred scopes.
    //! When a scope is processed, defer statements will push the pointer to
    //! the statement's deferred scope onto the back of this list.
    //! The pointers in this list do NOT need to be freed, as they are owned
    //! by their respective statements.
    /*!*/narr_t(struct scope*) deferred_scopes;

    bool is_within_defer;
    bool is_within_defer_primary;
    bool is_within_loop;
    bool is_within_loop_primary;

    //! Pointer to the first token the scope was parsed from.
    //! #NULL if this scope was created via a transformation.
    struct token const* srctok;
};
INIT_FINI_COUNTER_HEADER(scope);
//! @init
void scope_init(struct scope* scope, struct scope const* parent_scope);
//! @fini
void scope_fini(struct scope* scope);

//! @return
//!     #true if @p scope is the global scope.
bool scope_is_global(struct scope const* scope);

#define NO_ENTRY_FUNC               0
#define INVALID_ENTRY_FUNC          1
#define ENTRY_FUNC_NO_ARGS_RTN_VOID 2
#define ENTRY_FUNC_NO_ARGS_RTN_U    3
#define ENTRY_FUNC_NO_ARGS_RTN_S    4
#define ENTRY_FUNC_ARGS_RTN_VOID    5
#define ENTRY_FUNC_ARGS_RTN_U       6
#define ENTRY_FUNC_ARGS_RTN_S       7
//! Check if the scope contains one of the valid declarations of `entry`, the
//! functions defining the entry point to a program.
//! @br
//! Valid declarations of entry are:
//! * `func entry : () -> void`
//! * `func entry : () -> u`
//! * `func entry : () -> s`
//! * `func entry : (argc : u, argv : **char) -> void`
//! * `func entry : (argc : u, argv : **char) -> u`
//! * `func entry : (argc : u, argv : **char) -> s`
//! @return
//!     #NO_ENTRY_FUNC if the scope does not contain a declaration of `entry`.
//! @return
//!     #INVALID_ENTRY_FUNC if the scope contains a declaration of `entry` that
//!     does not match one the valid declarations of `entry`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_VOID if the scope contains a declaration of
//!     entry with the type `() -> void`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_U if the scope contains a declaration of
//!     entry with the type `() -> u`.
//! @return
//!     #ENTRY_FUNC_NO_ARGS_RTN_S if the scope contains a declaration of
//!     entry with the type `() -> s`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_VOID if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> void`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_U if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> u`.
//! @return
//!     #ENTRY_FUNC_ARGS_RTN_S if the scope contains a declaration of
//!     entry with the type `(argc : u, argv : ** char) -> s`.
int scope_contains_entry_func(struct scope* scope);

//============================================================================//
//      MODULE                                                                //
//============================================================================//
struct module
{
    //! File path of the module source.
    nstr_t src_path;

    //! File path of the compiled output for this module.
    nstr_t out_path;

    //! Contents of the #src_path.
    nstr_t src;

    //! Tokenization of #src.
    /*!*/narr_t(struct token) tokens;

    //! The global scope of this module.
    struct scope global_scope;

    //! List of each #id for symbols exported from the global #scope of this
    //! module.
    /*!*/narr_t(struct id) exports;

    //! Used for generating unique identifiers within this module.
    //! These integer identifiers are used for generating unique variable and
    //! label names within the module.
    uint32_t uid_counter;
};
INIT_FINI_COUNTER_HEADER(module);
//! @init
void module_init(struct module* module);
//! @fini
void module_fini(struct module* module);
//! Get a new uid from the module and increment the module's uid counter.
uint32_t module_generate_uid(struct module* module);
//! Adds the index of the most recently added global symbol from the global
//! symbol table of @p module to the list of exported symbol indices of @p
//! module.
void module_add_latest_global_symbol_to_exports(struct module* module);

//! Detect whether the export of a module came from an import.
bool is_import(
    struct module* module,
    char const* symstr,
    size_t symstr_length
);

//! Resolve the path of a module.
//! @param path
//!     User provided path of the module. #resolve_module_path will modify @p
//!     path to contain the absolute path of the module on success.
//! @param relto
//!     If the module cannot be located in the include path, the module will
//!     attempt to be located relative to path @p relto.
//! @return
//!     #STBOOL_SUCCESS if a readable file can be located.
//! @return
//!     #STBOOL_FAILURE otherwise.
stbool resolve_module_path(nstr_t* path, char const* relto);

#define MODULE_CACHE_MAX_LEN 4096
//! Global list of modules that have been or are in the process of being
//! compiled.
struct module module_cache[MODULE_CACHE_MAX_LEN];
void module_cache_init(void);
void module_cache_fini(void);

//! Number of modules within the #module_cache.
extern size_t module_cache_len;

//! Index in the #module_cache of the current module being compiled.
//! Updated upon entrance to #compile.
//! This variable will NOT be automatically updated upon exiting #compile.
extern size_t current_module_idx;
