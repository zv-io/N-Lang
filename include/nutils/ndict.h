/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//! @file ndict.h
//! Facilities for creating and manipulation dictionaries.
//! This module does *not* provide a generic dictionary container equivalent to
//! C++'s `std::map`, but instead provides a range of tools for a dictionary
//! manipulation on use-case-by-use-case basis.
#include "_base.h"
#include "npanic.h"

#define DICT_MAX_ELEMENTS UINT32_MAX

#define DICT_PRIMES_LENGTH 26
//! List of prime number roughly splitting incremental powers of two.
//! Creating a hash table with a number of slots equal to a prime in this list
//! will probably have good distribution.
extern uint32_t const dict_primes[DICT_PRIMES_LENGTH];

//! Find the density of a dictionary satisfying the equation:
//! ```
//! LOAD_FACTOR = SLOTS_IN_USE / SLOTS_TOTAL
//! ```
//! @param slots_in_use
//!     Number of non-empty slots within the dictionary.
//! @param slots_total
//!     Total number of slots within the dictionary.
//! @note
//!     The true load factor for a dictionary depends on the method used to
//!     store elements.
//!     For example, this function will *not* count the load factor for a
//!     dictionary using bucketed storage.
double dict_slot_load_factor(uint32_t slots_in_use, uint32_t slots_total);

//! Calculate the index into a dictionary.
//! For dictionary traversal using linear, quadratic, etc. probing the value of
//! this macro will correspond to the starting index for the probe.
//! For a dictionary using bucketed storage the value of this macro will
//! correspond to the index of the target bucket.
#define DICT_HASH_IDX(hash, dict_slots_total) (hash % dict_slots_total)

//! Move a linear probe forward by one element.
//! @param probe_idx @multi_eval
//!     Lvalue containing the current index of the probe.
//! @param dict_slots_total
//!     Total number of slots within the dictionary.
//!     This value is used for modulo-wrapping.
#define DICT_INCR_LINEAR_PROBE(probe_idx, dict_slots_total)                    \
    (probe_idx = (probe_idx + 1) % dict_slots_total);

//! #npanic indicating that the dictionary has grown too large.
#define NPANIC_DICT_TOO_LARGE()                                                \
        npanic("dictionary is too large");

//! Compute the hash of a character slice for dictionary indexing.
//! @param start
//!     Pointer to the first character in the character slice.
//! @param length
//!     Number of characters in the slice.
uint32_t hash_char_slice(char const* start, size_t length);
