/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "_base.h"

#define ANSI_ESC_DEFAULT       "\x1b[0;0m"

#define ANSI_ESC_BOLD          "\x1b[0;1m"
#define ANSI_ESC_ITALIC        "\x1b[0;3m"
#define ANSI_ESC_UNDERLINE     "\x1b[0;4m"

#define ASNI_ESC_COLOR_BLACK   "\x1b[0;30m"
#define ASNI_ESC_COLOR_RED     "\x1b[0;31m"
#define ASNI_ESC_COLOR_GREEN   "\x1b[0;32m"
#define ANSI_ESC_COLOR_YELLOW  "\x1b[0;33m"
#define ANSI_ESC_COLOR_BLUE    "\x1b[0;34m"
#define ANSI_ESC_COLOR_MAGENTA "\x1b[0;35m"
#define ANSI_ESC_COLOR_CYAN    "\x1b[0;36m"
#define ASNI_ESC_COLOR_WHITE   "\x1b[0;37m"

//! Applies the escape sequence @p esc_seq to @p str_literal if the `FILE*`
//! @p FILEptr points to a tty.
//! @note
//!     Multiple escape sequences can be combined together through compile time
//!     string concatenation to create a compound escape sequence.
#define ANSI_ESC_IF_TTY(FILEptr, esc_seq, str_literal) \
    (isatty(fileno(FILEptr)) ? esc_seq str_literal ANSI_ESC_DEFAULT : str_literal)
