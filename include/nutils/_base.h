/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// This header should be included by all other headers in the nutils library.
#pragma once
#define _GNU_SOURCE

//============================================================================//
//      LIBRARY-WIDE HEADERS                                                  //
//============================================================================//
// C Standard Library
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
// POSIX
#include <getopt.h>
#include <libgen.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

//============================================================================//
//      STATUS BOOL RETURN TYPE                                               //
//============================================================================//
typedef bool stbool;
#   undef true
#   define true  ((_Bool)1)
#   undef false
#   define false ((_Bool)0)
#define STBOOL_SUCCESS true
#define STBOOL_FAILURE false

//============================================================================//
//      UTILITY MACROS                                                        //
//============================================================================//
#   undef NULL
#   define NULL  ((void *)0)

static_assert(CHAR_BIT == 8, "invalid CHAR_BIT");
#define CHAR_BITS CHAR_BIT //!< No idea why POSIX went with `CHAR_BIT`-singular.
#define BYTE_BITS CHAR_BIT //!< 8 bits to a byte.

#define EMPTY_STRING ""
#define SINGLE_QUOTE_LEN      (1) //!< `'`
#define SINGLE_QUOTE_TYPE_LEN (1) //!< `a`
#define DOUBLE_QUOTE_LEN      (1) //!< `"`
#define DOUBLE_QUOTE_TYPE_LEN (1) //!< `a`
#define NULL_TERMINATOR_LEN   (1) //!< `\0`

#define INDENT_S                "  "
#define INDENT_M                "    "
#define INDENT_L                "        "
#define INDENT_S_LEN            (sizeof(INDENT_S) - 1)
#define INDENT_M_LEN            (sizeof(INDENT_M) - 1)
#define INDENT_L_LEN            (sizeof(INDENT_L) - 1)

#define SUCCESS (0)

//! Automatic type deduction for single variable declarations.
#define auto_t __auto_type

//! Thread local storage.
#ifndef thread_local
#   define thread_local __thread
#endif

//! Typeof operator.
#ifndef typeof
#   define typeof __typeof
#endif

//! Infinite loop.
#define LOOP_FOREVER while (1)

//! Suppress "unused <whatever>" warnings for a single variable.
#define SUPPRESS_UNUSED(variable) (void)(variable)

#define _STRINGIFY_HELPER(...) #__VA_ARGS__
//! Turn the provided text into a cstring.
//! @note
//!     Macros are expanded by the preprocessor, so this macro does NOT
//!     preserve whitespace.
//! ```
//!     Example:
//!     printf(
//!         "%s qux\n",
//!         STRINGIFY(foo bar,          baz)
//!     );
//!     ^^ prints "foo bar, baz qux"
//! ```
#define STRINGIFY(...) _STRINGIFY_HELPER(__VA_ARGS__)

//! Plain old data copy.
//! @note
//!     This macro literally just an assignment statement. The only point of the
//!     macro is to make it clear that a structure is copying all it's data by
//!     value.
#define POD_COPY(dest, src) ((dest) = (src))

//! Returns a pointer to the struct containing the member pointed to by
//! @p p_member who's member identifier in struct @p struct_type is @p
//! member_id. This is the same macro as the Linux Kernel's `list_entry`.
//! @param p_member
//!     Pointer to a member variable with identifier @p member_id within a
//!     struct of type @p struct_type.
//! @param struct_type
//!     The `typeof` the struct containing the provided member.
//! @param member_id
//!     Identifier of `*p_member` within the provided @p struct_type.
#define P_STRUCT_FROM_P_MEMBER(p_member, struct_type, member_id)               \
    ((struct_type*) ((char*)(p_member) - offsetof(struct_type, member_id)))

//! Zero the memory pointed to by @p p_instance.
//! @param p_instance
//!     Pointer to an object of type @p type.
//! @param type
//!     The `typeof` `*p_instance`.
#define MEMZERO_INSTANCE(p_instance, type)                                     \
    memset(p_instance, 0, sizeof(type))

//! Memset the memory pointed to by @p p_instance.
//! @param p_instance
//!     Pointer to an object of type @p type.
//! @param type
//!     The `typeof` `*p_instance`.
//! @param value
//!     8-bit value that will fill each byte of p_instance.
#define MEMSET_INSTANCE(p_instance, type, value)                               \
    memset(p_instance, value, sizeof(type))

//! Linux Kernel `likely` and `unlikely` definitions.
#define LIKELY(x)   __builtin_expect((x),1)
#define UNLIKELY(x) __builtin_expect((x),0)

//============================================================================//
//      CLANG DIAGNOSTICS/PRAGMAS/ATTRIBUTES                                  //
//============================================================================//
#define DIAGNOSTIC_PUSH   _Pragma("GCC diagnostic push")
#define DIAGNOSTIC_IGNORE_HELPER(w) STRINGIFY(GCC diagnostic ignored w)
#define DIAGNOSTIC_IGNORE(w) _Pragma(DIAGNOSTIC_IGNORE_HELPER(w))
#define DIAGNOSTIC_POP    _Pragma("GCC diagnostic pop")

#define ATTR_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#define ATTR_WARN_UNUSED_VARIABLE __attribute((unused))
#define _DEFAULT_PRIORITY 5000
#define ATTR_CONSTRUCTOR                    __attribute__((constructor(_DEFAULT_PRIORITY)))
#define ATTR_CONSTRUCTOR_PRIORITY(priority) __attribute__((constructor(priority)))
#define ATTR_DESTRUCTOR                     __attribute__((destructor(_DEFAULT_PRIORITY)))
#define ATTR_DESTRUCTOR_PRIORITY(priority)  __attribute__((destructor(priority)))
#define ATTR_DEFER_FINI(fini_func)          __attribute__((cleanup(fini_func)))
