/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//============================================================================//
//      CSTRING                                                               //
//============================================================================//
int cstr_cmp(char const* lhs, char const* rhs);
bool cstr_eq(char const* lhs, char const* rhs);
bool cstr_ne(char const* lhs, char const* rhs);

int cstr_n_cmp(char const* lhs, char const* rhs, size_t n);
bool cstr_n_eq(char const* lhs, char const* rhs, size_t n);
bool cstr_n_ne(char const* lhs, char const* rhs, size_t n);

int cstr_cmp_case(char const* lhs, char const* rhs);
bool cstr_eq_case(char const* lhs, char const* rhs);
bool cstr_ne_case(char const* lhs, char const* rhs);

int cstr_n_cmp_case(char const* lhs, char const* rhs, size_t n);
bool cstr_n_eq_case(char const* lhs, char const* rhs, size_t n);
bool cstr_n_ne_case(char const* lhs, char const* rhs, size_t n);

size_t cstr_len(char const* str);
bool cstr_len_ge(char const* str, size_t n);

void cstr_cat(char* dest, char const* src);
void cstr_n_cat(char* dest, char const* src, size_t n);

void cstr_cpy(char* dest, char const* src);
void cstr_n_cpy(char* dest, char const* src, size_t n);

//============================================================================//
//      NSTRING                                                               //
//============================================================================//
typedef struct
{
    char*  data;
    size_t len;
    size_t capacity;
} nstr_t;

void nstr_init(nstr_t* nstr);
void nstr_init_cstr(nstr_t* nstr, char const* other);
void nstr_init_nstr(nstr_t* nstr, nstr_t const* other);
void nstr_init_fmt(nstr_t* nstr, char const* fmt, ...);
void nstr_fini(nstr_t* nstr);
#define nstr_defer_fini __attribute__((cleanup(nstr_fini)))

void nstr_resize(nstr_t* nstr, size_t len);
void nstr_reserve(nstr_t* nstr, size_t nchars);

void nstr_assign_cstr(nstr_t* nstr, char const* other);
void nstr_assign_nstr(nstr_t* nstr, nstr_t const* other);
void nstr_assign_fmt(nstr_t* nstr, char const* fmt, ...);

void nstr_clear(nstr_t* nstr);

int nstr_cmp(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_eq(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_ne(nstr_t const* lhs, nstr_t const* rhs);

int nstr_cmp_case(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_eq_case(nstr_t const* lhs, nstr_t const* rhs);
bool nstr_ne_case(nstr_t const* lhs, nstr_t const* rhs);

void nstr_cat_cstr(nstr_t* dest, char const* src);
void nstr_cat_nstr(nstr_t* dest, nstr_t const* src);
void nstr_cat_fmt(nstr_t* dest, char const* fmt, ...);
