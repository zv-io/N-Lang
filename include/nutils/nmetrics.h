/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//============================================================================//
//      INIT FINI COUNTER                                                     //
//============================================================================//
void _verify_balanced_init_fini(
    char const* type_name,
    unsigned init_numcalls,
    unsigned fini_numcalls
);

#define INIT_FINI_COUNTER_HEADER(struct_name)                                  \
    extern unsigned _init_numcalls_##struct_name;                              \
    extern unsigned _fini_numcalls_##struct_name;                              \
    void _verify_balanced_init_fini_##struct_name(void)

#define INIT_FINI_COUNTER_SOURCE(struct_name)                                  \
    unsigned _init_numcalls_##struct_name = 0;                                  \
    unsigned _fini_numcalls_##struct_name = 0;                                  \
    void _verify_balanced_init_fini_##struct_name(void)                        \
    {                                                                          \
        _verify_balanced_init_fini(                                            \
            STRINGIFY(struct_name),                                            \
            _init_numcalls_##struct_name,                                      \
            _fini_numcalls_##struct_name                                       \
        );                                                                     \
    }

#define INIT_FINI_COUNTER_INCR_INIT(struct_name)                               \
    _init_numcalls_##struct_name += 1

#define INIT_FINI_COUNTER_INCR_FINI(struct_name)                               \
    _fini_numcalls_##struct_name += 1

#define INIT_FINI_COUNTER_VERIFY_BALANCED(struct_name)                         \
    _verify_balanced_init_fini_##struct_name()

//============================================================================//
//      HIGH PRECISION TIMING                                                 //
//============================================================================//
//! Calls `clock_gettime` with clock id `CLOCK_MONOTONIC`.
//! Populates @p ts with the time on success.
//! Panics on failure.
void ngettime(struct timespec* ts);
//! Calculated the difference between @p start and @p stop in milliseconds.
long long nelapsed_ms(struct timespec start, struct timespec stop);
