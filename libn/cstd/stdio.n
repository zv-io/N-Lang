# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

export extern struct FILE;

export extern let stdin  : mut ^mut FILE;
export extern let stdout : mut ^mut FILE;
export extern let stderr : mut ^mut FILE;

export let EOF : sint = -1s as sint;

export extern func fopen : (path : mut ^ascii, mode : mut ^ascii) -> ^mut FILE;
export extern func freopen : (path : mut ^ascii, mode : mut ^ascii, stream : ^mut FILE) -> ^mut FILE;
export extern func fclose : (stream : mut ^mut FILE) -> sint;

export extern func fflush : (stream : mut ^mut FILE) -> sint;

export extern func fseek : (stream : mut ^mut FILE, offset : slong, whence : sint) -> sint;
export extern func ftell : (stream : mut ^mut FILE) -> slong;
export extern func rewind : (stream : mut ^mut FILE) -> void;

export extern func fread  : (buf : mut ^mut void, size : mut size_t, count : mut size_t, stream : mut ^mut FILE) -> size_t;
export extern func fwrite : (buf : mut ^void, size : mut size_t, count : mut size_t, stream : mut ^mut FILE) -> size_t;

export extern func getchar : () -> sint;
export extern func putchar : (c : mut sint) -> sint;

export extern func fgetc : (stream : mut ^mut FILE) -> sint;
export extern func fputc : (c : mut sint, stream : ^mut FILE) -> sint;

export extern func gets : (str : mut ^mut ascii) -> ^ascii;
export extern func puts : (str : mut ^ascii) -> sint;

export extern func fgets : (str : mut ^mut ascii, stream : mut ^mut FILE) -> ^mut ascii;
export extern func fputs : (str : mut ^ascii, stream : mut ^mut FILE) -> sint;

export extern func ungetc : (c : mut sint, stream : mut ^mut FILE) -> sint;

export extern func remove : (path : mut ^ascii) -> sint;
export extern func rename : (old_path : mut ^mut ascii, new_path : mut ^mut ascii) -> sint;
