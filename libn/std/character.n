# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
export func ascii_is_alphabetic : (ch : ascii) -> bool;
export func ascii_is_digit_bin : (ch : ascii) -> bool;
export func ascii_is_digit_dec : (ch : ascii) -> bool;
export func ascii_is_digit_hex : (ch : ascii) -> bool;
export func ascii_is_digit_b36 : (ch : ascii) -> bool;
export func ascii_is_upper : (ch : ascii) -> bool;
export func ascii_is_lower : (ch : ascii) -> bool;
export func ascii_is_space : (ch : ascii) -> bool;

export func ascii_to_digit_bin : (ch : ascii) -> s;
export func ascii_to_digit_dec : (ch : ascii) -> s;
export func ascii_to_digit_hex : (ch : ascii) -> s;
export func ascii_to_digit_b36 : (ch : ascii) -> s;
export func ascii_to_upper : (ch : ascii) -> ascii;
export func ascii_to_lower : (ch : ascii) -> ascii;

export let ASCII_NUL : ascii = 0x00u8 as ascii; # Null
export let ASCII_SOH : ascii = 0x01u8 as ascii; # Start Of Heading
export let ASCII_STX : ascii = 0x02u8 as ascii; # Start Of Text
export let ASCII_ETX : ascii = 0x03u8 as ascii; # End Of Text
export let ASCII_EOT : ascii = 0x04u8 as ascii; # End Of Transmission
export let ASCII_ENQ : ascii = 0x05u8 as ascii; # Enquiry
export let ASCII_ACK : ascii = 0x06u8 as ascii; # Acknowledgment
export let ASCII_BEL : ascii = 0x07u8 as ascii; # Bell
export let ASCII_BS  : ascii = 0x08u8 as ascii; # Back Space
export let ASCII_HT  : ascii = 0x09u8 as ascii; # Horizontal Tab
export let ASCII_LF  : ascii = 0x0Au8 as ascii; # Line Feed
export let ASCII_VT  : ascii = 0x0Bu8 as ascii; # Vertical Tab
export let ASCII_FF  : ascii = 0x0Cu8 as ascii; # Form Feed
export let ASCII_CR  : ascii = 0x0Du8 as ascii; # Carriage Return
export let ASCII_SO  : ascii = 0x0Eu8 as ascii; # Shift Out / X-On
export let ASCII_SI  : ascii = 0x0Fu8 as ascii; # Shift In / X-Off
export let ASCII_DLE : ascii = 0x10u8 as ascii; # Data Line Escape
export let ASCII_DC1 : ascii = 0x11u8 as ascii; # Device Control 1
export let ASCII_DC2 : ascii = 0x12u8 as ascii; # Device Control 2
export let ASCII_DC3 : ascii = 0x13u8 as ascii; # Device Control 3
export let ASCII_DC4 : ascii = 0x14u8 as ascii; # Device Control 4
export let ASCII_NAK : ascii = 0x15u8 as ascii; # Negative Acknowledgement
export let ASCII_SYN : ascii = 0x16u8 as ascii; # Synchronous Idle
export let ASCII_ETB : ascii = 0x17u8 as ascii; # End of Transmit Block
export let ASCII_CAN : ascii = 0x18u8 as ascii; # Cancel
export let ASCII_EM  : ascii = 0x19u8 as ascii; # End of Medium
export let ASCII_SUB : ascii = 0x1Au8 as ascii; # Substitute
export let ASCII_ESC : ascii = 0x1Bu8 as ascii; # Escape
export let ASCII_FS  : ascii = 0x1Cu8 as ascii; # File Separator
export let ASCII_GS  : ascii = 0x1Du8 as ascii; # Group Separator
export let ASCII_RS  : ascii = 0x1Eu8 as ascii; # Record Separator
export let ASCII_US  : ascii = 0x1Fu8 as ascii; # Unit Separator

export let ASCII_LOWER_UPPER_DIFF : u8 = 'a'a as u8 - 'A'a as u8;

export let DIGITS_LOWER : [36u]ascii = [
    '0'a, '1'a, '2'a, '3'a, '4'a, '5'a, '6'a, '7'a,
    '8'a, '9'a, 'a'a, 'b'a, 'c'a, 'd'a, 'e'a, 'f'a,
    'g'a, 'h'a, 'i'a, 'j'a, 'k'a, 'l'a, 'm'a, 'n'a,
    'o'a, 'p'a, 'q'a, 'r'a, 's'a, 't'a, 'u'a, 'v'a,
    'w'a, 'x'a, 'y'a, 'z'a
];
export let DIGITS_UPPER : [36u]ascii = [
    '0'a, '1'a, '2'a, '3'a, '4'a, '5'a, '6'a, '7'a,
    '8'a, '9'a, 'A'a, 'B'a, 'C'a, 'D'a, 'E'a, 'F'a,
    'G'a, 'H'a, 'I'a, 'J'a, 'K'a, 'L'a, 'M'a, 'N'a,
    'O'a, 'P'a, 'Q'a, 'R'a, 'S'a, 'T'a, 'U'a, 'V'a,
    'W'a, 'X'a, 'Y'a, 'Z'a
];

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func ascii_is_alphabetic : (ch : ascii) -> bool
{
    return ascii_is_upper(ch)
        || ascii_is_lower(ch);
}

export func ascii_is_digit_bin : (ch : ascii) -> bool
{
    return ch as u8 == '0'a as u8
        || ch as u8 == '1'a as u8;
}

export func ascii_is_digit_dec : (ch : ascii) -> bool
{
    return ch as u8 >= '0'a as u8
        && ch as u8 <= '9'a as u8;
}

export func ascii_is_digit_hex : (ch : ascii) -> bool
{
    return ascii_is_digit_dec(ch)
        || ch as u8 >= 'A'a as u8 && ch as u8 <= 'F'a as u8
        || ch as u8 >= 'a'a as u8 && ch as u8 <= 'f'a as u8;
}

export func ascii_is_digit_b36 : (ch : ascii) -> bool
{
    return ascii_is_digit_dec(ch)
        || ch as u8 >= 'A'a as u8 && ch as u8 <= 'Z'a as u8
        || ch as u8 >= 'a'a as u8 && ch as u8 <= 'z'a as u8;
}

export func ascii_is_upper : (ch : ascii) -> bool
{
    return ch as u8 >= 'A'a as u8
        && ch as u8 <= 'Z'a as u8;
}

export func ascii_is_lower : (ch : ascii) -> bool
{
    return ch as u8 >= 'a'a as u8
        && ch as u8 <= 'z'a as u8;
}

export func ascii_is_space : (ch : ascii) -> bool
{
    return ch == ' 'a
        || ch == '\t'a
        || ch == '\n'a
        || ch == '\v'a
        || ch == '\f'a
        || ch == '\r'a;
}

export func ascii_to_digit_bin : (ch : ascii) -> s
{
    if ch == '0'a {return 0b0s;}
    if ch == '1'a {return 0b1s;}
    return -1s;
}

export func ascii_to_digit_dec : (ch : ascii) -> s
{
    if ch as u8 >= '0'a as u8 && ch as u8 <= '9'a as u8
    {
        return (ch as s - '0'a as s) as s;
    }
    return -1s;
}

export func ascii_to_digit_hex : (ch : ascii) -> s
{
    if ch as u8 >= '0'a as u8 && ch as u8 <= '9'a as u8
    {
        return ch as s - '0'a as s;
    }
    if ch as u8 >= 'A'a as u8 && ch as u8 <= 'F'a as u8
    {
        return ch as s - 'A'a as s + 10s;
    }
    if ch as u8 >= 'a'a as u8 && ch as u8 <= 'f'a as u8
    {
        return ch as s - 'a'a as s + 10s;
    }
    return -1s;
}

export func ascii_to_digit_b36 : (ch : ascii) -> s
{
    if ch as u8 >= '0'a as u8 && ch as u8 <= '9'a as u8
    {
        return ch as s - '0'a as s;
    }
    if ch as u8 >= 'A'a as u8 && ch as u8 <= 'Z'a as u8
    {
        return ch as s - 'A'a as s + 10s;
    }
    if ch as u8 >= 'a'a as u8 && ch as u8 <= 'z'a as u8
    {
        return ch as s - 'a'a as s + 10s;
    }
    return -1s;
}

export func ascii_to_upper : (ch : ascii) -> ascii
{
    if ascii_is_lower(ch)
    {
        let new_ascii : u8 = ch as u8 - ASCII_LOWER_UPPER_DIFF;
        return new_ascii as ascii;
    }
    return ch;
}

export func ascii_to_lower : (ch : ascii) -> ascii
{
    if ascii_is_upper(ch)
    {
        let new_ascii : u8 = ch as u8 + ASCII_LOWER_UPPER_DIFF;
        return new_ascii as ascii;
    }
    return ch;
}
