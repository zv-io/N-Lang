# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdio.n"a;
import "posix/unistd.n"a;
import "std/memory.n"a;
import "std/string.n"a;

export let FSTREAM_OPEN_R   : u = 0b001u;
export let FSTREAM_OPEN_W   : u = 0b010u;
export let FSTREAM_OPEN_WA  : u = 0b110u;
export let FSTREAM_OPEN_RW  : u = 0b011u;
export let FSTREAM_OPEN_RWA : u = 0b111u;

export struct fstream;

# Returns true on success.
# Returns false on failure.
export func fstream_init : (this : ^mut fstream, path : ^ascii, mode : u) -> bool;
export func fstream_fini : (this : ^mut fstream) -> void;

# Returns the number of bytes (non-negative) read on success.
# Returns -1s on failure.
export func fstream_read : (this : ^mut fstream, buf : ^mut void, nbytes : u) -> s;
# Returns the number of bytes (non-negative) written on success.
# Returns -1s on failure.
export func fstream_write : (this : ^mut fstream, buf : ^void, nbytes : u) -> s;

# Returns a non-negative integer on success.
# Returns -1s on failure.
export func fstream_print : (this : ^mut fstream, cstr : ^ascii) -> s;
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func fstream_println : (this : ^mut fstream, cstr : ^ascii) -> s;
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func fstream_printch : (this : ^mut fstream, ch : ascii) -> s;

# Print the provided str to stdout.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func print  : (cstr : ^ascii) -> s;
# Print the provided str to stdout followed by a newline character.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func println : (cstr : ^ascii) -> s;
# Print the provided character to stdout.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func printch : (ch : ascii) -> s;

# Print the provided str to stderr.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func eprint : (cstr : ^ascii) -> s;
# Print the provided str to stderr followed by a newline character.
# Returns a non-negative integer on success.
# Returns -1 on failure.
export func eprintln : (cstr : ^ascii) -> s;
# Print the provided character to stderr.
# Returns a non-negative integer on success.
# Returns -1s on failure.
export func eprintch : (ch : ascii) -> s;

# Get the size of a file specified by `path`.
# Returns the non-negative size of the file on success.
# Returns -1s on failure.
export func file_size : (path : ^ascii) -> s;
# Read a file into the provided buffer.
# Returns the number of bytes read on success, not including the null
# terminator.
# Returns -1s on failure, including if the buffer was not large enough to
# hold the contents of the file.
export func read_file_into_buf : (
    path : ^ascii,
    buf : ^mut void,
    bufsize : u,
    nullterm : bool
) -> s;
# Read a file into the provided string.
# Returns the number of bytes read on success.
# Returns -1s on failure.
export func read_file_into_string : (path : ^ascii, str : ^mut string) -> s;

export struct fstream
{
    let _fd : mut sint;
    let _mode : mut u;
    let _is_open : mut bool;

    func init    = fstream_init;
    func fini    = fstream_fini;

    func read    = fstream_read;
    func write   = fstream_write;

    func print   = fstream_print;
    func println = fstream_print;
    func printch = fstream_print;
}

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func fstream_init : (this : ^mut fstream, path : ^ascii, mode : u) -> bool
{
    this->_fd = -1s as sint;
    this->_mode = 0u;
    this->_is_open = false;

    let flags : mut sint = void;
    if   mode == FSTREAM_OPEN_R  {flags = O_CREAT | O_RDONLY;}
    elif mode == FSTREAM_OPEN_W  {flags = O_CREAT | O_WRONLY | O_TRUNC;}
    elif mode == FSTREAM_OPEN_WA {flags = O_CREAT | O_WRONLY | O_APPEND;}
    elif mode == FSTREAM_OPEN_RW {flags = O_CREAT | O_RDWR   | O_TRUNC;}
    elif mode == FSTREAM_OPEN_RWA{flags = O_CREAT | O_RDWR   | O_APPEND;}
    else{return false;}

    let fd : sint = open(path, flags, 0b110110110u as mode_t);
    if fd == -1s as sint
    {
        return false;
    }

    this->_fd = fd;
    this->_mode = mode;
    this->_is_open = true;
    return true;
}

export func fstream_fini : (this : ^mut fstream) -> void
{
    if !this->_is_open
    {
        return;
    }

    close(this->_fd);

    this->_mode = 0u;
    this->_is_open = false;
}

export func fstream_read : (this : ^mut fstream, buf : ^mut void, nbytes : u) -> s
{
    return read(this->_fd, buf, nbytes);
}

export func fstream_write : (this : ^mut fstream, buf : ^void, nbytes : u) -> s
{
    return write(this->_fd, buf, nbytes);
}

export func fstream_print : (this : ^mut fstream, cstr : ^ascii) -> s
{
    let len : u = str_length(cstr);
    if -1s == write(this->_fd, cstr, len)
    {
        return -1s;
    }
    return 0s;
}

export func fstream_println : (this : ^mut fstream, cstr : ^ascii) -> s
{
    if -1s == fstream_print(this, cstr)
    {
        return -1s;
    }
    if -1s == write(this->_fd,  $"\n"a, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func fstream_printch : (this : ^mut fstream, ch : ascii) -> s
{
    if -1s == write(this->_fd, ?ch, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func print  : (cstr : ^ascii) -> s
{
    let len : u = str_length(cstr);
    if -1s == write(STDOUT_FILENO, cstr, len)
    {
        return -1s;
    }
    return 0s;
}

export func println : (cstr : ^ascii) -> s
{
    if -1s == print(cstr)
    {
        return -1s;
    }
    if -1s == write(STDOUT_FILENO, $"\n"a, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func printch : (ch : ascii) -> s
{
    if -1s == write(STDOUT_FILENO, ?ch, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func eprint : (cstr : ^ascii) -> s
{
    let len : u = str_length(cstr);
    if -1s == write(STDERR_FILENO, cstr, len)
    {
        return -1s;
    }
    return 0s;
}

export func eprintln : (cstr : ^ascii) -> s
{
    if -1s == print(cstr)
    {
        return -1s;
    }
    if -1s == write(STDERR_FILENO, $"\n"a, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func eprintch : (ch : ascii) -> s
{
    if -1s == write(STDERR_FILENO, ?ch, 1u)
    {
        return -1s;
    }
    return 0s;
}

export func file_size : (path : ^ascii) -> s
{
    # In the future it would be nice if we could use `stat` here and just grab
    # the size of the file off of the`.st_size` member from the `struct stat`.
    # Until then we'll have to use the old fseek ftell trick.
    let fp : ^mut FILE = fopen(path, $"rb"a);
    if fp == null
    {
        return -1s;
    }
    defer{fclose(fp);}
    if 0s as sint != fseek(fp, 0s as slong, SEEK_END)
    {
        return -1s;
    }
    return ftell(fp) as s;
}

export func read_file_into_buf : (
    path : ^ascii,
    buf : ^mut void,
    bufsize : u,
    nullterm : bool
) -> s
{
    let fsize : s = file_size(path);
    if fsize == -1s{return -1s;}

    let min_bufsize : mut u = fsize as u;
    if nullterm{min_bufsize = min_bufsize + 1u;}
    if min_bufsize < bufsize{return -1s;}

    let fs : mut fstream = void;
    if !fs.init(path, FSTREAM_OPEN_R){return -1s;}
    defer{fs.fini();}

    let numread : s = fs.read(buf, fsize as u);
    if numread != fsize{return -1s;}

    let buf_as_str : ^mut ascii = buf as ^mut ascii;
    buf_as_str[fsize as u] = '\0'a;

    return fsize;
}

export func read_file_into_string : (path : ^ascii, str : ^mut string) -> s
{
    let fsize : s = file_size(path);
    if fsize == -1s{return -1s;}

    let fs : mut fstream = void;
    if !fs.init(path, FSTREAM_OPEN_R){return -1s;}
    defer{fs.fini();}

    str->resize(fsize as u);
    let numread : s = fs.read(str->_data, fsize as u);
    if numread != fsize
    {
        str->clear();
        return -1s;
    }

    return fsize;
}
