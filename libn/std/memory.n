# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdlib.n"a;
import "std/panic.n"a;

export func default_allocate : (size : u) -> ^mut void;
export func default_reallocate : (mem : ^mut void, size : u) -> ^mut void;
export func default_deallocate : (mem : ^mut void) -> void;

export let allocate_func : mut (u) -> ^mut void = default_allocate;
export let reallocate_func : mut (^mut void, u) -> ^mut void = default_reallocate;
export let deallocate_func : mut (^mut void) -> void = default_deallocate;

export func allocate : (size : u) -> ^mut void;
export func reallocate : (mem : ^mut void, size : u) -> ^mut void;
export func deallocate : (mem : ^mut void) -> void;

export func memory_copy : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void;
export func memory_copy_overlapping : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void;
export func memory_fill : (mem : ^mut void, nbytes : u, value : u8) -> void;
export func memory_swap : (mem1 : ^mut void, mem2 : ^void, nbytes : u) -> void;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let PANIC_ALLOCATE_FAILURE : ^ascii = $"allocate failure"a;
let PANIC_REALLOCATE_FAILURE : ^ascii = $"reallocate failure"a;

export func default_allocate : (size : u) -> ^mut void
{
    return calloc(size, 1u);
}

export func default_reallocate : (mem : ^mut void, size : u) -> ^mut void
{
    return realloc(mem, size);
}

export func default_deallocate : (mem : ^mut void) -> void
{
    free(mem);
}

export func allocate : (size : u) -> ^mut void
{
    let new_mem : ^mut void = allocate_func(size);
    if new_mem == null && size != 0u{panic(PANIC_ALLOCATE_FAILURE);}
    return new_mem;
}

export func reallocate : (mem : ^mut void, size : u) -> ^mut void
{
    let new_mem : ^mut void = reallocate_func(mem, size);
    if new_mem == null && size != 0u{panic(PANIC_REALLOCATE_FAILURE);}
    return new_mem;
}

export func deallocate : (mem : ^mut void) -> void
{
    deallocate_func(mem);
}

export func memory_copy : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void
{
    let dest_byte : mut ^mut u8 = dest as ^mut u8;
    let src_byte : mut ^u8 = src as ^u8;
    let bytes_remaining : mut u = nbytes;
    loop bytes_remaining != 0u
    {
        @dest_byte = @src_byte;
        dest_byte = dest_byte +^ 1u;
        src_byte = src_byte +^ 1u;
        bytes_remaining = bytes_remaining - 1u;
    }
}

export func memory_copy_overlapping : (
    dest : ^mut void,
    src : ^void,
    nbytes : u
) -> void
{
    let tmp : ^mut void = allocate(nbytes);
    defer{deallocate(tmp);}
    memory_copy(tmp, src, nbytes);
    memory_copy(dest, tmp, nbytes);
}

export func memory_fill : (mem : ^mut void, nbytes : u, value : u8) -> void
{
    let byte : mut ^mut u8 = mem as ^mut u8;
    loop nbytes != 0u
    {
        @byte = value;
        byte = byte +^ 1u;
    }
}

export func memory_swap : (mem1 : ^mut void, mem2 : ^void, nbytes : u) -> void
{
    let mem1_byte : mut ^mut u8 = mem1 as ^mut u8;
    let mem2_byte : mut ^mut u8 = mem2 as ^mut u8;
    let tmp_byte : mut ^mut u8 = void;
    loop nbytes != 0u
    {
        @tmp_byte = @mem1_byte;
        @mem1_byte = @mem2_byte;
        @mem2_byte = @tmp_byte;
        mem1_byte = mem1_byte +^ 1u;
        mem2_byte = mem2_byte +^ 1u;
    }
}
