/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/compile.h>
#include <nc/lexer.h>
#include <nc/parser.h>
#include <nc/codegen.h>
#include <nc/link.h>

#define PHASE_COLOR_SEQ ANSI_ESC_COLOR_CYAN

#define STR_PHASE_LEXICAL_ANALYSIS  "LEXICAL ANALYSIS"
#define STR_PHASE_SYNTAX_ANALYSIS   "SYNTAX ANALYSIS"
#define STR_PHASE_SEMANTIC_ANALYSIS "SEMANTIC ANALYSIS"
#define STR_PHASE_CODEGEN           "CODE GENERATION"
#define STR_MODULE_COMPILATION      "MODULE COMPILATION"
#define STR_PHASE_MAX_LENGTH 18

#define _BEGIN_PHASE_LEXICAL_ANALYSIS  "BEGIN LEXICAL ANALYSIS PHASE (%s)"
#define _BEGIN_PHASE_SYNTAX_ANALYSIS   "BEGIN SYNTAX ANALYSIS PHASE (%s)"
#define _BEGIN_PHASE_SEMANTIC_ANALYSIS "BEGIN SEMANTIC ANALYSIS PHASE (%s)"
#define _BEGIN_PHASE_CODEGEN           "BEGIN CODE GENERATION PHASE (%s)"
#define _END_PHASE_LEXICAL_ANALYSIS  "END LEXICAL ANALYSIS PHASE (%s)"
#define _END_PHASE_SYNTAX_ANALYSIS   "END SYNTAX ANALYSIS PHASE (%s)"
#define _END_PHASE_SEMANTIC_ANALYSIS "END SEMANTIC ANALYSIS PHASE (%s)"
#define _END_PHASE_CODEGEN           "END CODE GENERATION PHASE (%s)"

#define BEGIN_PHASE_LEXICAL_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _BEGIN_PHASE_LEXICAL_ANALYSIS)
#define BEGIN_PHASE_SYNTAX_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _BEGIN_PHASE_SYNTAX_ANALYSIS)
#define BEGIN_PHASE_SEMANTIC_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _BEGIN_PHASE_SEMANTIC_ANALYSIS)
#define BEGIN_PHASE_CODEGEN \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _BEGIN_PHASE_CODEGEN)
#define END_PHASE_LEXICAL_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _END_PHASE_LEXICAL_ANALYSIS)
#define END_PHASE_SYNTAX_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _END_PHASE_SYNTAX_ANALYSIS)
#define END_PHASE_SEMANTIC_ANALYSIS \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _END_PHASE_SEMANTIC_ANALYSIS)
#define END_PHASE_CODEGEN \
    ANSI_ESC_IF_TTY(stdout, PHASE_COLOR_SEQ, _END_PHASE_CODEGEN)

#define ERROR_DURING_LEXICAL_ANALYSIS \
    "One or more errors occured during lexical analysis."
#define ERROR_DURING_SYNTAX_ANALYSIS \
    "One or more errors occured during syntax analysis."
#define ERROR_DURING_SEMANTIC_ANALYSIS \
    "One or more errors occured during semantic analysis."

int compile(char const* src_path, struct module* const module)
{
    struct timespec ts_compile_start = {0, 0};
    struct timespec ts_compile_stop = {0, 0};
    struct timespec ts_lexical_analysis_start = {0, 0};
    struct timespec ts_lexical_analysis_stop = {0, 0};
    struct timespec ts_syntax_analysis_start = {0, 0};
    struct timespec ts_syntax_analysis_stop = {0, 0};
    struct timespec ts_semantic_analysis_start = {0, 0};
    struct timespec ts_semantic_analysis_stop = {0, 0};
    struct timespec ts_codegen_start = {0, 0};
    struct timespec ts_codegen_stop = {0, 0};

    long long time_ms_lexical_analysis = 0;
    long long time_ms_syntax_analysis = 0;
    long long time_ms_semantic_analysis = 0;
    long long time_ms_codegen = 0;
    long long time_ms_compile = 0;

    int status = COMPILE_SUCCESS;

    ngettime(&ts_compile_start);
    nlogf(LOG_DEBUG, "Compiling source file '%s'.", src_path);

    nstr_assign_cstr(&module->src_path, src_path);

    char const* file_extension = NULL;
    switch (config.target)
    {
    case TARGET_NATIVE:
        file_extension = "o";
        break;
    case TARGET_C99:
        file_extension = "c";
        break;
    default:
        NPANIC_DFLT_CASE();
    }
    nstr_assign_fmt(
        &module->out_path,
        "%s.%s",
        src_path,
        file_extension
    );
    nlogf(
        LOG_DEBUG,
        "Source file '%s' will be compiled to object file '%s' for native builds.",
        src_path,
        module->out_path.data
    );

    nstr_t nstr_defer_fini touch_system_cmd;
    nstr_init_fmt(
        &touch_system_cmd,
        "mkdir -p `dirname \"%s\"` && touch %s",
        module->out_path.data,
        module->out_path.data
    );
    nlogf(LOG_DEBUG, "Creating output file '%s'.", module->out_path.data);
    nlogf(LOG_DEBUG, "$ %s", touch_system_cmd.data);
    if (0 != system(touch_system_cmd.data))
    {
        nlogf(
            LOG_ERROR,
            "Failed to create output file '%s'.",
            module->out_path.data
        );
        return COMPILE_FAILURE_OTHER;
    }

    // Check to see if this module is already in the module cache.
    nlogf(
        LOG_DEBUG,
        "Checking module cache for duplicate module '%s'.",
        module->src_path.data
    );
    for (size_t i = 0; i < module_cache_len; ++i)
    {
        if (nstr_eq(&module->src_path, &module_cache[i].src_path))
        {
            nlogf(
                LOG_ERROR,
                "Module '%s' is already in the module cache.",
                module->src_path.data
            );
            return COMPILE_FAILURE_OTHER;
        }
    }
    // Module is not already in the cache. Add it to the cache.
    module_cache[module_cache_len] = *module;
    struct module* const cached_module = &module_cache[module_cache_len];
    module_cache_len += 1;

    if (!file_exist(cached_module->src_path.data))
    {
        nlogf(LOG_FATAL, FMT_FAILED_TO_LOCATE_FILE, cached_module->src_path.data);
        return COMPILE_FAILURE_OTHER;
    }
    if (-1 == file_read_into_nstr(cached_module->src_path.data, &cached_module->src))
    {
        nlogf(LOG_FATAL, FMT_FAILED_TO_READ_FROM_FILE, cached_module->src_path.data);
        return COMPILE_FAILURE_OTHER;
    }

    nlogf(LOG_DEBUG, BEGIN_PHASE_LEXICAL_ANALYSIS, src_path);
    ngettime(&ts_lexical_analysis_start);
    if (!dophase_lexical_analysis(cached_module))
    {
        nlogf(LOG_DEBUG, ERROR_DURING_LEXICAL_ANALYSIS);
        status = COMPILE_FAILURE_LEXICAL_ANALYSIS;
        goto cleanup;
    }
    ngettime(&ts_lexical_analysis_stop);
    time_ms_lexical_analysis =
        nelapsed_ms(ts_lexical_analysis_start, ts_lexical_analysis_stop);
    nlogf(LOG_DEBUG, "Total tokens scanned => %zu.", narr_length(cached_module->tokens));
    nlogf(LOG_DEBUG, END_PHASE_LEXICAL_ANALYSIS, src_path);

    nlogf(LOG_DEBUG, BEGIN_PHASE_SYNTAX_ANALYSIS, src_path);
    ngettime(&ts_syntax_analysis_start);
    if (!dophase_syntax_analysis(cached_module))
    {
        nlogf(LOG_DEBUG, ERROR_DURING_SYNTAX_ANALYSIS);
        status = COMPILE_FAILURE_SYNTAX_ANALYSIS;
        goto cleanup;
    }
    ngettime(&ts_syntax_analysis_stop);
    time_ms_syntax_analysis =
        nelapsed_ms(ts_syntax_analysis_start, ts_syntax_analysis_stop);
    nlogf(LOG_DEBUG, END_PHASE_SYNTAX_ANALYSIS, src_path);

    nlogf(LOG_DEBUG, BEGIN_PHASE_SEMANTIC_ANALYSIS, src_path);
    ngettime(&ts_semantic_analysis_start);
    if (!dophase_semantic_analysis(cached_module))
    {
        nlogf(LOG_DEBUG, ERROR_DURING_SEMANTIC_ANALYSIS);
        status = COMPILE_FAILURE_SEMANTIC_ANALYSIS;
        goto cleanup;
    }
    ngettime(&ts_semantic_analysis_stop);
    time_ms_semantic_analysis =
        nelapsed_ms(ts_semantic_analysis_start, ts_semantic_analysis_stop);
    nlogf(LOG_DEBUG, END_PHASE_SEMANTIC_ANALYSIS, src_path);

    nlogf(LOG_DEBUG, BEGIN_PHASE_CODEGEN, src_path);
    ngettime(&ts_codegen_start);
    if (!dophase_codegen(cached_module))
    {
        nlogf(LOG_DEBUG, ERROR_DURING_SEMANTIC_ANALYSIS);
        status = COMPILE_FAILURE_CODEGEN;
        goto cleanup;
    }
    ngettime(&ts_codegen_stop);
    time_ms_codegen = nelapsed_ms(ts_codegen_start, ts_codegen_stop);
    nlogf(LOG_DEBUG, END_PHASE_CODEGEN, src_path);

    ngettime(&ts_compile_stop);
    time_ms_compile = nelapsed_ms(ts_compile_start, ts_compile_stop);

cleanup:
#define LOG_COMPILE_METRIC(phase, time)                                        \
    nlogf(                                                                     \
        LOG_METRIC,                                                            \
        "[%s] %-" STRINGIFY(STR_PHASE_MAX_LENGTH) "s : %lld ms",               \
        cached_module->src_path.data,                                          \
        phase,                                                                 \
        time                                                                   \
    )
    LOG_COMPILE_METRIC(STR_PHASE_LEXICAL_ANALYSIS, time_ms_lexical_analysis);
    LOG_COMPILE_METRIC(STR_PHASE_SYNTAX_ANALYSIS, time_ms_syntax_analysis);
    LOG_COMPILE_METRIC(STR_PHASE_SEMANTIC_ANALYSIS, time_ms_semantic_analysis);
    LOG_COMPILE_METRIC(STR_PHASE_CODEGEN, time_ms_codegen);
    LOG_COMPILE_METRIC(STR_MODULE_COMPILATION, time_ms_compile);

    return status;
}

int compilep(char const* src_path, struct module* const module)
{
    module_cache_init();

    int status = compile(src_path, module);
    if (COMPILE_SUCCESS != status)
    {
        goto cleanup;
    }

    switch (config.target)
    {
    case TARGET_NATIVE:
        if (!config.is_compile_only && !link_modules())
        {
            status = COMPILE_FAILURE_LINKING;
        }
        break;
    case TARGET_C99:
        {
            nstr_t nstr_defer_fini cp_system_cmd;
            nstr_init_fmt(
                &cp_system_cmd,
                "cp %s %s",
                module_cache[0].out_path.data,
                config.output_file
            );
            nlogf(LOG_DEBUG, "$ %s", cp_system_cmd.data);
            if (0 != system(cp_system_cmd.data))
            {
                nlogf(LOG_ERROR, "Failed to copy file.");
                status = STBOOL_FAILURE;
            }
        }
        break;
    default:
        NPANIC_DFLT_CASE();
    }

cleanup:
    if (!config.is_save_temps)
    {
        // Remove all temporary files created from compiling object files.
        for (size_t i = 0; i < module_cache_len; ++i)
        {
            nlogf(LOG_DEBUG, "Removing file '%s'", module_cache[i].out_path.data);
            remove(module_cache[i].out_path.data);
        }
    }

    module_cache_fini();
    NIR_INIT_FINI_COUNTER_VERIFY_BALANCED();
    return status;
}
