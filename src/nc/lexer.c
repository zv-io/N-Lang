/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file lexer.c
 * @brief Lexer for the N Language
 *
 * The lexer works on the principle of an incrementing state machine.
 * The lexer is a simple translation of the regular grammar describing
 * the N language. The regular grammar can be found in the language
 * documentation.
 *
 * Since regular languages are easy to lex, we prefer not to use
 * regular expressions libraries as regex libraries are much more
 * generic and unnecessarily complex for the task of lexing. We also
 * prefer not to use tools like lex/flex as they often generate
 * unnecessarily large state machines for even simple grammars, and
 * they can make it unnecessarily difficult to give good error messages.
 *
 */

#include <nc/lexer.h>
#include <nutils/nutils.h>

//! Perform lexical analysis
//!
//! The result of this operation is module->tokens
//! containing a narray of tokens.
//!
//! For token definition, see file token.h
stbool dophase_lexical_analysis(struct module* module)
{
    // Create the lexer data structure.
    struct Lexer lexer = (struct Lexer){
        .src = module->src.data,
        .line = 1,   // Line numbers are 1-indexed
        .column = 1, // Column numbers are 1-indexed
        .length = 0  // Initial token length
    };

    // status determines if the lexer succeeded.
    // The variable is threaded throughout the computation.
    stbool status = STBOOL_SUCCESS;

    // The start state (and end state) of the state machine
    // recognizing the regular grammar defining the N syntax.
    start_state(module, &lexer, &status);

    return status;
}

//! Increment the lexer state by a given number
void increment_state_machine(struct Lexer* lexer, uint64_t n)
{
    lexer->src += n;
    lexer->column += n;
    lexer->length += n;
}

//! Emit the last seen token and reset the token length
void emit_token(struct module* module, struct Lexer* lexer, enum token_type tag)
{
    struct token tok = (struct token){.tag = tag,
                                      .start = lexer->src - lexer->length,
                                      .module = module,
                                      .line = lexer->line,
                                      .column = lexer->column - lexer->length,
                                      .length = lexer->length};
    module->tokens = narr_push(module->tokens, &tok);

    // Reset token length
    lexer->length = 0;
}

struct trie* trie_alloc()
{
    struct trie* trie = nalloc(sizeof(struct trie));

    if (trie != NULL)
    {
        trie->end_of_word = false;
        for (uint8_t i = 0; i < TRIE_CHILDREN; i++)
        { trie->children[i] = NULL; } trie->token = TOKEN_IDENTIFIER;
    }
    else
    {
        nlogf(LOG_ERROR, "[LEXER] nalloc failed to allocate memory for trie");
    }

    return trie;
}

void trie_insert(struct trie* root, char const* key, enum token_type token)
{
    struct trie* p = root;
    size_t len_key = strlen(key);

    for (size_t i = 0; i < len_key; i += 1)
    {
        uint8_t index = (uint8_t)key[i];
        if (p->children[index] == NULL) { p->children[index] = trie_alloc(); }
        p = p->children[index];
    }
    p->end_of_word = true;
    p->token = token;
}

void trie_setup(struct trie* trie)
{
    trie_insert(trie, "addressof", TOKEN_KEYWORD_ADDRESSOF);
    trie_insert(trie, "alias", TOKEN_KEYWORD_ALIAS);
    trie_insert(trie, "alignas", TOKEN_KEYWORD_ALIGNAS);
    trie_insert(trie, "alignof", TOKEN_KEYWORD_ALIGNOF);
    trie_insert(trie, "as", TOKEN_KEYWORD_AS);
    trie_insert(trie, "bool", TOKEN_KEYWORD_BOOL);
    trie_insert(trie, "break", TOKEN_KEYWORD_BREAK);
    trie_insert(trie, "ascii", TOKEN_KEYWORD_ASCII);
    trie_insert(trie, "continue", TOKEN_KEYWORD_CONTINUE);
    trie_insert(trie, "countof", TOKEN_KEYWORD_COUNTOF);
    trie_insert(trie, "defer", TOKEN_KEYWORD_DEFER);
    trie_insert(trie, "elif", TOKEN_KEYWORD_ELIF);
    trie_insert(trie, "else", TOKEN_KEYWORD_ELSE);
    trie_insert(trie, "export", TOKEN_KEYWORD_EXPORT);
    trie_insert(trie, "extern", TOKEN_KEYWORD_EXTERN);
    trie_insert(trie, "f128", TOKEN_KEYWORD_F128);
    trie_insert(trie, "f16", TOKEN_KEYWORD_F16);
    trie_insert(trie, "f32", TOKEN_KEYWORD_F32);
    trie_insert(trie, "f64", TOKEN_KEYWORD_F64);
    trie_insert(trie, "false", TOKEN_KEYWORD_FALSE);
    trie_insert(trie, "func", TOKEN_KEYWORD_FUNC);
    trie_insert(trie, "if", TOKEN_KEYWORD_IF);
    trie_insert(trie, "import", TOKEN_KEYWORD_IMPORT);
    trie_insert(trie, "in", TOKEN_KEYWORD_IN);
    trie_insert(trie, "let", TOKEN_KEYWORD_LET);
    trie_insert(trie, "loop", TOKEN_KEYWORD_LOOP);
    trie_insert(trie, "mut", TOKEN_KEYWORD_MUT);
    trie_insert(trie, "null", TOKEN_KEYWORD_NULL);
    trie_insert(trie, "restrict", TOKEN_KEYWORD_RESTRICT);
    trie_insert(trie, "return", TOKEN_KEYWORD_RETURN);
    trie_insert(trie, "s", TOKEN_KEYWORD_S);
    trie_insert(trie, "s16", TOKEN_KEYWORD_S16);
    trie_insert(trie, "s32", TOKEN_KEYWORD_S32);
    trie_insert(trie, "s64", TOKEN_KEYWORD_S64);
    trie_insert(trie, "s8", TOKEN_KEYWORD_S8);
    trie_insert(trie, "sizeof", TOKEN_KEYWORD_SIZEOF);
    trie_insert(trie, "struct", TOKEN_KEYWORD_STRUCT);
    trie_insert(trie, "true", TOKEN_KEYWORD_TRUE);
    trie_insert(trie, "typeof", TOKEN_KEYWORD_TYPEOF);
    trie_insert(trie, "u", TOKEN_KEYWORD_U);
    trie_insert(trie, "u16", TOKEN_KEYWORD_U16);
    trie_insert(trie, "u32", TOKEN_KEYWORD_U32);
    trie_insert(trie, "u64", TOKEN_KEYWORD_U64);
    trie_insert(trie, "u8", TOKEN_KEYWORD_U8);
    trie_insert(trie, "void", TOKEN_KEYWORD_VOID);
    trie_insert(trie, "volatile", TOKEN_KEYWORD_VOLATILE);
}

enum token_type trie_lookup(
    struct trie* root, char const* key, uint64_t len_key)
{
    struct trie* p = root;

    for (size_t i = 0; i < len_key; i += 1)
    {
        uint8_t index = (uint8_t)key[i];
        if (!p->children[index]) { return TOKEN_IDENTIFIER; }
        p = p->children[index];
    }

    if (p->end_of_word) { return p->token; }

    return TOKEN_IDENTIFIER;
}

void trie_free(struct trie* trie)
{
    for (size_t i = 0; i < TRIE_CHILDREN; i += 1)
    {
        if (trie->children[i] != NULL) { trie_free(trie->children[i]); }
    }
    nfree(trie);
}

//! start_state represents the starting state (and accepting state)
//! of the state machine recognizing the regular grammar defining
//! the N syntax.
void start_state(struct module* module, struct Lexer* lexer, stbool* status)
{
    struct trie* trie = trie_alloc();
    trie_setup(trie);

    // Step through the stream, character by character, until
    // the end of the stream has been reached
    while (*(lexer->src) != '\0')
    {
        // Comment
        if (*(lexer->src) == '#')
        {
            lexer->src += 1;
            lex_comment(lexer);
        }

        // Newline
        else if (*(lexer->src) == '\n')
        {
            lexer->src += 1;
            lexer->line += 1;
            lexer->column = 1;
        }

        // Space character
        else if (isspace(*(lexer->src)))
        {
            lexer->src += 1;
            lexer->column += 1;
        }

        // Identifier
        //
        // An identifier must start with an underscore, or an ascii letter
        else if (*(lexer->src) == '_' || isalpha(*(lexer->src)))
        {
            increment_state_machine(lexer, 1);
            lex_identifier(module, lexer, trie);
        }

        // String literal
        else if (*(lexer->src) == '"')
        {
            increment_state_machine(lexer, 1);
            lex_string(module, lexer, status);
        }

        // Character literal
        else if (*(lexer->src) == '\'')
        {
            increment_state_machine(lexer, 1);
            lex_char(module, lexer, status);
        }

        // Dash
        else if (*(lexer->src) == '-')
        {
            increment_state_machine(lexer, 1);
            operator_dash(module, lexer, status);
        }

        // Plus
        else if (*(lexer->src) == '+')
        {
            increment_state_machine(lexer, 1);
            operator_plus(module, lexer, status);
        }

        // Hexadecimal literal
        else if (
            *(lexer->src) == '0'
            && ((lexer->src)[1] == 'x' || (lexer->src)[1] == 'X'))
        {
            increment_state_machine(lexer, 2);
            literal_hexadecimal(module, lexer, status);
        }

        // Binary literal
        else if (
            *(lexer->src) == '0'
            && ((lexer->src)[1] == 'b' || (lexer->src)[1] == 'B'))
        {
            increment_state_machine(lexer, 2);
            literal_binary(module, lexer, status);
        }

        // Octal literal
        else if (
            *(lexer->src) == '0'
            && ((lexer->src)[1] == 'o' || (lexer->src)[1] == 'O'))
        {
            increment_state_machine(lexer, 2);
            literal_octal(module, lexer, status);
        }

        // Decimal literal
        else if (*(lexer->src) >= '0' && *(lexer->src) <= '9')
        {
            literal_decimal(module, lexer, status);
        }

        // Boolean AND
        else if (*(lexer->src) == '&' && (lexer->src)[1] == '&')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_AMPERSAND_AMPERSAND);
        }

        // Bitwise AND
        else if (*(lexer->src) == '&')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_AMPERSAND);
        }

        // Logical NOT-EQUAL
        else if (*(lexer->src) == '!' && (lexer->src)[1] == '=')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_BANG_EQUAL);
        }

        // Logical NOT
        else if (*(lexer->src) == '!')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_BANG);
        }

        // Type of
        else if (*(lexer->src) == ':')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_COLON);
        }

        // ...
        else if (*(lexer->src) == '.' && (lexer->src)[1] == '.')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_DOT_DOT);
        }

        // Dot operator
        else if (*(lexer->src) == '.')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_DOT);
        }

        // Logical Equality
        else if (*(lexer->src) == '=' && (lexer->src)[1] == '=')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_EQUAL_EQUAL);
        }

        // Assignment
        else if (*(lexer->src) == '=')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_EQUAL);
        }

        // Logical Inequality
        else if (*(lexer->src) == '<' && (lexer->src)[1] == '=')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_LESSTHAN_EQUAL);
        }

        // SHIFT LEFT
        else if (*(lexer->src) == '<' && (lexer->src)[1] == '<')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_LESSTHAN_LESSTHAN);
        }

        // Logical Inequality
        else if (*(lexer->src) == '<')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_LESSTHAN);
        }

        // Logical Inequality
        else if (*(lexer->src) == '>' && (lexer->src)[1] == '=')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_GREATERTHAN_EQUAL);
        }

        // SHIFT RIGHT
        else if (*(lexer->src) == '>' && (lexer->src)[1] == '>')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_GREATERTHAN_GREATERTHAN);
        }

        // Logical Inequality
        else if (*(lexer->src) == '>')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_GREATERTHAN);
        }

        // Boolean OR
        else if (*(lexer->src) == '|' && (lexer->src)[1] == '|')
        {
            increment_state_machine(lexer, 2);
            emit_token(module, lexer, TOKEN_OPERATOR_PIPE_PIPE);
        }

        // Bitwise OR
        else if (*(lexer->src) == '|')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_PIPE);
        }

        // Multiplication
        else if (*(lexer->src) == '*')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_ASTERISK);
        }

        // Pointer deference
        else if (*(lexer->src) == '@')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_AT);
        }

        // Pointer
        else if (*(lexer->src) == '^')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_CARET);
        }

        // Comma
        else if (*(lexer->src) == ',')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_COMMA);
        }

        // Array decay
        else if (*(lexer->src) == '$')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_DOLLAR);
        }

        // Brace
        else if (*(lexer->src) == '{')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_LEFTBRACE);
        }

        // Bracket
        else if (*(lexer->src) == '[')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_LEFTBRACKET);
        }

        // Parenthesis
        else if (*(lexer->src) == '(')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_LEFTPARENTHESIS);
        }

        // Question Mark
        else if (*(lexer->src) == '?')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_QUESTION);
        }

        // Brace
        else if (*(lexer->src) == '}')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_RIGHTBRACE);
        }

        // Bracket
        else if (*(lexer->src) == ']')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_RIGHTBRACKET);
        }

        // Parenthesis
        else if (*(lexer->src) == ')')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_RIGHTPARENTHESIS);
        }

        // Semicolon
        else if (*(lexer->src) == ';')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_SEMICOLON);
        }

        // Division
        else if (*(lexer->src) == '/')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_SLASH);
        }

        // Bitwise NOT
        else if (*(lexer->src) == '~')
        {
            increment_state_machine(lexer, 1);
            emit_token(module, lexer, TOKEN_OPERATOR_TILDE);
        }

        // NOTE: Fail on seeing a character which is not allowed
        // by the regular grammar.
        else
        {
            *status = STBOOL_FAILURE;
            nlogf_flc(
                LOG_ERROR,
                module->src_path.data,
                lexer->line,
                lexer->column,
                "Unknown character in lexer");
            return;
        }
    }

    // Upon finishing consuming the file data, emit End-Of-File (EOF)
    emit_token(module, lexer, TOKEN_EOF);

    trie_free(trie);

    return;
}

//! Consume until the end of a line
//!
//! There is no need to update the column count since the
//! column count will be reset by the newline
void lex_comment(struct Lexer* lexer)
{
    while (*(lexer->src) != '\0' && *(lexer->src) != '\n') { lexer->src += 1; }
}

//! Lex the remainder of the identifier
//!
//! Check if an identifier is a keyword
//!
//! If the identifier is a keyword, then emit the keyword.
//! Else, emit the identifier.
void lex_identifier(
    struct module* module, struct Lexer* lexer, struct trie* trie)
{
    // An identifier can have underscores, ascii letters, and ascii numbers
    while (*(lexer->src) == '_' || isalnum(*(lexer->src)))
    { increment_state_machine(lexer, 1); }

    enum token_type token =
        trie_lookup(trie, lexer->src - lexer->length, lexer->length);
    emit_token(module, lexer, token);
}

//! Lex the remainder of a string literal
void lex_string(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Consume until the end of the string, or escape character, or
    // newline character, or end of file
    while (*(lexer->src) != '"' && *(lexer->src) != '\\'
           && *(lexer->src) != '\r' && *(lexer->src) != '\n'
           && *(lexer->src) != '\0')
    { increment_state_machine(lexer, 1); }

    // Upon seeing the end of the string, emit the string
    if (*(lexer->src) == '"' && (lexer->src)[1] == 'a')
    {
        increment_state_machine(lexer, 2);
        emit_token(module, lexer, TOKEN_LITERAL_STRING);
    }

    // Upon seeing a newline, error
    //
    // Currently multi-line strings are not allowed.
    // This may be changed in the future.
    else if (*(lexer->src) == '\r' || *(lexer->src) == '\n')
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Multi-line strings are not allowed");
        return;
    }

    // Handle an escape character
    //
    // XXX: This will push a function call onto the stack
    // There can be a stack overflow upon seeing enough escape
    // characters.
    else if (*(lexer->src) == '\\')
    {
        increment_state_machine(lexer, 1);

        lex_string_escaped_char(module, lexer, status);
    }

    // Handle end of file
    else if (*(lexer->src) == '\0')
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Unterminated string");
        return;
    }
}

//! Function that validates that an escape sequence is valid in N
void lex_string_escaped_char(
    struct module* module, struct Lexer* lexer, stbool* status)
{
    char c = *(lexer->src);

    // Valid escape characters
    if (c == '0' || c == 'a' || c == 'b' || c == 'e' || c == 'f' || c == 'n'
        || c == 'r' || c == 't' || c == 'v' || c == '\'' || c == '"'
        || c == '\\')
    {
        increment_state_machine(lexer, 1);
        lex_string(module, lexer, status);
    }

    // Invalid escape character
    else
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Unknown escape character in string");
        return;
    }
}

//! Lex a character
void lex_char(struct module* module, struct Lexer* lexer, stbool* status)
{
    if (*(lexer->src) != '\\' && (lexer->src)[1] == '\''
        && (lexer->src)[2] == 'a')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_ASCII);
    }

    // Lex an escaped character
    else if (
        *(lexer->src) == '\\'
        && ((lexer->src)[1] == '0' || (lexer->src)[1] == 'a'
            || (lexer->src)[1] == 'b' || (lexer->src)[1] == 'e'
            || (lexer->src)[1] == 'f' || (lexer->src)[1] == 'n'
            || (lexer->src)[1] == 'r' || (lexer->src)[1] == 't'
            || (lexer->src)[1] == 't' || (lexer->src)[1] == 'v'
            || (lexer->src)[1] == '\'' || (lexer->src)[1] == '"'
            || (lexer->src)[1] == '\\')
        && (lexer->src)[2] == '\'' && (lexer->src)[3] == 'a')
    {
        increment_state_machine(lexer, 4);
        emit_token(module, lexer, TOKEN_LITERAL_ASCII);
    }

    // Error on bad character format
    else
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Unable to lex character\n");
    }
}

//! Proceed on seeing -
void operator_dash(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Pointer difference
    if (*(lexer->src) == '^' && (lexer->src)[1] == '^')
    {
        increment_state_machine(lexer, 2);
        emit_token(module, lexer, TOKEN_OPERATOR_DASH_CARET_CARET);
    }

    // Pointer subtraction
    else if (*(lexer->src) == '^')
    {
        increment_state_machine(lexer, 1);
        emit_token(module, lexer, TOKEN_OPERATOR_DASH_CARET);
    }

    // Return type
    else if (*(lexer->src) == '>')
    {
        increment_state_machine(lexer, 1);
        emit_token(module, lexer, TOKEN_OPERATOR_DASH_GREATERTHAN);
    }

    // Negative Hexadecimal literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'x' || (lexer->src)[1] == 'X'))
    {
        increment_state_machine(lexer, 2);
        literal_hexadecimal(module, lexer, status);
    }

    // Negative Binary literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'b' || (lexer->src)[1] == 'B'))
    {
        increment_state_machine(lexer, 2);
        literal_binary(module, lexer, status);
    }

    // Negative Octal literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'o' || (lexer->src)[1] == 'O'))
    {
        increment_state_machine(lexer, 2);
        literal_octal(module, lexer, status);
    }

    // Negative Decimal literal
    else if (*(lexer->src) >= '0' && *(lexer->src) <= '9')
    {
        literal_decimal(module, lexer, status);
    }

    // Subtraction
    else
    {
        emit_token(module, lexer, TOKEN_OPERATOR_DASH);
    }
}

//! Proceed on seeing +
void operator_plus(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Pointer addition
    if (*(lexer->src) == '^')
    {
        increment_state_machine(lexer, 1);
        emit_token(module, lexer, TOKEN_OPERATOR_PLUS_CARET);
    }

    // Positive Hexadecimal literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'x' || (lexer->src)[1] == 'X'))
    {
        increment_state_machine(lexer, 2);
        literal_hexadecimal(module, lexer, status);
    }

    // Positive Binary literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'b' || (lexer->src)[1] == 'B'))
    {
        increment_state_machine(lexer, 2);
        literal_binary(module, lexer, status);
    }

    // Positive Octal literal
    else if (
        *(lexer->src) == '0'
        && ((lexer->src)[1] == 'o' || (lexer->src)[1] == 'O'))
    {
        increment_state_machine(lexer, 2);
        literal_octal(module, lexer, status);
    }

    // Positive Decimal literal
    else if (isdigit(*(lexer->src)))
    {
        literal_decimal(module, lexer, status);
    }

    // Addition
    else
    {
        emit_token(module, lexer, TOKEN_OPERATOR_PLUS);
    }
}

//! Proceed to lex a hexadecimal literal
void literal_hexadecimal(
    struct module* module, struct Lexer* lexer, stbool* status)
{
    // Validate that at least one digit exists
    if (!isxdigit(*(lexer->src)))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Hexadecimal literal with no digits");
    }

    // Consume all digits
    while (isxdigit(*(lexer->src))) { increment_state_machine(lexer, 1); }

    // Consume data type
    numeric_datatype(module, lexer, status);
}

//! Proceed to lex an octal literal
void literal_octal(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Validate that at least one digit exists
    if (!(*(lexer->src) >= '0' && *(lexer->src) <= '7'))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Octal literal with no digits");
    }

    // Consume all digits
    while (*(lexer->src) >= '0' && *(lexer->src) <= '7')
    { increment_state_machine(lexer, 1); } // Consume data type
    numeric_datatype(module, lexer, status);
}

//! Proceed to lex a binary literal
void literal_binary(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Validate that at least one digit exists
    if (!(*(lexer->src) >= '0' && *(lexer->src) <= '1'))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Binary literal with no digits");
    }

    // Consume all digits
    while (*(lexer->src) >= '0' && *(lexer->src) <= '1')
    { increment_state_machine(lexer, 1); } // Consume data type
    numeric_datatype(module, lexer, status);
}

//! Proceed to lex a decimal literal
void literal_decimal(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Validate that at least one digit exists
    if (!isdigit(*(lexer->src)))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Decimal literal with no digits");
    }

    // Consume all digits
    while (isdigit(*(lexer->src))) { increment_state_machine(lexer, 1); }

    // Consume optional floating point digits
    if (*(lexer->src) == '.')
    {
        increment_state_machine(lexer, 1);
        literal_float(module, lexer, status);
    }

    // Consume data type
    else
    {
        numeric_datatype(module, lexer, status);
    }
}

//! Proceed to lex a floating point literal
void literal_float(struct module* module, struct Lexer* lexer, stbool* status)
{
    // Validate that at least one digit exists
    if (!isdigit(*(lexer->src)))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Floating point literal with no digits");
    }

    // Consume all digits
    while (isdigit(*(lexer->src))) { increment_state_machine(lexer, 1); }

    // Consume optional exponent
    if (*(lexer->src) == 'e' || *(lexer->src) == 'E')
    {
        increment_state_machine(lexer, 1);
        literal_exponent(module, lexer, status);
    }

    // Consume data type
    else
    {
        numeric_datatype(module, lexer, status);
    }
}

//! Proceed to consume an exponentiated floating point literal
void literal_exponent(
    struct module* module, struct Lexer* lexer, stbool* status)
{
    // Consume optional sign
    if (*(lexer->src) == '+' || *(lexer->src) == '-')
    { increment_state_machine(lexer, 1); }

    // Validate that at least one digit exists
    if (!(*(lexer->src) >= '0' && *(lexer->src) <= '9'))
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Exponent with no digits");
    }

    // Consume all digits
    while (*(lexer->src) >= '0' && *(lexer->src) <= '9')
    { increment_state_machine(lexer, 1); } // Consume data type
    numeric_datatype(module, lexer, status);
}

//! Lex a numeric data-type suffix
void numeric_datatype(
    struct module* module, struct Lexer* lexer, stbool* status)
{
    // u64
    if (*(lexer->src) == 'u' && (lexer->src)[1] == '6'
        && (lexer->src)[2] == '4')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_U64);
    }

    // u32
    else if (
        *(lexer->src) == 'u' && (lexer->src)[1] == '3'
        && (lexer->src)[2] == '2')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_U32);
    }

    // u16
    else if (
        *(lexer->src) == 'u' && (lexer->src)[1] == '1'
        && (lexer->src)[2] == '6')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_U16);
    }

    // u8
    else if (*(lexer->src) == 'u' && (lexer->src)[1] == '8')
    {
        increment_state_machine(lexer, 2);
        emit_token(module, lexer, TOKEN_LITERAL_U8);
    }

    // u
    else if (*(lexer->src) == 'u')
    {
        increment_state_machine(lexer, 1);
        emit_token(module, lexer, TOKEN_LITERAL_U);
    }

    // s64
    else if (
        *(lexer->src) == 's' && (lexer->src)[1] == '6'
        && (lexer->src)[2] == '4')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_S64);
    }

    // s32
    else if (
        *(lexer->src) == 's' && (lexer->src)[1] == '3'
        && (lexer->src)[2] == '2')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_S32);
    }

    // s16
    else if (
        *(lexer->src) == 's' && (lexer->src)[1] == '1'
        && (lexer->src)[2] == '6')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_S16);
    }

    // s8
    else if (*(lexer->src) == 's' && (lexer->src)[1] == '8')
    {
        increment_state_machine(lexer, 2);
        emit_token(module, lexer, TOKEN_LITERAL_S8);
    }

    // s
    else if (*(lexer->src) == 's')
    {
        increment_state_machine(lexer, 1);
        emit_token(module, lexer, TOKEN_LITERAL_S);
    }

    // f16
    else if (
        *(lexer->src) == 'f' && (lexer->src)[1] == '1'
        && (lexer->src)[2] == '6')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_F16);
    }

    // f32
    else if (
        *(lexer->src) == 'f' && (lexer->src)[1] == '3'
        && (lexer->src)[2] == '2')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_F32);
    }

    // f64
    else if (
        *(lexer->src) == 'f' && (lexer->src)[1] == '6'
        && (lexer->src)[2] == '4')
    {
        increment_state_machine(lexer, 3);
        emit_token(module, lexer, TOKEN_LITERAL_F64);
    }

    // f128
    else if (
        *(lexer->src) == 'f' && (lexer->src)[1] == '1' && (lexer->src)[2] == '2'
        && (lexer->src)[3] == '8')
    {
        increment_state_machine(lexer, 4);
        emit_token(module, lexer, TOKEN_LITERAL_F128);
    }

    // Unhandled case
    else
    {
        *status = STBOOL_FAILURE;
        nlogf_flc(
            LOG_ERROR,
            module->src_path.data,
            lexer->line,
            lexer->column,
            "Unable to lex numeric datatype");
    }
}
