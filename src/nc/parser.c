/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/parser.h>

#include <stdio.h>

#define STR_PREFIX_NON    "non-"
#define STR_SEPARATOR_AND " and "
#define STR_SEPARATOR_OR  " or "

#define STR_LEFT_LOWER    "left"
#define STR_LEFT_CAPITAL  "Left"
#define STR_RIGHT_LOWER   "right"
#define STR_RIGHT_CAPITAL "Right"

#define STR_PARENTHESES_LOWER   "parentheses"
#define STR_PARENTHESES_CAPITAL "Parentheses"

#define STR_ID         "identifier"
#define STR_DT         "data type"
#define STR_STRUCT_DEF "struct definition"
#define STR_FUNC_SIG   "function signature"

#define STR_LITERAL_U8    "u8 literal"
#define STR_LITERAL_U16   "u16 literal"
#define STR_LITERAL_U32   "u32 literal"
#define STR_LITERAL_U64   "u64 literal"
#define STR_LITERAL_U     "u literal"
#define STR_LITERAL_S8    "s8 literal"
#define STR_LITERAL_S16   "s16 literal"
#define STR_LITERAL_S32   "s32 literal"
#define STR_LITERAL_S64   "s64 literal"
#define STR_LITERAL_S     "s literal"
#define STR_LITERAL_CHAR  "character literal"
#define STR_LITERAL_STR   "string literal"
#define STR_LITERAL_NULL  "null"
#define STR_LITERAL_ARRAY "array literal"

#define STR_EXPR            "expression"
#define STR_PRIMARY_LOWER   "primary"
#define STR_POSTFIX_LOWER   "postfix"
#define STR_UNARY_LOWER     "unary"
#define STR_BINARY_LOWER    "binary"
#define STR_PRIMARY_CAPITAL "Primary"
#define STR_POSTFIX_CAPITAL "Postfix"
#define STR_UNARY_CAPITAL   "Unary"
#define STR_BINARY_CAPITAL  "Binary"

#define STR_VAR_DECL  "variable declaration"
#define STR_FUNC_DECL "function declaration"

#define STR_IF          "if"
#define STR_ELIF        "elif"
#define STR_ELSE        "else"
#define STR_CONDITIONAL "conditional expression"
#define STR_LOOP_BODY   "loop body"

#define STR_COLON            "colon ':'"
#define STR_SEMICOLON        "semicolon ';'"
#define STR_COMMA            "comma ','"
#define STR_PAREN_L          "opening parenthesis '('"
#define STR_PAREN_R          "closing parenthesis ')'"
#define STR_CURLY_BRACKET_L  "opening curly bracket '{'"
#define STR_CURLY_BRACKET_R  "closing curly bracket '}'"
#define STR_SQUARE_BRACKET_L "opening square bracket '['"
#define STR_SQUARE_BRACKET_R "closing square bracket ']'"
#define STR_TOKEN_OPERATOR_ASSIGN  "assignment operator '='"
#define STR_IN               "'in' keyword"

#define STR_VOID             "void"
#define STR_INTEGER          "integer"
#define STR_INTEGER_UNSIGNED "unsigned integer"
#define STR_INTEGER_SIGNED   "signed integer"
#define STR_FLOATING_POINT   "floating point"
#define STR_BOOL             "bool"
#define STR_CHAR             "char"
#define STR_FUNCTION         "function"
#define STR_STRUCT           "struct"
#define STR_BASE             "base"
#define STR_POINTER          "pointer"
#define STR_ARRAY            "array"

#define STR_MUTABLE          "mutable"
#define STR_IMMUTABLE        "immutable"

#define STR_STMT       "statement"
#define STR_FUNC_BODY  "function body"
#define STR_STRUCT_DEF "struct definition"

#define STR_EXPECTED(expected_cstr) \
    "Expected " expected_cstr "."
#define STR_EXPECTED_AFTER(expected_cstr, after_cstr) \
    "Expected " expected_cstr " after " after_cstr "."
#define STR_EXPECTED_X_TYPE(expected_type_cstr) \
    "Expected " expected_type_cstr " type."
#define STR_EXPECTED_X_TYPE_AFTER(expected_type_cstr, after_cstr) \
    "Expected " expected_type_cstr " type after " after_cstr "."
#define FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(expected_type_cstr) \
    "Expression has type '%s'. Expected " expected_type_cstr " type."

#define STR_EXPORT_OF_NON_GLOBAL_DECLARATION \
    "Export of non-global declaration."
#define STR_EXTERN_OF_NON_GLOBAL_DECLARATION \
    "Extern of non-global declaration."

#define FMT_ILLEGAL_USE_OF_DATA_TYPE \
    "Illegal data type '%s'."
#define FMT_NO_DATA_TYPE_MATCHING_IDENTIFIER \
    "No data type matches identifier '%.*s'."

#define STR_ILLEGAL_INIT_EXPR \
    "Illegal initialization expression."

#define STR_USE_OF_COMPOSITE_TYPE_REQUIRES_SIZEOF_KNOWLEDGE \
    "Use of undefined composite type requires sizeof knowledge."

#define STR_STRUCT_HAS_NO_MEMBER_VARIABLES \
    "Struct has no member variables."
#define FMT_MEMBER_NOT_IN_STRUCT \
    "No member named '%.*s' in struct '%.*s'."
#define FMT_DUPLICATE_STRUCT_ID \
    "Duplicate identifier '%.*s' in struct."
#define FMT_MEMBER_FUNC_NO_TARGET \
    "Member function '%.*s' references invalid target."
#define FMT_MEMBER_FUNC_TARGET_NOT_FUNC \
    "Member function '%.*s' references a non-function target."

#define FMT_CONDITIONAL_EXPR_NON_BOOL_TYPE_NOT_ALLOWED \
    "Conditional expression of non-bool type '%s' is not allowed."

#define STR_FOR_LOOP_INIT_MUST_BE_A_VARIABLE_DECLARATION_OR_EXPR_OR_EMPTY_STMT \
    "For-loop initialization statement must be a variable declaration, expression, or empty statement."

#define STR_ALIAS_OUTERMOST_MUST_BE_UNQUALIFIED \
    "Outermost data type modifier/data type base must be unqualified for aliasing."

#define FMT_MISMATCHED_NUMBER_OF_ARGS \
    "Mismatched number of function arguments (expected %zu, received %zu)."

#define FMT_MISMATCHED_DATA_TYPES_IN_ARRAY_LITERAL \
    "Mismatched data types '%s' and  '%s' in array literal."
#define STR_ARRAY_LITERAL_REQUIRES_AT_LEAST_ONE_NON_FILL_ELEMENT \
    "Array literal requires at least one non-fill element."
#define STR_INVALID_ARRAY_LITERAL_FILL_LENGTH \
    "Invalid array-literal fill length."
#define STR_ILLEGAL_USE_OF_QUALIFIERS_ON_ARRAY_TYPE \
    "Illegal use of qualifiers on an array type."
#define STR_ARRAY_MUST_NOT_HAVE_LENGTH_ZERO \
    "Array must not have length zero."

#define STR_NON_CONSTEXPR_EXPR_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Non-constexpr expression not allowed at global scope."
#define STR_NON_CONSTEXPR_INITIALIZATION_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Non-constexpr initialization not allowed at global scope."
#define STR_CONDITIONAL_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Conditional statement not allowed at global scope."
#define STR_LOOP_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Loop not allowed at global scope."
#define STR_STANDALONE_EXPR_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Standalone expression statement not allowed at global scope."
#define STR_RETURN_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Return statement not allowed at global scope."
#define STR_STANDALONE_SCOPE_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Standalone scope-block not allowed at global scope."
#define STR_DEFER_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "Defer statement not allowed at global scope."

#define STR_IMPORT_FAILED \
    "Import failed."
#define STR_IMPORT_ONLY_ALLOWED_AT_GLOBAL_SCOPE \
    "Importing is only allowed at global scope."
#define STR_ALIAS_ONLY_ALLOWED_AT_GLOBAL_SCOPE \
    "Aliasing is only allowed at global scope."
#define STR_STRUCT_DECL_ONLY_ALLOWED_AT_GLOBAL_SCOPE \
    "Struct declarations are only allowed at global scope."
#define STR_STRUCT_DOT_MEMBER_FUNC_CALL_ONLY_ALLOWED_ON_LVALUE \
    "Dot invocation of a member function is only allowed on lvalue structs."

#define FMT_USE_OF_UNDECLARED_IDENTIFIER \
    "Use of undeclared identifier '%.*s'."
#define FMT_REDECLARATION_OF_DEFINED_IDENTIFIER \
    "Redeclaration of identifier '%.*s' previously defined at %s:%zu:%zu."

#define FMT_XEXPR_NOT_ALLOWED_AT_GLOBAL_SCOPE \
    "%s expression not allowed at global scope."

#define FMT_CANNOT_PASS_ARRAY_BY_VALUE_TO_FUNC \
    "Functions do not support array pass by value."

#define FMT_UOP_ADDR_OF_MUST_BE_APPLIED_TO_LVALUE \
    "Address-of operator '%s' must be applied to an lvalue."
#define FMT_UOP_DEREF_MUST_BE_APPLIED_TO_POINTER \
    "Dereference operator '%s' must be applied to a pointer."
#define FMT_UOP_DECAY_MUST_BE_APPLIED_TO_ARRAY \
    "Decay operator '%s' must be applied to an array."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_NUMERIC_TYPES \
    "Unary %s '%s' must only be applied to numeric types."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_INTEGER_TYPES \
    "Unary %s '%s' must only be applied to integer types."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_UNSIGNED_INTEGER_TYPES \
    "Unary %s '%s' must only be applied to unsigned integer types."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_SIGNED_INTEGER_TYPES \
    "Unary %s '%s' must only be applied to signed integer types."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_SIGNED_INTEGER_AND_FLOAT_TYPES \
    "Unary %s '%s' must only be applied to signed integer and floating point types."
#define FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_BOOL_TYPE \
    "Unary %s '%s' must only be applied to the bool type."
#define FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES \
    "Unary 'countof' must only be applied to array types."
#define FMT_UOP_CANNOT_TAKE_COUNTOF_VOID_TYPE \
    "Cannot take the countof a void type."
#define FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE \
    "Cannot take the sizeof a void type."

#define FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE \
    "Illegal data type '%s' as %s hand side of binary operator '%s'."
#define FMT_BOP_MISMATCHED_DATA_TYPES \
    "Mismatched data types '%s' and '%s' in binary expression."
#define FMT_BOP_ASSIGNMENT_LHS_NOT_MUT \
    "Left hand side of assignment expression is not mutable."
#define FMT_BOP_ASSIGNMENT_LHS_NOT_LVALUE \
    "Left hand side of assignment expression is not an lvalue."
#define FMT_BOP_ASSIGNMENT_RHS_NOT_RVALUE \
    "Right hand side of assignment expression is not an rvalue."
#define FMT_BOP_LOGICAL_XHS_MUST_BE_BOOL \
    "%s hand side of logical expression must be a bool type."
#define FMT_BOP_XHS_MUST_BE_DATA_TYPE \
    "%s hand side of binary expression must be a data type."
#define FMT_BOP_XHS_MUST_NOT_BE_DATA_TYPE \
    "%s hand side of binary expression must not be a data type."

#define FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS \
    "Cannot implicitly convert right hand side data type '%s' to left hand side data type '%s'."
#define FMT_CANNOT_EXPLICITLY_CONVERT_LHS_TO_RHS \
    "Cannot explicitly convert left hand side data type '%s' to right hand side data type '%s'."
#define FMT_CANNOT_IMPLICITLY_CONVERT_RTN_TYPE \
    "Cannot implicitly convert expression of type '%s' to return type '%s'."
#define FMT_CANNOT_IMPLICITLY_CONVERT_ARGUMENT_TYPE_TO_PARAM_TYPE \
    "Cannot implicitly convert argument %zu type '%s' to parameter type '%s'."
#define FMT_CANNOT_IMPLICITLY_LOOP_VAR_TO_LOOP_RANGE_LOWER \
    "Cannot implicitly convert loop variable type '%s' to lower range type '%s'."
#define FMT_CANNOT_IMPLICITLY_LOOP_VAR_TO_LOOP_RANGE_UPPER \
    "Cannot implicitly convert loop variable type '%s' to upper range type '%s'."

#define FMT_STMT_RETURN_NOT_WITHIN_FUNCTION \
    "Return statement is not within a function."
#define FMT_STMT_RETURN_LACKS_EXPR \
    "Return statement lacks expression in function with non-void return type."

#define STR_BREAK_OUTSIDE_OF_LOOP \
    "'break' outside of loop."
#define STR_BREAK_WITHIN_DEFERRED_SCOPE \
    "'break' within deferred scope."
#define STR_CONTINUE_OUTSIDE_OF_LOOP \
    "'continue' outside of loop."
#define STR_CONTINUE_WITHIN_DEFERRED_SCOPE \
    "'continue' within deferred scope."

#define FMT_ADDING_SYMBOL_TO_MODULE_EXPORTS \
    "Adding symbol '%.*s' to the exports of module '%s'."

#define FMT_FAILED_RESOLVE_PATH \
    "Failed to resolve path '%s'."

#define MATCH_EXPECTED(_match_stmt, _save_tok, _expected_cstr, ...)            \
__extension__({                                                                \
    if (UNLIKELY(!(_match_stmt)))                                              \
    {                                                                          \
        NLOGF_FLC_TOK(LOG_ERROR, _save_tok, _expected_cstr);                   \
        __VA_ARGS__                                                            \
    }                                                                          \
})

bool match_terminal(struct token** next, enum token_type ttype)
{
    return (*(*next)++).tag == ttype;
}

void log_redeclaration_of_defined_identifier(
    struct symbol const* existing_symbol,
    struct id const* new_id
)
{
    struct id const* id = existing_symbol->id;
    NLOGF_FLC_TOK(
        LOG_ERROR,
        new_id->srctok,
        FMT_REDECLARATION_OF_DEFINED_IDENTIFIER,
        (int)id->length,
        id->start,
        id->srctok->module->src_path.data,
        id->srctok->line,
        id->srctok->column
    );
}

void processing_context_init(
    struct processing_context* ctx,
    struct module* module
)
{
    nassert(NULL != module);
    ctx->module = module;
    ctx->curr_scope = NULL; // Start out in no scope.
    ctx->curr_func = NULL;
}

void processing_context_fini(struct processing_context* ctx)
{
    SUPPRESS_UNUSED(ctx);
}

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
stbool prod_id(struct id* id, struct token** next)
{
    struct token* save = *next;

    if (match_terminal(next, TOKEN_IDENTIFIER))
    {
        id->start = save->start;
        id->length = save->length;
        id->srctok = save;
        return STBOOL_SUCCESS;
    }

    return STBOOL_FAILURE;
}

stbool proc_id(struct id* id, struct processing_context* ctx)
{
    SUPPRESS_UNUSED(id);
    SUPPRESS_UNUSED(ctx);
    return STBOOL_SUCCESS;
}

//============================================================================//
//      QUALIFIERS                                                            //
//============================================================================//
stbool prod_qualifiers(struct qualifiers* q, struct token** next)
{
    struct token* save = *next;

    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_MUT)
    )
    {
        q->is_mut = true;
        save = *next;
    }

    // This function is successful even if no qualifiers are parsed since a
    // group of qualifiers where no qualifiers were specified is still a valid
    // set of qualifiers.
    *next = save;
    return STBOOL_SUCCESS;
}

stbool proc_qualifiers(struct qualifiers* q, struct processing_context* ctx)
{
    SUPPRESS_UNUSED(q);
    SUPPRESS_UNUSED(ctx);
    return STBOOL_SUCCESS;
}

//============================================================================//
//      DATA TYPES                                                            //
//============================================================================//
//====================================//
//      DATA TYPE                     //
//====================================//
stbool prod_dt(struct dt* dt, struct token** next)
{
    struct token* save = *next;
    dt->srctok = save;

    // <qualifiers> ...
    if (!prod_qualifiers(&dt->qualifiers, next))
    {
        return STBOOL_FAILURE;
    }
    save = *next;

    // ... '^' <data-type>
    struct dt inner;
    dt_init(&inner);
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_CARET)
        && prod_dt(&inner, next)
    )
    {
        dt->inner = nalloc(sizeof(typeof(*dt->inner)));
        *dt->inner = inner;
        dt->tag = DT_POINTER;
        return STBOOL_SUCCESS;
    }
    // ... '[' <literal-u> ']' <data-type>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_LEFTBRACKET)
            /* update save for integer parsing */
            && (save = *next)
        && match_terminal(next, TOKEN_LITERAL_U)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTBRACKET)
        && prod_dt(&inner, next)
    )
    {
        dt->inner = nalloc(sizeof(typeof(*dt->inner)));
        *dt->inner = inner;
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U == val.tag);
        dt->tag = DT_ARRAY;
        dt->array.length = val.u;
        return STBOOL_SUCCESS;
    }
    dt_fini(&inner);
    // ... 'void'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_VOID)
    )
    {
        dt->tag = DT_VOID;
        return STBOOL_SUCCESS;
    }
    // ... 'u8'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_U8)
    )
    {
        dt->tag = DT_U8;
        return STBOOL_SUCCESS;
    }
    // ... 'u16'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_U16)
    )
    {
        dt->tag = DT_U16;
        return STBOOL_SUCCESS;
    }
    // ... 'u32'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_U32)
    )
    {
        dt->tag = DT_U32;
        return STBOOL_SUCCESS;
    }
    // ... 'u64'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_U64)
    )
    {
        dt->tag = DT_U64;
        return STBOOL_SUCCESS;
    }
    // ... 'u'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_U)
    )
    {
        dt->tag = DT_U;
        return STBOOL_SUCCESS;
    }
    // ... 's8'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_S8)
    )
    {
        dt->tag = DT_S8;
        return STBOOL_SUCCESS;
    }
    // ... 's16'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_S16)
    )
    {
        dt->tag = DT_S16;
        return STBOOL_SUCCESS;
    }
    // ... 's32'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_S32)
    )
    {
        dt->tag = DT_S32;
        return STBOOL_SUCCESS;
    }
    // ... 's64'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_S64)
    )
    {
        dt->tag = DT_S64;
        return STBOOL_SUCCESS;
    }
    // ... 's'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_S)
    )
    {
        dt->tag = DT_S;
        return STBOOL_SUCCESS;
    }
    // ... 'f16'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_F16)
    )
    {
        dt->tag = DT_F16;
        return STBOOL_SUCCESS;
    }
    // ... 'f32'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_F32)
    )
    {
        dt->tag = DT_F32;
        return STBOOL_SUCCESS;
    }
    // ... 'f64'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_F64)
    )
    {
        dt->tag = DT_F64;
        return STBOOL_SUCCESS;
    }
    // ... 'f128'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_F128)
    )
    {
        dt->tag = DT_F128;
        return STBOOL_SUCCESS;
    }
    // ... 'bool'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_BOOL)
    )
    {
        dt->tag = DT_BOOL;
        return STBOOL_SUCCESS;
    }
    // ... 'char'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_ASCII)
    )
    {
        dt->tag = DT_ASCII;
        return STBOOL_SUCCESS;
    }
    // ... <function-signature-no-param-names>
    struct func_sig func_sig;
    func_sig_init(&func_sig);
    if (
        *next = save,
        prod_func_sig(&func_sig, NULL, next)
    )
    {
        dt->tag = DT_FUNCTION;
        dt->func_sig = nalloc(sizeof(typeof(*dt->func_sig)));
        *dt->func_sig = func_sig;
        return STBOOL_SUCCESS;
    }
    func_sig_fini(&func_sig);
    // ... <identifier>
    struct id id;
    id_init(&id);
    if (
        *next = save,
        prod_id(&id, next)
    )
    {
        dt->tag = DT_UNRESOLVED_ID;
        dt->unresolved.id = id;
        return STBOOL_SUCCESS;
    }
    id_fini(&id);
    // ... 'typeof' '(' <expr-unary> ')'
    struct expr typeof_expr;
    expr_init(&typeof_expr);
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_TYPEOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_expr_unary(&typeof_expr, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        dt->tag = DT_UNRESOLVED_TYPEOF_EXPR;
        dt->unresolved.typeof_expr = nalloc(sizeof(struct expr));
        *dt->unresolved.typeof_expr = typeof_expr;
        return STBOOL_SUCCESS;
    }
    expr_fini(&typeof_expr);
    // ... 'typeof' '(' <data-type> ')'
    struct dt typeof_dt;
    dt_init(&typeof_dt);
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_TYPEOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_dt(&typeof_dt, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        dt->tag = DT_UNRESOLVED_TYPEOF_DT;
        dt->unresolved.typeof_dt = nalloc(sizeof(struct dt));
        *dt->unresolved.typeof_dt = typeof_dt;
        return STBOOL_SUCCESS;
    }
    dt_fini(&typeof_dt);

    return STBOOL_FAILURE;
}

static stbool _proc_dt(struct dt* dt, struct processing_context* ctx)
{
    if (NULL != dt->inner && !_proc_dt(dt->inner, ctx))
    {
        return STBOOL_FAILURE;
    }

    // Before processing the data type we need to check if the data type was
    // parsed from an in-source identifier or a typeof expression.
    // In the case of a data type parsed from an in-source identifier, we need
    // to look up that identifier in the global scope, verify that it
    // corresponds to a data type, and then populate this data type with the
    // identified type.
    // In the case of a data type parsed from a typeof expression/dt, we need to
    // get the type of that expression.
    if (DT_UNRESOLVED_ID == dt->tag)
    {
        nassert(NULL == dt->inner);
        struct id const* id = &dt->unresolved.id;
        struct symbol* const symbol =
            symbol_table_find(ctx->module->global_scope.symbol_table, id->start, id->length);
        if (
            NULL == symbol
            || (
                SYMBOL_STRUCT != symbol->tag
                && SYMBOL_ALIAS != symbol->tag
            )
        )
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                dt->srctok,
                FMT_NO_DATA_TYPE_MATCHING_IDENTIFIER,
                (int)id->length,
                id->start
            );
            return STBOOL_FAILURE;
        }
        symbol_mark_use(symbol);
        nassert(NULL != symbol->dt);
        nassert(DT_UNSET != symbol->dt->tag);

        struct qualifiers const qualifiers_save = dt->qualifiers;
        struct token const* const srctok_save = dt->srctok;
        dt_assign(dt, symbol->dt, true);
        qualifers_union(&dt->qualifiers, &dt->qualifiers, &qualifiers_save);
        dt->srctok = srctok_save;
    }
    else if (DT_UNRESOLVED_TYPEOF_EXPR == dt->tag)
    {
        nassert(NULL == dt->inner);
        if (!proc_expr(dt->unresolved.typeof_expr, ctx))
        {
            return STBOOL_FAILURE;
        }

        struct qualifiers const qualifiers_save = dt->qualifiers;
        struct token const* const srctok_save = dt->srctok;
        // Use separate dt so the expression doesn't get finalized before it can
        // be used.
        struct dt _dt;
        dt_init(&_dt);
        dt_assign(&_dt, &dt->unresolved.typeof_expr->dt, true);
        dt_reset(dt);
        dt_assign(dt, &_dt, true);
        dt_fini(&_dt);
        qualifers_union(&dt->qualifiers, &dt->qualifiers, &qualifiers_save);
        dt->srctok = srctok_save;
    }
    else if (DT_UNRESOLVED_TYPEOF_DT == dt->tag)
    {
        nassert(NULL == dt->inner);
        if (!proc_dt(dt->unresolved.typeof_dt, ctx))
        {
            return STBOOL_FAILURE;
        }

        struct qualifiers const qualifiers_save = dt->qualifiers;
        struct token const* const srctok_save = dt->srctok;
        // Use separate dt so the expression doesn't get finalized before it can
        // be used.
        struct dt _dt;
        dt_init(&_dt);
        dt_assign(&_dt, dt->unresolved.typeof_dt, true);
        dt_reset(dt);
        dt_assign(dt, &_dt, true);
        dt_fini(&_dt);
        qualifers_union(&dt->qualifiers, &dt->qualifiers, &qualifiers_save);
        dt->srctok = srctok_save;
    }

    //// Now that the data type has been fully formed, process it.
    if (!proc_qualifiers(&dt->qualifiers, ctx))
    {
        return STBOOL_FAILURE;
    }
    switch (dt->tag)
    {
    case DT_VOID:   /* intentional fallthrough */
    case DT_U8:     /* intentional fallthrough */
    case DT_U16:    /* intentional fallthrough */
    case DT_U32:    /* intentional fallthrough */
    case DT_U64:    /* intentional fallthrough */
    case DT_U:      /* intentional fallthrough */
    case DT_S8:     /* intentional fallthrough */
    case DT_S16:    /* intentional fallthrough */
    case DT_S32:    /* intentional fallthrough */
    case DT_S64:    /* intentional fallthrough */
    case DT_S:      /* intentional fallthrough */
    case DT_F16:    /* intentional fallthrough */
    case DT_F32:    /* intentional fallthrough */
    case DT_F64:    /* intentional fallthrough */
    case DT_F128:   /* intentional fallthrough */
    case DT_BOOL:   /* intentional fallthrough */
    case DT_ASCII:  /* intentional fallthrough */
    case DT_STRUCT: /* intentional fallthrough */
        /* nothing */
        break;
    case DT_FUNCTION:
        proc_func_sig(dt->func_sig, ctx);
        break;
    case DT_POINTER:
        /* nothing */
        break;
    case DT_ARRAY:
        {
            stbool arr_status = STBOOL_SUCCESS;
            if (0 == dt->array.length)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    dt->srctok,
                    STR_ARRAY_MUST_NOT_HAVE_LENGTH_ZERO
                );
                arr_status = STBOOL_FAILURE;
            }
            if (!qualifiers_is_default(&dt->qualifiers))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    dt->srctok,
                    STR_ILLEGAL_USE_OF_QUALIFIERS_ON_ARRAY_TYPE
                );
                arr_status = STBOOL_FAILURE;
            }
            if (STBOOL_SUCCESS != arr_status)
            {
                return STBOOL_FAILURE;
            }
        }
        break;
    case DT_UNRESOLVED_ID: /* intentional fallthrough */
    case DT_UNRESOLVED_TYPEOF_EXPR:
    case DT_UNRESOLVED_TYPEOF_DT:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    case DT_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

stbool proc_dt(struct dt* dt, struct processing_context* ctx)
{
    if (!_proc_dt(dt, ctx))
    {
        return STBOOL_FAILURE;
    }

    // If the data type is composite it may be the case that the type has
    // been declared, but not defined.
    // If so, the data type cannot be used if knowledge of the data type's
    // size is required.
    bool composite_requires_sizeof_knowledge = true;
    struct dt const* search_dt = dt;
    while (NULL != search_dt)
    {
        if (
            dt_is_primitive(search_dt)
            || dt_is_pointer(search_dt)
            || dt_is_function(search_dt)
            || (dt_is_struct(search_dt) && (NULL != search_dt->struct_ref->def))
        )
        {
            composite_requires_sizeof_knowledge = false;
            break;
        }
        search_dt = search_dt->inner;
    }
    if (composite_requires_sizeof_knowledge)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            dt->srctok,
            STR_USE_OF_COMPOSITE_TYPE_REQUIRES_SIZEOF_KNOWLEDGE
        );
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//====================================//
//      STRUCT DEFINITION             //
//====================================//
int prod_struct_def(
    struct struct_def* def,
    struct token** next
)
{
    struct token* save = *next;
    def->srctok = save;

    // '{' { [ <member-var> | <member-func> ] } '}'
    if (!match_terminal(next, TOKEN_OPERATOR_LEFTBRACE))
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;

    LOOP_FOREVER
    {
        // <member-var>
        {
            struct member_var member_var;
            member_var_init(&member_var);
            if (
                *next = save,
                !prod_member_var(&member_var, next)
            )
            {
                member_var_fini(&member_var);
                goto try_prod_member_func;
            }
            save = *next;
            def->member_vars = narr_push(def->member_vars, &member_var);
            continue;
        }

try_prod_member_func:
        // <member-func>
        {
            struct member_func member_func;
            member_func_init(&member_func);
            if (
                *next = save,
                !prod_member_func(&member_func, next)
            )
            {
                member_func_fini(&member_func);
                break;
            }
            save = *next;
            def->member_funcs = narr_push(def->member_funcs, &member_func);
            continue;
        }

    }
    *next = save;

    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_RIGHTBRACE),
        save,
        STR_EXPECTED(STR_CURLY_BRACKET_R),
        return PROD_FAILURE_SOFT;
    );

    return PROD_SUCCESS;
}

stbool proc_struct_def(
    struct struct_def* def,
    struct processing_context* ctx
)
{
    SUPPRESS_UNUSED(ctx);

    size_t const member_vars_len = narr_length(def->member_vars);
    size_t const member_funcs_len = narr_length(def->member_funcs);

    if (0 == member_vars_len)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            def->srctok,
            STR_STRUCT_HAS_NO_MEMBER_VARIABLES
        );
        return STBOOL_FAILURE;
    }

    // Make sure that no member variables/functions have the same identifier.
    // Each member is compared against all other members that it has not yet
    // been compared against.
    for (size_t i = 0; i < member_vars_len; ++i)
    {
        // Compare against member variables.
        for (size_t j = i+1; j < member_vars_len; ++j)
        {
            if (id_eq(&def->member_vars[i].id, &def->member_vars[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    def->member_vars[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(def->member_vars[j].id.length),
                    def->member_vars[j].id.start
                );
                return STBOOL_FAILURE;
            }
        }

        // Compare against member functions.
        for (size_t j = 0; j < member_funcs_len; ++j)
        {
            if (id_eq(&def->member_vars[i].id, &def->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    def->member_vars[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(def->member_vars[j].id.length),
                    def->member_vars[j].id.start
                );
                return STBOOL_FAILURE;
            }
        }
    }
    for (size_t i = 0; i < member_funcs_len; ++i)
    {
        // Compare against member functions.
        for (size_t j = i+1; j < member_funcs_len; ++j)
        {
            if (id_eq(&def->member_funcs[i].id, &def->member_funcs[j].id))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    def->member_funcs[j].srctok,
                    FMT_DUPLICATE_STRUCT_ID,
                    (int)(def->member_funcs[j].id.length),
                    def->member_funcs[j].id.start
                );
                return STBOOL_FAILURE;
            }
        }
    }

    // Process all member variables.
    for (size_t i = 0; i < member_vars_len; ++i)
    {
        if (!proc_member_var(&def->member_vars[i], ctx))
        {
            return STBOOL_FAILURE;
        }
    }
    // Process all member functions.
    for (size_t i = 0; i < member_funcs_len; ++i)
    {
        if (!proc_member_func(&def->member_funcs[i], ctx))
        {
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

//====================================//
//      FUNCTION SIGNATURE            //
//====================================//
stbool prod_func_sig(
    struct func_sig* func_sig,
    narr_t(struct id)* param_ids,
    struct token** next
)
{
    bool is_parse_param_ids = NULL != param_ids;
    struct token* save = *next;

    // '('
    if (
        *next = save,
        !match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
    )
    {
        return STBOOL_FAILURE;
    }
    save = *next;

    // function parameters
    nassert(narr_length(func_sig->param_dts) == 0);
    LOOP_FOREVER
    {
        struct id id;
        if (is_parse_param_ids)
        {
            id_init(&id);
        }
        struct dt dt;
        dt_init(&dt);

        // <data-type>
        // <identifier> ':' <data-type>
        if (
            *next = save,
            (!is_parse_param_ids || prod_id(&id, next))
            && (!is_parse_param_ids || match_terminal(next, TOKEN_OPERATOR_COLON))
            && prod_dt(&dt, next)
        )
        {
            if (is_parse_param_ids)
            {
                *param_ids = narr_push(*param_ids, &id);
            }
            func_sig->param_dts = narr_push(func_sig->param_dts, &dt);

            // ',' indicates further parameters
            save = *next;
            if (match_terminal(next, TOKEN_OPERATOR_COMMA))
            {
                save = *next;
                continue;
            }
            break;
        }
        else
        {
            if (is_parse_param_ids)
            {
                id_fini(&id);
            }
            dt_fini(&dt);
            break;
        }
    }

    // ')' '->' <data-type>
    if (
        *next = save,
        !(
            match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
            && match_terminal(next, TOKEN_OPERATOR_DASH_GREATERTHAN)
            && prod_dt(&func_sig->rtn_dt, next)
        )
    )
    {
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

stbool proc_func_sig(
    struct func_sig* func_sig,
    struct processing_context* ctx
)
{
    if (!proc_dt(&func_sig->rtn_dt, ctx))
    {
        return STBOOL_FAILURE;
    }

    for (size_t i = 0; i < narr_length(func_sig->param_dts); ++i)
    {
        if (!proc_dt(&func_sig->param_dts[i], ctx))
        {
            return STBOOL_FAILURE;
        }
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
stbool prod_literal(struct literal* literal, struct token** next)
{
    struct token* save = *next;
    literal->srctok = save;

    // <literal-u8>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_U8)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U8 == val.tag);

        literal->u8 = val.u8;
        literal->tag = LITERAL_U8;
        return STBOOL_SUCCESS;
    }

    // <literal-u16>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_U16)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U16 == val.tag);

        literal->u16 = val.u16;
        literal->tag = LITERAL_U16;
        return STBOOL_SUCCESS;
    }

    // <literal-u32>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_U32)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U32 == val.tag);

        literal->u32 = val.u32;
        literal->tag = LITERAL_U32;
        return STBOOL_SUCCESS;
    }

    // <literal-u64>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_U64)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U64 == val.tag);

        literal->u64 = val.u64;
        literal->tag = LITERAL_U64;
        return STBOOL_SUCCESS;
    }

    // <literal-u>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_U)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_U == val.tag);

        literal->u = val.u;
        literal->tag = LITERAL_U;
        return STBOOL_SUCCESS;
    }

    // <literal-s8>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_S8)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_S8 == val.tag);

        literal->s8 = val.s8;
        literal->tag = LITERAL_S8;
        return STBOOL_SUCCESS;
    }

    // <literal-s16>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_S16)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_S16 == val.tag);

        literal->s16 = val.s16;
        literal->tag = LITERAL_S16;
        return STBOOL_SUCCESS;
    }

    // <literal-s32>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_S32)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_S32 == val.tag);

        literal->s32 = val.s32;
        literal->tag = LITERAL_S32;
        return STBOOL_SUCCESS;
    }

    // <literal-s64>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_S64)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_S64 == val.tag);

        literal->s64 = val.s64;
        literal->tag = LITERAL_S64;
        return STBOOL_SUCCESS;
    }

    // <literal-s>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_S)
    )
    {
        struct n_integer_value val;
        ssize_t const parse_result = parse_n_integer_literal(save->start, &val);
        nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
        nassert(N_INTEGER_S == val.tag);

        literal->s = val.s;
        literal->tag = LITERAL_S;
        return STBOOL_SUCCESS;
    }

    // <literal-f32>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_F32)
    )
    {
        nassert(TOKEN_LITERAL_F32 == ((*next)-1)->tag);
        literal->f32.start = ((*next)-1)->start;
        literal->f32.len   = ((*next)-1)->length;
        literal->tag = LITERAL_F32;
        return STBOOL_SUCCESS;
    }

    // <literal-f64>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_F64)
    )
    {
        nassert(TOKEN_LITERAL_F64 == ((*next)-1)->tag);
        literal->f64.start = ((*next)-1)->start;
        literal->f64.len   = ((*next)-1)->length;
        literal->tag = LITERAL_F64;
        return STBOOL_SUCCESS;
    }

    // <literal-bool>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_TRUE)
    )
    {
        literal->boolean = true;
        literal->tag = LITERAL_BOOL;
        return STBOOL_SUCCESS;
    }

    // <literal-bool>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_FALSE)
    )
    {
        literal->boolean = false;
        literal->tag = LITERAL_BOOL;
        return STBOOL_SUCCESS;
    }

    // <literal-char>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_ASCII)
    )
    {
        n_ascii nch;
        parse_n_ascii_literal(save->start, &nch);

        literal->character = nch;
        literal->tag = LITERAL_CHAR;
        return STBOOL_SUCCESS;
    }

    // <literal-string>
    if (
        *next = save,
        match_terminal(next, TOKEN_LITERAL_STRING)
    )
    {
        nstr_t nstr_defer_fini s;
        nstr_init(&s);

        parse_n_astr_literal(save->start, &s);

        nstr_assign_nstr(&literal->str, &s);
        literal->tag = LITERAL_STR;
        return STBOOL_SUCCESS;
    }

    // <literal-null>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_NULL)
    )
    {
        literal->tag = LITERAL_NULL;
        return STBOOL_SUCCESS;
    }

    // <literal-array>
    {
        // '[' ...
        if (
            *next = save,
            !match_terminal(next, TOKEN_OPERATOR_LEFTBRACKET)
        )
        {
            goto literal_array_failure;
        }
        save = *next;

        // ... { <literal> ',' } <literal> [ ',' '..' <literal-u> ] ...
        narr_t(struct literal) array = narr_alloc(0, sizeof(struct literal));
        LOOP_FOREVER
        {
            struct literal sub_literal;
            literal_init(&sub_literal);

            if (
                *next = save,
                match_terminal(next, TOKEN_OPERATOR_DOT_DOT)
            )
            {
                save = *next;
                MATCH_EXPECTED(
                    match_terminal(next, TOKEN_LITERAL_U),
                    save,
                    STR_EXPECTED(STR_LITERAL_U),
                    {
                        literal_fini(&sub_literal);
                        goto literal_array_failure_fini_array;
                    }
                );
                struct n_integer_value val;
                ssize_t const parse_result =
                    parse_n_integer_literal(save->start, &val);
                nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);
                nassert(N_INTEGER_U == val.tag);

                save = *next;
                sub_literal.tag = LITERAL_ARRAY_FILL;
                sub_literal.array_fill_len = (size_t)val.u;
                sub_literal.srctok = save;
                array = narr_push(array, &sub_literal);
                goto literal_array_parse_closing_bracket;
            }
            else if (
                *next = save,
                prod_literal(&sub_literal, next)
            )
            {
                save = *next;
                array = narr_push(array, &sub_literal);
                if (
                    *next = save,
                    match_terminal(next, TOKEN_OPERATOR_COMMA)
                )
                {
                    // There are more literals to parse.
                    save = *next;
                    continue;
                }
                // Out of literals to parse.
                break;
            }
            literal_fini(&sub_literal);
            goto literal_array_failure_fini_array;
        }

literal_array_parse_closing_bracket:
        // ... ']'
        *next = save;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_RIGHTBRACKET),
            save,
            STR_EXPECTED(STR_SQUARE_BRACKET_R),
            goto literal_array_failure_fini_array;
        );
        literal->tag = LITERAL_ARRAY;
        literal->array = array;
        return STBOOL_SUCCESS;

literal_array_failure_fini_array:
        for (size_t i = 0; i < narr_length(array); ++i)
        {
            literal_fini(&array[i]);
        }
        narr_free(array);
    }

literal_array_failure:
    return STBOOL_FAILURE;
}

stbool proc_literal(struct literal* literal, struct processing_context* ctx)
{
    SUPPRESS_UNUSED(ctx);

    if (LITERAL_ARRAY == literal->tag)
    {
        if (narr_length(literal->array) == 0)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                literal->srctok,
                STR_ARRAY_MUST_NOT_HAVE_LENGTH_ZERO
            );
            return STBOOL_FAILURE;
        }
        if (LITERAL_ARRAY_FILL == literal->array[0].tag)
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                literal->srctok,
                STR_ARRAY_LITERAL_REQUIRES_AT_LEAST_ONE_NON_FILL_ELEMENT
            );
            return STBOOL_FAILURE;
        }

        struct dt ATTR_DEFER_FINI(dt_fini) first_dt =
            literal_get_dt(&literal->array[0]);

        for (size_t i = 1; i < narr_length(literal->array); ++i)
        {
            if (LITERAL_ARRAY_FILL == literal->array[i].tag)
            {
                nassert(i == narr_length(literal->array)-1);
                if (i >= literal->array[i].array_fill_len)
                {
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        literal->array[i].srctok,
                        STR_INVALID_ARRAY_LITERAL_FILL_LENGTH
                    );
                    return STBOOL_FAILURE;
                }
                break;
            }
            struct dt ATTR_DEFER_FINI(dt_fini) other_dt =
                literal_get_dt(&literal->array[i]);
            if (!dt_can_implicitly_convert(&other_dt, &first_dt))
            {
                DECLARE_DT_NSTR(first_dt_nstr, &first_dt);
                DECLARE_DT_NSTR(other_dt_nstr, &first_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    literal->array[i].srctok,
                    FMT_MISMATCHED_DATA_TYPES_IN_ARRAY_LITERAL,
                    first_dt_nstr.data,
                    other_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }
        }
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//
stbool prod_expr(struct expr* expr, struct token** next)
{
    return prod_expr_binary_assignment(expr, next);
}

stbool prod_expr_binary_assignment(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_logical_or(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-logical-or> <expr-binary-assignment'>
    if (
        *next = save,
        prod_expr_binary_assignment_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-logical-or>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_assignment_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_EQUAL)
        && prod_expr_binary_assignment(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_ASSIGN;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_logical_or(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_logical_and(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-logical-and> <expr-binary-logical-or'>
    if (
        *next = save,
        prod_expr_binary_logical_or_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-logical-and>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_logical_or_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_PIPE_PIPE)
        && prod_expr_binary_logical_or(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_LOGICAL_OR;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_logical_and(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_bitwise_or(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-bitwise-or> <expr-binary-logical-and'>
    if (
        *next = save,
        prod_expr_binary_logical_and_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-bitwise-or>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_logical_and_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_AMPERSAND_AMPERSAND)
        && prod_expr_binary_logical_and(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_LOGICAL_AND;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_bitwise_or(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_bitwise_xor(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-bitwise-xor> <expr-binary-bitwise-or'>
    if (
        *next = save,
        prod_expr_binary_bitwise_or_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *(expr->binary.lhs_expr) = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-bitwise-xor>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_bitwise_or_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_PIPE)
        && prod_expr_binary_bitwise_or(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_BITWISE_OR;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_bitwise_xor(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_bitwise_and(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-bitwise-and> <expr-binary-bitwise-xor'>
    if (
        *next = save,
        prod_expr_binary_bitwise_xor_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-bitwise-and>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_bitwise_xor_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_CARET)
        && prod_expr_binary_bitwise_xor(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_BITWISE_XOR;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_bitwise_and(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_relational_exact(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-relational-exact> <expr-binary-bitwise-and'>
    if (
        *next = save,
        prod_expr_binary_bitwise_and_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-relational-exact>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_bitwise_and_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_AMPERSAND)
        && prod_expr_binary_bitwise_and(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_BITWISE_AND;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_relational_exact(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_relational_inexact(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-relational-inexact> <expr-binary-relational-exact'>
    if (
        *next = save,
        prod_expr_binary_relational_exact_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-relational-inexact>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_relational_exact_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_EQUAL_EQUAL)
        && prod_expr_binary_relational_exact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_EQ;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_BANG_EQUAL)
        && prod_expr_binary_relational_exact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_NE;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_relational_inexact(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_shift(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-shift> <expr-binary-relational-inexact'>
    if (
        *next = save,
        prod_expr_binary_relational_inexact_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-shift>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_relational_inexact_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_LESSTHAN)
        && prod_expr_binary_relational_inexact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_LT;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_GREATERTHAN)
        && prod_expr_binary_relational_inexact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_GT;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_LESSTHAN_EQUAL)
        && prod_expr_binary_relational_inexact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_LE;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_GREATERTHAN_EQUAL)
        && prod_expr_binary_relational_inexact(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_REL_GE;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_shift(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_additive(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-additive> <expr-binary-shift'>
    if (
        *next = save,
        prod_expr_binary_shift_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-additive>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_shift_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_LESSTHAN_LESSTHAN)
        && prod_expr_binary_shift(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_SHIFT_L;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_GREATERTHAN_GREATERTHAN)
        && prod_expr_binary_shift(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_SHIFT_R;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_additive(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_multiplicative(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-multiplicative> <expr-binary-additive'>
    if (
        *next = save,
        prod_expr_binary_additive_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-multiplicative>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_additive_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_PLUS)
        && prod_expr_binary_additive(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_PLUS;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_DASH)
        && prod_expr_binary_additive(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_MINUS;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_DASH_CARET_CARET)
        && prod_expr_binary_additive(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_PTR_DIFF;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_PLUS_CARET)
        && prod_expr_binary_additive(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_PTR_PLUS;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_DASH_CARET)
        && prod_expr_binary_additive(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_PTR_MINUS;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_multiplicative(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_binary_cast(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-binary-cast> <expr-binary-multiplicative'>
    if (
        *next = save,
        prod_expr_binary_multiplicative_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-binary-cast>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_multiplicative_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_ASTERISK)
        && prod_expr_binary_multiplicative(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_MULT;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_SLASH)
        && prod_expr_binary_multiplicative(&rhs, next)
    )
    {
        bexpr->tag = EXPR_BINARY_DIV;
        bexpr->binary.rhs_expr = nalloc(sizeof(struct expr));
        *(bexpr->binary.rhs_expr) = rhs;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);
    return STBOOL_FAILURE;
}

stbool prod_expr_binary_cast(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    struct expr lhs;
    expr_init(&lhs);
    if (
        *next = save,
        !prod_expr_unary(&lhs, next)
    )
    {
        expr_fini(&lhs);
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-function-unary> <expr-binary-cast'>
    if (
        *next = save,
        prod_expr_binary_cast_p(expr, next)
    )
    {
        expr->binary.lhs_expr = nalloc(sizeof(struct expr));
        *expr->binary.lhs_expr = lhs;
        return STBOOL_SUCCESS;
    }

    // <expr-function-unary>
    expr_assign(expr, &lhs, false);
    expr_fini(&lhs);
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_binary_cast_p(
    struct expr* bexpr,
    struct token** next
)
{
    struct token* save = *next;

    struct dt rhs_dt;
    dt_init(&rhs_dt);
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_AS)
        && prod_dt(&rhs_dt, next)
    )
    {
        bexpr->tag = EXPR_BINARY_CAST;
        bexpr->binary.rhs_dt = nalloc(sizeof(struct dt));
        *bexpr->binary.rhs_dt = rhs_dt;
        bexpr->srctok = save;
        return STBOOL_SUCCESS;
    }
    dt_fini(&rhs_dt);

    return STBOOL_FAILURE;
}

stbool prod_expr_unary(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;
    struct expr rhs;
    expr_init(&rhs);

    // '?' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_QUESTION)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_ADDR_OF;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '@' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_AT)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_DEREF;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '$' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_DOLLAR)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_DECAY;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '+' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_PLUS)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_PLUS;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '-' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_DASH)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_MINUS;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '~' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_TILDE)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_BITWISE_NOT;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // '!' <expr-unary>
    if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_BANG)
        && prod_expr_unary(&rhs, next)
    )
    {
        expr->tag = EXPR_UNARY_LOGICAL_NOT;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // 'countof' '(' <expr-unary> ')'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_COUNTOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_expr_unary(&rhs, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        expr->tag = EXPR_UNARY_COUNTOF_EXPR;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // 'sizeof' '(' <expr-unary> ')'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_SIZEOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_expr_unary(&rhs, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        expr->tag = EXPR_UNARY_SIZEOF_EXPR;
        expr->unary.rhs_expr = nalloc(sizeof(struct expr));
        *expr->unary.rhs_expr = rhs;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    expr_fini(&rhs);

    struct dt rhs_dt;
    dt_init(&rhs_dt);

    // 'countof' '(' <data-type> ')'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_COUNTOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_dt(&rhs_dt, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        expr->tag = EXPR_UNARY_COUNTOF_DT;
        expr->unary.rhs_dt = nalloc(sizeof(struct dt));
        *expr->unary.rhs_dt = rhs_dt;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    // 'sizeof' '(' <data-type> ')'
    if (
        *next = save,

        match_terminal(next, TOKEN_KEYWORD_SIZEOF)
        && match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        && prod_dt(&rhs_dt, next)
        && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        expr->tag = EXPR_UNARY_SIZEOF_DT;
        expr->unary.rhs_dt = nalloc(sizeof(struct expr));
        *expr->unary.rhs_dt = rhs_dt;
        expr->srctok = save;
        return STBOOL_SUCCESS;
    }

    dt_fini(&rhs_dt);

    // <expr-postfix>
    if (
        *next = save,
        prod_expr_postfix(expr, next)
    )
    {
        return STBOOL_SUCCESS;
    }

    return STBOOL_FAILURE;
}

stbool prod_expr_postfix(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    if (
        *next = save,
        !prod_expr_primary(expr, next)
    )
    {
        return STBOOL_FAILURE;
    }
    save = *next;

    // <expr-primary> <expr-postfix'>
    if (
        *next = save,
        prod_expr_postfix_p(expr, next)
    )
    {
        return STBOOL_SUCCESS;
    }

    // <expr-primary>
    // `expr` should already be assigned from the initial call to
    // prod_expr_primary.
    *next = save;
    return STBOOL_SUCCESS;
}

stbool prod_expr_postfix_p(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;
    struct token* srctok_save = save;

    struct expr postfixy_expr;
    expr_init(&postfixy_expr);

    // <expr-postfix'> '(' [ { <expr> ',' } <expr> ] ')'
    {
        // ... '(' ...
        if (
            *next = save,
            !match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
        )
        {
            goto prod_function_call_failure;
        }
        save = *next;

        // ... [ { <expr> ',' } <expr> ] ...
        narr_t(struct expr) args = narr_alloc(0, sizeof(struct expr));
        nassert(narr_length(args) == 0);

        LOOP_FOREVER
        {
            struct expr arg;
            expr_init(&arg);

            if (
                *next = save,
                prod_expr(&arg, next)
            )
            {
                args = narr_push(args, &arg);
                save = *next;

                // ',' indicates further arguments.
                if (match_terminal(next, TOKEN_OPERATOR_COMMA))
                {
                    save = *next;
                    continue;
                }
                break;
            }
            else
            {
                expr_fini(&arg);
                break;
            }
        }

        // ... ')'
        if (
            *next = save,
            !match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
        )
        {
            goto prod_function_call_failure__cleanup_args;
        }
        save = *next;

        postfixy_expr.tag = EXPR_POSTFIX_FUNCTION_CALL;
        postfixy_expr.postfix.args = args;
        goto prod_expr_postfix_success;

prod_function_call_failure__cleanup_args:
        for (size_t i = 0; i < narr_length(args); ++i)
        {
            expr_fini(&args[i]);
        }
        narr_free(args);
prod_function_call_failure:;
    }

    // <expr-postfix'> '[' <expr> ']'
    {
        struct expr subscript;
        expr_init(&subscript);
        if (
            *next = save,
            match_terminal(next, TOKEN_OPERATOR_LEFTBRACKET)
            && prod_expr(&subscript, next)
            && match_terminal(next, TOKEN_OPERATOR_RIGHTBRACKET)
        )
        {
            postfixy_expr.tag = EXPR_POSTFIX_SUBSCRIPT;
            postfixy_expr.postfix.subscript = nalloc(sizeof(struct expr));
            *postfixy_expr.postfix.subscript = subscript;
            goto prod_expr_postfix_success;
        }
        expr_fini(&subscript);
    }

    // <expr-postfix'> '.' <identifier>
    {
        if (
            *next = save,
            !match_terminal(next, TOKEN_OPERATOR_DOT)
        )
        {
            goto prod_expr_postfix_dot_failure__soft;
        }
        save = *next;

        struct id id;
        id_init(&id);
        MATCH_EXPECTED(
            prod_id(&id, next),
            save,
            STR_EXPECTED(STR_ID),
            {
                id_fini(&id);
                goto prod_expr_postfix_failure;
            }
        );
        save = *next;

        postfixy_expr.tag = EXPR_POSTFIX_ACCESS_DOT;
        postfixy_expr.postfix.access.id = id;
        goto prod_expr_postfix_success;

prod_expr_postfix_dot_failure__soft:;
    }

    // <expr-postfix'> '->' <identifier>
    {
        if (
            *next = save,
            !match_terminal(next, TOKEN_OPERATOR_DASH_GREATERTHAN)
        )
        {
            goto prod_expr_postfix_arrow_failure__soft;
        }
        save = *next;

        struct id id;
        id_init(&id);
        MATCH_EXPECTED(
            prod_id(&id, next),
            save,
            STR_EXPECTED(STR_ID),
            {
                id_fini(&id);
                goto prod_expr_postfix_failure;
            }
        );
        save = *next;

        postfixy_expr.tag = EXPR_POSTFIX_ACCESS_ARROW;
        postfixy_expr.postfix.access.id = id;
        goto prod_expr_postfix_success;

prod_expr_postfix_arrow_failure__soft:;
    }

prod_expr_postfix_failure:
    expr_fini(&postfixy_expr);
    return STBOOL_FAILURE;

prod_expr_postfix_success:
    postfixy_expr.postfix.lhs = nalloc(sizeof(struct expr));
    *postfixy_expr.postfix.lhs = *expr;
    *expr = postfixy_expr;
    expr->srctok = srctok_save;

    // As long as one postfix expression is parsed the entire expression is
    // a postfix expression. We recurse on prod_expr_postfix_p until we run
    // out of postfix expressions. When this occurs, it's not an error, it's
    // just where the entire postfix expression ends.
    save = *next;
    if (
        *next = save,
        !prod_expr_postfix_p(expr, next)
    )
    {
        *next = save;
    }

    return STBOOL_SUCCESS;
}

stbool prod_expr_primary(
    struct expr* expr,
    struct token** next
)
{
    struct token* save = *next;

    // <identifier>
    {
        id_init(&expr->id);
        if (
            *next = save,
            prod_id(&expr->id, next)
        )
        {
            expr->tag = EXPR_PRIMARY_IDENTIFIER;
            expr->srctok = save;
            return STBOOL_SUCCESS;
        }
        id_fini(&expr->id);
    }

    // <literal>
    {
        struct literal literal;
        literal_init(&literal);
        if (
            *next = save,
            prod_literal(&literal, next)
        )
        {
            expr->tag = EXPR_PRIMARY_LITERAL;
            expr->literal = nalloc(sizeof(struct literal));
            *expr->literal = literal;
            expr->srctok = save;
            return STBOOL_SUCCESS;
        }
        literal_fini(&literal);
    }

    // <expr-primary-paren>
    {
        struct expr expr_paren;
        expr_init(&expr_paren);
        if (
            *next = save,
            match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
            && prod_expr(&expr_paren, next)
            && match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
        )
        {
            expr->tag = EXPR_PRIMARY_PAREN;
            expr->paren = nalloc(sizeof(struct expr));
            *expr->paren = expr_paren;
            expr->srctok = save;
            return STBOOL_SUCCESS;
        }
        expr_fini(&expr_paren);
    }

    return STBOOL_FAILURE;
}

stbool check_xhs_are_base(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (!dt_is_base(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE " " STR_EXPECTED(STR_BASE),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (!dt_is_base(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE " " STR_EXPECTED(STR_BASE),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_ptr(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (!dt_is_pointer(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE " " STR_EXPECTED(STR_POINTER),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (!dt_is_pointer(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE " " STR_EXPECTED(STR_POINTER),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_not_void(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (dt_is_void(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_VOID),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (dt_is_void(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_VOID),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_not_integer(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (dt_is_integer(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_INTEGER),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (dt_is_integer(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_INTEGER),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_not_floating_point(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (dt_is_float(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_FLOATING_POINT),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (dt_is_float(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_FLOATING_POINT),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_not_bool(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (dt_is_bool(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_BOOL),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (dt_is_bool(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_BOOL),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_xhs_are_not_char(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (dt_is_char(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_CHAR),
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (dt_is_char(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                " " STR_EXPECTED_X_TYPE(STR_PREFIX_NON STR_CHAR),
            rhs_dt_nstr.data,
            STR_RIGHT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool check_assignment_value_categories(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;

    if (!expr_is_lvalue(bexpr->binary.lhs_expr))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ASSIGNMENT_LHS_NOT_LVALUE
        );
        status = STBOOL_FAILURE;
    }
    if (!expr_is_rvalue(bexpr->binary.rhs_expr))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ASSIGNMENT_RHS_NOT_RVALUE
        );
        status = STBOOL_FAILURE;
    }

    return status;
}

stbool check_assignment_mutability_constraints(struct expr const* bexpr)
{
    if (!bexpr->binary.lhs_expr->dt.qualifiers.is_mut)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ASSIGNMENT_LHS_NOT_MUT
        );
        return STBOOL_FAILURE;
    }
    return STBOOL_SUCCESS;
}

stbool check_assignment_implicit_conversion(struct expr const* bexpr)
{
    if (!dt_can_implicitly_convert(&bexpr->binary.rhs_expr->dt, &bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS,
            rhs_dt_nstr.data,
            lhs_dt_nstr.data
        );
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

stbool check_bit_shift_compatability(struct expr const* bexpr)
{
    return dt_is_integer(&bexpr->binary.lhs_expr->dt)
        && dt_is_integer_unsigned(&bexpr->binary.rhs_expr->dt);
}

stbool check_type_equal_arithmetic_compatability(struct expr const* bexpr)
{
    if (!check_xhs_are_base(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_bool(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_char(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_MISMATCHED_DATA_TYPES,
            lhs_dt_nstr.data,
            rhs_dt_nstr.data
        );

        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

stbool check_type_equal_bitwise_compatability(struct expr const* bexpr)
{
    if (!check_xhs_are_base(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_floating_point(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_bool(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (!check_xhs_are_not_char(bexpr))
    {
        return STBOOL_FAILURE;
    }


    if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
    {
        nassert(
            dt_is_integer(&bexpr->binary.lhs_expr->dt)
            &&
            dt_is_integer(&bexpr->binary.rhs_expr->dt)
        );

        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_MISMATCHED_DATA_TYPES,
            lhs_dt_nstr.data,
            rhs_dt_nstr.data
        );

        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

stbool check_type_equal_relational_compatability(struct expr const* bexpr)
{
    if (!check_xhs_are_not_void(bexpr))
    {
        return STBOOL_FAILURE;
    }

    if (
        dt_is_base(&bexpr->binary.lhs_expr->dt)
        ||
        dt_is_base(&bexpr->binary.rhs_expr->dt)
    )
    {
        if (!check_xhs_are_base(bexpr))
        {
            return STBOOL_FAILURE;
        }

        if (bexpr->binary.rhs_expr->dt.tag != bexpr->binary.lhs_expr->dt.tag)
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
            DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                bexpr->srctok,
                FMT_BOP_MISMATCHED_DATA_TYPES,
                lhs_dt_nstr.data,
                rhs_dt_nstr.data
            );

            return STBOOL_FAILURE;
        }
    }
    else if (
        dt_is_pointer(&bexpr->binary.lhs_expr->dt)
        ||
        dt_is_pointer(&bexpr->binary.rhs_expr->dt)
    )
    {
        if (!check_xhs_are_ptr(bexpr))
        {
            return STBOOL_FAILURE;
        }

        // TODO: This is sort of the idea we're going for but it may be too
        // performance costly. Investigate whether we should be using two calls
        // to this function.
        if (!(dt_can_implicitly_convert(&bexpr->binary.lhs_expr->dt, &bexpr->binary.rhs_expr->dt)
            || dt_can_implicitly_convert(&bexpr->binary.rhs_expr->dt, &bexpr->binary.lhs_expr->dt))
        )
        {
            DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
            DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
            NLOGF_FLC_TOK(
                LOG_ERROR,
                bexpr->srctok,
                FMT_BOP_MISMATCHED_DATA_TYPES,
                lhs_dt_nstr.data,
                rhs_dt_nstr.data
            );
            return STBOOL_FAILURE;
        }
    }
    else if (dt_is_array(&bexpr->binary.lhs_expr->dt))
    {
        DECLARE_DT_NSTR(lhs_dt_nstr, &bexpr->binary.lhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE,
            lhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        return STBOOL_FAILURE;
    }
    else if (dt_is_array(&bexpr->binary.rhs_expr->dt))
    {
        DECLARE_DT_NSTR(rhs_dt_nstr, &bexpr->binary.rhs_expr->dt);
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE,
            rhs_dt_nstr.data,
            STR_LEFT_LOWER,
            token_type_to_cstr(bexpr->srctok->tag)
        );
        return STBOOL_FAILURE;
    }
    else
    {
        NPANIC_UNEXPECTED_CTRL_FLOW();
    }

    return STBOOL_SUCCESS;
}

stbool check_type_equal_logical_compatability(struct expr const* bexpr)
{
    stbool status = STBOOL_SUCCESS;
    if (!dt_is_bool(&bexpr->binary.lhs_expr->dt))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_LOGICAL_XHS_MUST_BE_BOOL,
            "Left"
        );
        status = STBOOL_FAILURE;
    }
    if (!dt_is_bool(&bexpr->binary.rhs_expr->dt))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            bexpr->srctok,
            FMT_BOP_LOGICAL_XHS_MUST_BE_BOOL,
            "Right"
        );
        status = STBOOL_FAILURE;
    }
    if (STBOOL_SUCCESS != status)
    {
        return status;
    }

    return STBOOL_SUCCESS;
}

stbool proc_expr_primary(
    struct expr* expr,
    struct processing_context* ctx
)
{
    nassert(expr_is_primary(expr));

    switch (expr->tag)
    {
    case EXPR_PRIMARY_IDENTIFIER:
        {
            if (!proc_id(&expr->id, ctx))
            {
                return STBOOL_FAILURE;
            }

            // Lookup the identifier in the current scopes symbol table.
            struct scope* scope = ctx->curr_scope;
            struct symbol* symbol = NULL;
            do
            {
                struct id* id = &expr->id;
                symbol = symbol_table_find(scope->symbol_table, id->start, id->length);
                scope = (struct scope*)scope->parent;
            } while (NULL == symbol && scope != GLOBAL_SCOPE_PARENT_PTR);
            if (NULL == symbol)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_USE_OF_UNDECLARED_IDENTIFIER,
                    (int)expr->id.length,
                    expr->id.start
                );
                return STBOOL_FAILURE;
            }

            // The identifier exists in the symbol table. Mark the symbol as
            // used to indicate that the symbol has a purpose.
            symbol_mark_use(symbol);

            // Assign the datatype of the expression.
            switch (symbol->tag)
            {
            case SYMBOL_VARIABLE:
                dt_assign(&expr->dt, symbol->dt, true);
                expr->valcat = VALCAT_LVALUE;
                break;
            case SYMBOL_FUNCTION:
                dt_assign(&expr->dt, symbol->dt, true);
                expr->valcat = VALCAT_RVALUE;
                break;
            case SYMBOL_STRUCT:
                dt_assign(&expr->dt, symbol->dt, true);
                expr->valcat = VALCAT_NONE;
                break;
            case SYMBOL_ALIAS:
                /* alias identifier cannot be used for an expression */
                return STBOOL_FAILURE;
            default:
                NPANIC_DFLT_CASE();
            }

            // TODO: Change some identifiers to be constexpr based on whether
            // the identifier refers to a constexpr or not. Need to overhaul
            // symbol for this.
            expr->is_constexpr = true;
        }
        break;

    case EXPR_PRIMARY_LITERAL:
        {
            if (!proc_literal(expr->literal, ctx))
            {
                return STBOOL_FAILURE;
            }
            struct dt ATTR_DEFER_FINI(dt_fini) literal_dt =
                literal_get_dt(expr->literal);
            dt_assign(&expr->dt, &literal_dt, false);
            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = true;
        }
        break;

    case EXPR_PRIMARY_PAREN:
        {
            if (!proc_expr(expr->paren, ctx))
            {
                return STBOOL_FAILURE;
            }

            if (scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_XEXPR_NOT_ALLOWED_AT_GLOBAL_SCOPE,
                    STR_PARENTHESES_CAPITAL
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, &expr->paren->dt, true);
            expr->valcat = expr->paren->valcat;
            expr->is_constexpr = expr->paren->is_constexpr;
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    nassert(DT_UNSET != expr->dt.tag);
    return STBOOL_SUCCESS;
}

stbool proc_expr_postfix(
    struct expr* expr,
    struct processing_context* ctx
)
{
    nassert(expr_is_postfix(expr));

    if (!proc_expr(expr->postfix.lhs, ctx))
    {
        return STBOOL_FAILURE;
    }

    switch (expr->tag)
    {
    case EXPR_POSTFIX_FUNCTION_CALL:
        {
            if (!dt_is_function(&expr->postfix.lhs->dt))
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, &expr->postfix.lhs->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(STR_FUNCTION),
                    lhs_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }
            struct func_sig const* const lhs_func_sig =
                expr->postfix.lhs->dt.func_sig;

            bool const is_dot_member_func_call =
                (EXPR_POSTFIX_ACCESS_DOT == expr->postfix.lhs->tag)
                && expr->postfix.lhs->postfix.access.is_member_func;
            bool const is_arrow_member_func_call =
                (EXPR_POSTFIX_ACCESS_ARROW == expr->postfix.lhs->tag)
                && expr->postfix.lhs->postfix.access.is_member_func;
            bool const is_member_func_call =
                is_dot_member_func_call || is_arrow_member_func_call;
            struct expr* member_func_expr;
            struct expr* instance_expr;

            if (is_member_func_call)
            {
                // If the expression is an invocation of a member function, we
                // need to translate the member function call into a function
                // call using the defined function targeted by the member
                // function and add the extra 'this' pointer.
                member_func_expr = expr->postfix.lhs;
                instance_expr = member_func_expr->postfix.lhs;
                nassert(member_func_expr->postfix.access.is_member_func);

                if (!expr_is_lvalue(instance_expr) && is_dot_member_func_call)
                {
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        expr->srctok,
                        STR_STRUCT_DOT_MEMBER_FUNC_CALL_ONLY_ALLOWED_ON_LVALUE
                    );
                    return STBOOL_FAILURE;
                }

                struct expr this_arg;
                expr_init(&this_arg);
                expr_assign(&this_arg, instance_expr, false);
                expr->postfix.args =
                    narr_unshift(expr->postfix.args, &this_arg);
            }

            size_t const num_params = narr_length(lhs_func_sig->param_dts);
            size_t const num_args = narr_length(expr->postfix.args);
            if (num_args != num_params)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_MISMATCHED_NUMBER_OF_ARGS,
                    num_params,
                    num_args
                );
                return STBOOL_FAILURE;
            }

            stbool args_status = STBOOL_SUCCESS;
            for (size_t i = 0; i < num_args; ++i)
            {
                struct expr* const curr_arg = &expr->postfix.args[i];
                struct dt* const curr_arg_dt = &curr_arg->dt;
                struct dt* const func_param_dt = &lhs_func_sig->param_dts[i];

                bool const is_possibly_this_arg = i == 0;
                bool const is_this_arg_dot =
                    is_possibly_this_arg && is_dot_member_func_call;
                bool const is_this_arg_arrow =
                    is_possibly_this_arg && is_arrow_member_func_call;
                bool const is_this_arg = is_this_arg_dot || is_this_arg_arrow;

                // The 'this' argument should have already been
                // processed/assigned above so it should not be reprocessed.
                if (!is_this_arg && !proc_expr(curr_arg, ctx))
                {
                    args_status = STBOOL_FAILURE;
                    continue;
                }

                if (is_possibly_this_arg && is_dot_member_func_call)
                {
                    // The 'this' argument provided to the member function
                    // must be a pointer, but a dot member function call
                    // indicates that the 'this' argument needs to have its
                    // address taken.
                    // We could build an address-of expression here by hand, but
                    // to make sure future changes to the expr struct don't
                    // mess up this portion of the code, the code generation
                    // phase will perform an address-of operation at by
                    // performing the same checks that were just done above.
                    // This may end up being a mistake, so if it turns out there
                    // is yet another data mismatch bug having to do with member
                    // function calls in the future, this is probably the issue.
                    //      - Past Victor
                    struct dt* const new_mem = nalloc(sizeof(struct dt));
                    dt_init(new_mem);
                    new_mem->tag = DT_POINTER;
                    dt_push_modifier(curr_arg_dt, new_mem);
                }
                if (!dt_can_implicitly_convert(curr_arg_dt, func_param_dt))
                {
                    nassert(DT_UNSET != curr_arg->dt.tag);
                    nassert(DT_UNSET != func_param_dt->tag);

                    DECLARE_DT_NSTR(arg_dt_nstr, &curr_arg->dt);
                    DECLARE_DT_NSTR(param_dt_nstr, func_param_dt);
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        expr->postfix.args[i].srctok,
                        FMT_CANNOT_IMPLICITLY_CONVERT_ARGUMENT_TYPE_TO_PARAM_TYPE,
                        i+1,
                        arg_dt_nstr.data,
                        param_dt_nstr.data
                    );
                    args_status = STBOOL_FAILURE;
                }
            }
            if (STBOOL_SUCCESS != args_status)
            {
                return STBOOL_FAILURE;
            }

            nassert(DT_FUNCTION == expr->postfix.lhs->dt.tag);
            dt_assign(&expr->dt, &lhs_func_sig->rtn_dt, true);
            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_POSTFIX_SUBSCRIPT:
        {
            if (
               !dt_is_pointer(&expr->postfix.lhs->dt)
               && !dt_is_array(&expr->postfix.lhs->dt)
            )
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, &expr->postfix.lhs->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(
                        STR_POINTER STR_SEPARATOR_OR STR_ARRAY
                    ),
                    lhs_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }

            if (!proc_expr(expr->postfix.subscript, ctx))
            {
                return STBOOL_FAILURE;
            }
            if (!dt_is_integer_unsigned(&expr->postfix.subscript->dt))
            {
                DECLARE_DT_NSTR(subscript_dt_nstr, &expr->postfix.subscript->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.subscript->srctok,
                    FMT_ILLEGAL_USE_OF_DATA_TYPE
                        " " STR_EXPECTED_X_TYPE(STR_INTEGER_UNSIGNED),
                    subscript_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, &expr->postfix.lhs->dt, true);
            dt_pop_modifier(&expr->dt);

            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_POSTFIX_ACCESS_DOT:
        {
            struct dt* const lhs_dt = &expr->postfix.lhs->dt;
            bool const is_struct = dt_is_struct(lhs_dt);

            if (!is_struct)
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, lhs_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(STR_STRUCT),
                    lhs_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }

            nassert(NULL != lhs_dt->struct_ref->def);
            struct struct_def* const struct_def = lhs_dt->struct_ref->def;
            struct id* const id = &expr->postfix.access.id;

            ssize_t const member_var_idx =
                struct_def_member_var_idx(struct_def, id);
            ssize_t const member_func_idx =
                struct_def_member_func_idx(struct_def, id);
            if (MEMBER_VAR_NOT_FOUND != member_var_idx)
            {
                dt_assign(
                    &expr->dt,
                    &struct_def->member_vars[member_var_idx].dt,
                    true
                );
                expr->valcat = VALCAT_LVALUE;
                expr->is_constexpr = false;
                expr->postfix.access.is_member_func = false;
            }
            else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
            {
                struct member_func* const member_func =
                    &struct_def->member_funcs[member_func_idx];
                dt_assign(
                    &expr->dt,
                    &member_func->dt,
                    true
                );
                expr->valcat = VALCAT_NONE;
                expr->is_constexpr = false;
                expr->postfix.access.is_member_func = true;
            }
            else
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_MEMBER_NOT_IN_STRUCT,
                    (int)id->length,
                    id->start,
                    (int)lhs_dt->struct_ref->id.length,
                    lhs_dt->struct_ref->id.start
                );
                return STBOOL_FAILURE;
            }
        }
        break;

    case EXPR_POSTFIX_ACCESS_ARROW:
        {
            struct dt* const ptr_dt = &expr->postfix.lhs->dt;
            bool const dt_is_ptr = dt_is_pointer(ptr_dt);
            if (!dt_is_ptr)
            {
                DECLARE_DT_NSTR(ptr_dt_nstr, ptr_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(STR_POINTER),
                    ptr_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }
            nassert(NULL != ptr_dt->inner);

            struct dt* const struct_dt = ptr_dt->inner;
            bool const dt_is_ptr_to_struct = dt_is_struct(struct_dt);
            if (!dt_is_ptr_to_struct)
            {
                DECLARE_DT_NSTR(ptr_dt_nstr, ptr_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(
                        STR_POINTER " to " STR_STRUCT
                    ),
                    ptr_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }

            struct struct_def* const struct_def = struct_dt->struct_ref->def;
            struct id* const id = &expr->postfix.access.id;

            ssize_t const member_var_idx =
                struct_def_member_var_idx(struct_def, id);
            ssize_t const member_func_idx =
                struct_def_member_func_idx(struct_def, id);
            if (MEMBER_VAR_NOT_FOUND != member_var_idx)
            {
                dt_assign(
                    &expr->dt,
                    &struct_def->member_vars[member_var_idx].dt,
                    true
                );
                expr->valcat = VALCAT_LVALUE;
                expr->is_constexpr = false;
                expr->postfix.access.is_member_func = false;
            }
            else if (MEMBER_FUNC_NOT_FOUND != member_func_idx)
            {
                struct member_func* const member_func =
                    &struct_def->member_funcs[member_func_idx];
                dt_assign(
                    &expr->dt,
                    &member_func->dt,
                    true
                );
                expr->valcat = VALCAT_NONE;
                expr->is_constexpr = false;
                expr->postfix.access.is_member_func = true;
            }
            else
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->postfix.lhs->srctok,
                    FMT_MEMBER_NOT_IN_STRUCT,
                    (int)id->length,
                    id->start,
                    (int)struct_dt->struct_ref->id.length,
                    struct_dt->struct_ref->id.start
                );
                return STBOOL_FAILURE;
            }
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

stbool proc_expr_unary(
    struct expr* expr,
    struct processing_context* ctx
)
{
    nassert(expr_is_unary(expr));

    if (EXPR_UNARY_COUNTOF_DT == expr->tag || EXPR_UNARY_SIZEOF_DT == expr->tag)
    {
        if (!proc_dt(expr->unary.rhs_dt, ctx))
        {
            return STBOOL_FAILURE;
        }
    }
    else
    {
        if (!proc_expr(expr->unary.rhs_expr, ctx))
        {
            return STBOOL_FAILURE;
        }
    }


    switch (expr->tag)
    {
    case EXPR_UNARY_ADDR_OF:
        {
            if (!expr_is_lvalue(expr->unary.rhs_expr))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_ADDR_OF_MUST_BE_APPLIED_TO_LVALUE,
                    token_type_to_cstr(TOKEN_OPERATOR_QUESTION)
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);
            struct dt* const new_mem = nalloc(sizeof(struct dt));
            dt_init(new_mem);
            new_mem->tag = DT_POINTER;
            dt_push_modifier(&expr->dt, new_mem);

            expr->valcat = VALCAT_RVALUE;
            // The address of a global variable will not change since globals
            // are fixed in memory. If the lvalue is a global then its address
            // is constexpr, otherwise it isn't constexpr.
            expr->is_constexpr = scope_is_global(ctx->curr_scope);
        }
        break;

    case EXPR_UNARY_DEREF:
        {
            if (!dt_is_pointer(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_DEREF_MUST_BE_APPLIED_TO_POINTER,
                    token_type_to_cstr(TOKEN_OPERATOR_AT)
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, &expr->postfix.lhs->dt, true);
            dt_pop_modifier(&expr->dt);

            expr->valcat = VALCAT_LVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_UNARY_DECAY:
        {
            if (!dt_is_array(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_DECAY_MUST_BE_APPLIED_TO_ARRAY,
                    token_type_to_cstr(TOKEN_OPERATOR_DOLLAR)
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);
            expr->dt.tag = DT_POINTER;

            expr->valcat = VALCAT_RVALUE;
            // The address of a global variable will not change since globals
            // are fixed in memory. If the lvalue is a global then its address
            // is constexpr, otherwise it isn't constexpr.
            expr->is_constexpr = scope_is_global(ctx->curr_scope);
        }
        break;

    case EXPR_UNARY_PLUS:
        {
            if (!dt_is_numeric(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_NUMERIC_TYPES,
                    "plus",
                    token_type_to_cstr(TOKEN_OPERATOR_PLUS)
                );
                return STBOOL_FAILURE;
            }
            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_UNARY_MINUS:
        {
            struct dt const* const rhs_dt = &expr->unary.rhs_expr->dt;
            if (!(dt_is_integer_signed(rhs_dt) || dt_is_float(rhs_dt)))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_SIGNED_INTEGER_AND_FLOAT_TYPES,
                    "minus",
                    token_type_to_cstr(TOKEN_OPERATOR_DASH)
                );
                return STBOOL_FAILURE;
            }
            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_UNARY_BITWISE_NOT:
        {
            if (!dt_is_integer(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_INTEGER_TYPES,
                    "bitwise-not",
                    token_type_to_cstr(TOKEN_OPERATOR_TILDE)
                );
                return STBOOL_FAILURE;
            }
            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_UNARY_LOGICAL_NOT:
        {
            if (!dt_is_bool(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_X_MUST_ONLY_BE_APPLIED_TO_BOOL_TYPE,
                    "logical not",
                    token_type_to_cstr(TOKEN_OPERATOR_BANG)
                );
                return STBOOL_FAILURE;
            }
            dt_assign(&expr->dt, &expr->unary.rhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = false;
        }
        break;

    case EXPR_UNARY_COUNTOF_EXPR:
        {
            if (!dt_is_array(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES
                );
            }
            expr->dt.tag = DT_U;

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = true;
        }
        break;

    case EXPR_UNARY_COUNTOF_DT:
        {
            if (!dt_is_array(expr->unary.rhs_dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_COUNTOF_MUST_ONLY_BE_APPLIED_TO_ARRAY_TYPES
                );
            }
            expr->dt.tag = DT_U;

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = true;
        }
        break;

    case EXPR_UNARY_SIZEOF_EXPR:
        {
            if (dt_is_void(&expr->unary.rhs_expr->dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE
                );
                return STBOOL_FAILURE;
            }
            expr->dt.tag = DT_U;

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = true;
        }
        break;

    case EXPR_UNARY_SIZEOF_DT:
        {
            if (dt_is_void(expr->unary.rhs_dt))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_UOP_CANNOT_TAKE_SIZEOF_VOID_TYPE
                );
                return STBOOL_FAILURE;
            }
            expr->dt.tag = DT_U;

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = true;
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    nassert(DT_UNSET != expr->dt.tag);
    nassert(NULL == expr->dt.srctok);
    return STBOOL_SUCCESS;
}

stbool proc_expr_binary(
    struct expr* expr,
    struct processing_context* ctx
)
{
    nassert(expr_is_binary(expr));

    nassert(DT_UNSET == expr->dt.tag);
    nassert(0 == dt_length_modifiers(&expr->dt));
    nassert(NULL == expr->dt.srctok);


    if (!proc_expr(expr->binary.lhs_expr, ctx))
    {
        return STBOOL_FAILURE;
    }
    switch (expr->tag)
    {
    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        if (!proc_expr(expr->binary.rhs_expr, ctx))
        {
            return STBOOL_FAILURE;
        }
        break;

    case EXPR_BINARY_CAST:
        if (!proc_dt(expr->binary.rhs_dt, ctx))
        {
            return STBOOL_FAILURE;
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    switch (expr->tag)
    {
    case EXPR_BINARY_ASSIGN:
        {
            if (!check_assignment_value_categories(expr))
            {
                return STBOOL_FAILURE;
            }
            if (!check_assignment_mutability_constraints(expr))
            {
                return STBOOL_FAILURE;
            }
            if (!check_assignment_implicit_conversion(expr))
            {
                return STBOOL_FAILURE;
            }

            // Assignment expressions always evaluate to a `void` rvalue.
            expr->dt.tag = DT_VOID;
            expr->valcat = VALCAT_RVALUE;
        }
        break;

    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND:
        {
            if (!check_type_equal_bitwise_compatability(expr))
            {
                return STBOOL_FAILURE;
            }

            // LHS chosen arbitrarily.
            dt_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
                && expr->binary.rhs_expr->is_constexpr;
        }
        break;

    case EXPR_BINARY_SHIFT_L: /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:
        {
            if (!check_bit_shift_compatability(expr))
            {
                return STBOOL_FAILURE;
            }

            // Bit-shift expressions always take on the type of the LHS.
            dt_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
                && expr->binary.rhs_expr->is_constexpr;
        }
        break;

    case EXPR_BINARY_PTR_PLUS:  /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:
        {
            if (!dt_is_pointer(&expr->binary.lhs_expr->dt))
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, &expr->binary.lhs_expr->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                        " " STR_EXPECTED_X_TYPE(STR_POINTER),
                    lhs_dt_nstr.data,
                    STR_LEFT_LOWER,
                    token_type_to_cstr(expr->srctok->tag)
                );
                return STBOOL_FAILURE;
            }
            if (!dt_is_integer(&expr->binary.rhs_expr->dt))
            {
                DECLARE_DT_NSTR(rhs_dt_nstr, &expr->binary.rhs_expr->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                        " " STR_EXPECTED_X_TYPE(STR_INTEGER),
                    rhs_dt_nstr.data,
                    STR_RIGHT_LOWER,
                    token_type_to_cstr(expr->srctok->tag)
                );
                return STBOOL_FAILURE;
            }

            // Typeof the result should be the lhs pointer type.
            dt_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
                && expr->binary.rhs_expr->is_constexpr;
        }
        break;

    case EXPR_BINARY_PLUS:  /* intentional fallthrough */
    case EXPR_BINARY_MINUS: /* intentional fallthrough */
    case EXPR_BINARY_MULT:  /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        {
            if (!check_type_equal_arithmetic_compatability(expr))
            {
                return STBOOL_FAILURE;
            }

            // LHS chosen arbitrarily.
            dt_assign(&expr->dt, &expr->binary.lhs_expr->dt, true);

            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = expr->binary.lhs_expr->is_constexpr
                && expr->binary.rhs_expr->is_constexpr;
        }
        break;

    case EXPR_BINARY_PTR_DIFF:
        {
            if (!dt_is_pointer(&expr->binary.lhs_expr->dt))
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, &expr->binary.lhs_expr->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                        " " STR_EXPECTED_X_TYPE(STR_POINTER),
                    lhs_dt_nstr.data,
                    STR_LEFT_LOWER,
                    token_type_to_cstr(expr->srctok->tag)
                );
                return STBOOL_FAILURE;
            }

            if (!dt_is_pointer(&expr->binary.rhs_expr->dt))
            {
                DECLARE_DT_NSTR(rhs_dt_nstr, &expr->binary.rhs_expr->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_BOP_ILLEGAL_USE_OF_XHS_DATA_TYPE
                        " " STR_EXPECTED_X_TYPE(STR_POINTER),
                    rhs_dt_nstr.data,
                    STR_RIGHT_LOWER,
                    token_type_to_cstr(expr->srctok->tag)
                );
                return STBOOL_FAILURE;
            }

            // Pointer subtraction returns `u`, the N-lang equivalent to
            // `ptrdiff_t`.
            expr->dt.tag = DT_U;

            expr->valcat = VALCAT_RVALUE;
        }
        break;

    case EXPR_BINARY_LOGICAL_OR: /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND:
        {
            if (!check_type_equal_logical_compatability(expr))
            {
                return STBOOL_FAILURE;
            }

            // Logical expressions always return a boolean value.
            struct dt dt;
            dt_init(&dt);
            dt.tag = DT_BOOL;
            dt_assign(&expr->dt, &dt, true);
            dt_fini(&dt);

            expr->valcat = VALCAT_RVALUE;
        }
        break;

    case EXPR_BINARY_REL_EQ: /* intentional fallthrough */
    case EXPR_BINARY_REL_NE: /* intentional fallthrough */
    case EXPR_BINARY_REL_LT: /* intentional fallthrough */
    case EXPR_BINARY_REL_LE: /* intentional fallthrough */
    case EXPR_BINARY_REL_GT: /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:
        {
            if (!check_type_equal_relational_compatability(expr))
            {
                return STBOOL_FAILURE;
            }

            // Relational expressions always return a boolean value.
            struct dt dt;
            dt_init(&dt);
            dt.tag = DT_BOOL;
            dt_assign(&expr->dt, &dt, true);
            dt_fini(&dt);

            expr->valcat = VALCAT_RVALUE;
        }
        break;

    case EXPR_BINARY_CAST:
        {
            struct dt const* const lhs_dt = &expr->binary.lhs_expr->dt;
            struct dt const* const rhs_dt = expr->binary.rhs_dt;
            if (!dt_can_explicitly_convert(lhs_dt, rhs_dt))
            {
                DECLARE_DT_NSTR(lhs_dt_nstr, lhs_dt);
                DECLARE_DT_NSTR(rhs_dt_nstr, rhs_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    expr->srctok,
                    FMT_CANNOT_EXPLICITLY_CONVERT_LHS_TO_RHS,
                    lhs_dt_nstr.data,
                    rhs_dt_nstr.data
                );
                return STBOOL_FAILURE;
            }

            dt_assign(&expr->dt, rhs_dt, true);
            expr->valcat = VALCAT_RVALUE;
            expr->is_constexpr = expr->binary.lhs_expr->is_constexpr;
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

stbool proc_expr(
    struct expr* expr,
    struct processing_context* ctx
)
{
    if (expr_is_primary(expr))
    {
        if (!proc_expr_primary(expr, ctx)){return STBOOL_FAILURE;}
    }
    else if (expr_is_postfix(expr))
    {
        if (!proc_expr_postfix(expr, ctx)){return STBOOL_FAILURE;}
        nassert(DT_UNSET != expr->postfix.lhs->dt.tag);
    }
    else if (expr_is_unary(expr))
    {
        if (!proc_expr_unary(expr, ctx)){return STBOOL_FAILURE;}
        nassert(DT_UNSET != expr->unary.rhs_expr->dt.tag);
    }
    else if (expr_is_binary(expr))
    {
        if (!proc_expr_binary(expr, ctx)){return STBOOL_FAILURE;}
        nassert(DT_UNSET != expr->binary.lhs_expr->dt.tag);
        nassert(DT_UNSET != expr->binary.rhs_expr->dt.tag);
    }
    else{NPANIC_UNEXPECTED_CTRL_FLOW();}
    nassert(DT_UNSET != expr->dt.tag);

    if (scope_is_global(ctx->curr_scope) && !expr->is_constexpr)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            expr->srctok,
            STR_NON_CONSTEXPR_EXPR_NOT_ALLOWED_AT_GLOBAL_SCOPE
        );
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      IN-SOURCE IDENTIFIER DECLARATION                                      //
//============================================================================//
//====================================//
//      VARIABLE DECLARATION          //
//====================================//
int prod_var_decl(
    struct var_decl* decl,
    struct token** next
)
{
    struct token* save = *next;
    decl->srctok = save;

    // [ 'export' ] 'extern' 'var' <identifier> ':' <data-type>            ';'
    // [ 'export' ]          'var' <identifier> ':' <data-type> '=' 'void' ';'
    // [ 'export' ]          'var' <identifier> ':' <data-type> '=' <expr> ';'
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXPORT)
    )
    {
        decl->is_export = true;
        save = *next;
    }
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXTERN)
    )
    {
        decl->is_extern = true;
        save = *next;
    }
    if (
        *next = save,
        !match_terminal(next, TOKEN_KEYWORD_LET)
    )
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&decl->id, next),
        save,
        STR_EXPECTED(STR_ID),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_COLON),
        save,
        STR_EXPECTED(STR_COLON),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    MATCH_EXPECTED(
        prod_dt(&decl->dt, next),
        save,
        STR_EXPECTED(STR_DT),
        return PROD_FAILURE_HARD;
    );
    save = *next;

    if (!decl->is_extern)
    {
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_EQUAL),
            save,
            STR_EXPECTED(STR_TOKEN_OPERATOR_ASSIGN),
            return PROD_FAILURE_HARD;
        );
        save = *next;
        if (!match_terminal(next, TOKEN_KEYWORD_VOID))
        {
            *next = save;
            struct expr expr;
            expr_init(&expr);
            MATCH_EXPECTED(
                prod_expr(&expr, next),
                save,
                STR_EXPECTED(STR_EXPR),
                {
                    expr_fini(&expr);
                    return PROD_FAILURE_HARD;
                }
            );
            save = *next;
            decl->def = nalloc(sizeof(struct expr));
            *decl->def = expr;
        }
    }
    save = *next;

    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
        save,
        STR_EXPECTED(STR_SEMICOLON),
        return PROD_FAILURE_HARD;
    );

    return PROD_SUCCESS;
}

stbool proc_var_decl(
    struct var_decl* decl,
    bool is_stack_decl,
    struct processing_context* ctx
)
{
    if (!proc_id(&decl->id, ctx))
    {
        return STBOOL_FAILURE;
    }
    if (!proc_dt(&decl->dt, ctx))
    {
        return STBOOL_FAILURE;
    }

    if (is_stack_decl)
    {
        if (decl->is_export && !scope_is_global(ctx->curr_scope))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                decl->srctok,
                STR_EXPORT_OF_NON_GLOBAL_DECLARATION
            );
            return STBOOL_FAILURE;
        }
        if (decl->is_extern && !scope_is_global(ctx->curr_scope))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                decl->srctok,
                STR_EXTERN_OF_NON_GLOBAL_DECLARATION
            );
            return STBOOL_FAILURE;
        }

        struct symbol symbol;
        symbol_init_variable(&symbol, decl);
        struct symbol* added =
            symbol_table_insert(ctx->curr_scope->symbol_table, &symbol);
        if (NULL == added)
        {
            struct id* id = &decl->id;
            struct symbol* existing_symbol =
                symbol_table_find(ctx->curr_scope->symbol_table, id->start, id->length);
            nassert(NULL != existing_symbol);
            log_redeclaration_of_defined_identifier(existing_symbol, &decl->id);
            symbol_fini(&symbol);
            return STBOOL_FAILURE;
        }

        if (decl->is_export)
        {
            nlogf(
                LOG_DEBUG,
                FMT_ADDING_SYMBOL_TO_MODULE_EXPORTS,
                (int)decl->id.length,
                decl->id.start,
                ctx->module->src_path.data
            );
            module_add_latest_global_symbol_to_exports(ctx->module);
        }

        if (NULL != decl->def)
        {
            if (!proc_expr(decl->def, ctx))
            {
                return STBOOL_FAILURE;
            }
            if (!dt_can_implicitly_convert__variable_init(&decl->def->dt, &decl->dt))
            {
                DECLARE_DT_NSTR(lhs_nstr, &decl->def->dt);
                DECLARE_DT_NSTR(rhs_nstr, &decl->dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    decl->srctok,
                    FMT_CANNOT_IMPLICITLY_CONVERT_RHS_TO_LHS,
                    rhs_nstr.data,
                    lhs_nstr.data
                );
                return STBOOL_FAILURE;
            }
        }
    }

    return STBOOL_SUCCESS;
}

//====================================//
//      FUNCTION DECLARATION          //
//====================================//
int prod_func_decl(
    struct func_decl* decl,
    struct token** next
)
{
    nassert(DT_FUNCTION == decl->dt.tag);
    nassert(NULL != decl->dt.func_sig);

    struct token* save = *next;
    decl->srctok = save;

    // [ 'export' ] [ 'extern' ] 'func' <identifier> ':' <function-signature-wi-params> ';'
    // [ 'export' ]              'func' <identifier> ':' <function-signature-wi-params> <scope>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXPORT)
    )
    {
        decl->is_export = true;
        save = *next;
    }
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXTERN)
    )
    {
        decl->is_extern = true;
        save = *next;
    }
    if (
        *next = save,
        !match_terminal(next, TOKEN_KEYWORD_FUNC)
    )
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&decl->id, next),
        save,
        STR_EXPECTED(STR_ID),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_COLON),
        save,
        STR_EXPECTED(STR_COLON),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    MATCH_EXPECTED(
        prod_func_sig(decl->dt.func_sig, &decl->param_ids, next),
        save,
        STR_EXPECTED(STR_FUNC_SIG),
        return PROD_FAILURE_HARD;
    );
    save = *next;

    if (decl->is_extern)
    {
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            return PROD_FAILURE_HARD;
        );
    }
    else if (!decl->is_extern)
    {
        if (!match_terminal(next, TOKEN_OPERATOR_SEMICOLON))
        {
            *next = save;
            struct scope def;
            scope_init(&def, UNDEFINED_SCOPE_PARENT);
            MATCH_EXPECTED(
                PROD_SUCCESS == prod_scope(&def, next),
                save,
                STR_EXPECTED(STR_FUNC_BODY),
                {
                    scope_fini(&def);
                    return PROD_FAILURE_HARD;
                }
            );
            decl->def = nalloc(sizeof(struct scope));
            *decl->def = def;
        }
    }
    else
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            save,
            STR_EXPECTED(STR_SEMICOLON STR_SEPARATOR_OR STR_FUNC_BODY)
        );
        return PROD_FAILURE_HARD;
    }

    return PROD_SUCCESS;
}

stbool proc_func_decl(
    struct func_decl* decl,
    struct processing_context* ctx
)
{
    nassert(DT_FUNCTION == decl->dt.tag);
    nassert(NULL != decl->dt.func_sig);

    //// Params and return type.
    if (!proc_func_sig(decl->dt.func_sig, ctx))
    {
        return STBOOL_FAILURE;
    }
    for (size_t i = 0; i < narr_length(decl->param_ids); ++i)
    {
        // Multiple parameters with the same name cannot exist within a function
        // definition by the one definition rule. Since a function signature
        // itself has no scope to check identifiers against, we'll just check
        // that the identifier has a unique name by comparing it to every other
        // identifier.
        for (size_t j = i+1; j < narr_length(decl->param_ids); ++j)
        {
            if (
                (
                    decl->param_ids[i].length ==
                    decl->param_ids[j].length
                )
                && cstr_n_cmp(
                    decl->param_ids[i].start,
                    decl->param_ids[j].start,
                    decl->param_ids[i].length
                ) == 0
            )
            {
                return STBOOL_FAILURE;
            }
        }
    }
    for (size_t i = 0; i < narr_length(decl->dt.func_sig->param_dts); ++i)
    {
        struct dt const* dt = &decl->dt.func_sig->param_dts[i];
        if (dt_is_array(dt))
        {
            NLOGF_FLC_TOK(
                LOG_ERROR,
                dt->srctok,
                FMT_CANNOT_PASS_ARRAY_BY_VALUE_TO_FUNC
            );
            return STBOOL_FAILURE;
        }
    }

    //// Function name
    struct symbol symbol;
    symbol_init_function(&symbol, decl);

    struct symbol* existing_symbol =
        symbol_table_find(ctx->curr_scope->symbol_table, decl->id.start, decl->id.length);
    if (NULL == existing_symbol)
    {
        existing_symbol = symbol_table_insert(ctx->curr_scope->symbol_table, &symbol);
        nassert(NULL != existing_symbol);

        if (decl->is_export)
        {
            nlogf(
                LOG_DEBUG,
                FMT_ADDING_SYMBOL_TO_MODULE_EXPORTS,
                (int)decl->id.length,
                decl->id.start,
                ctx->module->src_path.data
            );
            module_add_latest_global_symbol_to_exports(ctx->module);
        }
    }
    else
    {
        symbol_fini(&symbol);
        switch (existing_symbol->tag)
        {
        case SYMBOL_VARIABLE: /* intentional fallthrough */
        case SYMBOL_STRUCT:   /* intentional fallthrough */
        case SYMBOL_ALIAS:
            log_redeclaration_of_defined_identifier(existing_symbol, &decl->id);
            return STBOOL_FAILURE;

        case SYMBOL_FUNCTION:
            // Redeclaration of a function is fine as long as the function is
            // not yet defined.
            if (existing_symbol->is_defined)
            {
                log_redeclaration_of_defined_identifier(existing_symbol, &decl->id);
                return STBOOL_FAILURE;
            }
            // This declaration should automatically inherit a set export
            // attribute from its previous declaration.
            if (existing_symbol->is_export)
            {
                decl->is_export = true;
            }
            break;

        default:
            NPANIC_DFLT_CASE();
        }
    }

    if (decl->is_export && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            decl->srctok,
            STR_EXPORT_OF_NON_GLOBAL_DECLARATION
        );
        return STBOOL_FAILURE;
    }
    if (decl->is_extern && !scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            decl->srctok,
            STR_EXTERN_OF_NON_GLOBAL_DECLARATION
        );
        return STBOOL_FAILURE;
    }

    //// Function definition
    if (NULL != decl->def)
    {
        existing_symbol->is_defined = true;

        if (!preproc_scope(decl->def, ctx))
        {
            return STBOOL_FAILURE;
        }
        typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
        typeof(ctx->curr_func) save_curr_func = ctx->curr_func;
        ctx->curr_scope = decl->def;
        ctx->curr_func = decl->dt.func_sig;

        stbool status = STBOOL_SUCCESS;
        nassert(
            narr_length(decl->dt.func_sig->param_dts)
            ==
            narr_length(decl->param_ids)
        );
        for (size_t i = 0; i < narr_length(decl->param_ids); ++i)
        {
            struct id* const param_id = &decl->param_ids[i];
            struct dt* const param_dt = &decl->dt.func_sig->param_dts[i];

            struct symbol* existing_symbol =
                symbol_table_find(ctx->curr_scope->symbol_table, param_id->start, param_id->length);
            if (NULL != existing_symbol)
            {
                log_redeclaration_of_defined_identifier(existing_symbol, param_id);
                return STBOOL_FAILURE;
            }

            struct symbol symbol;
            symbol_init(&symbol);
            symbol.tag = SYMBOL_VARIABLE;
            symbol.id = param_id;
            symbol.dt = param_dt;
            symbol.srctok = param_id->srctok;
            if (NULL == symbol_table_insert(ctx->curr_scope->symbol_table, &symbol))
            {
                struct symbol* existing_symbol =
                    symbol_table_find(ctx->curr_scope->symbol_table, param_id->start, param_id->length);
                nassert(NULL != existing_symbol);
                log_redeclaration_of_defined_identifier(existing_symbol, param_id);
                symbol_fini(&symbol);
                return STBOOL_FAILURE;
            }
        }
        if (STBOOL_SUCCESS != status)
        {
            return STBOOL_FAILURE;
        }

        if (!proc_scope(decl->def, ctx))
        {
            return STBOOL_FAILURE;
        }

        ctx->curr_scope = save_curr_scope;
        ctx->curr_func = save_curr_func;
    }

    return STBOOL_SUCCESS;
}

//====================================//
//      MEMBER VARIABLE               //
//====================================//
stbool prod_member_var(
    struct member_var* memb,
    struct token** next
)
{
    struct token* save = *next;
    memb->srctok = save;

    // 'var' <identifier> : <data-type> ';'
    if (!match_terminal(next, TOKEN_KEYWORD_LET))
    {
        return STBOOL_FAILURE;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&memb->id, next),
        save,
        STR_EXPECTED(STR_ID),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_COLON),
        save,
        STR_EXPECTED(STR_COLON),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        prod_dt(&memb->dt, next),
        save,
        STR_EXPECTED(STR_DT),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
        save,
        STR_EXPECTED(STR_SEMICOLON),
        return STBOOL_FAILURE;
    );

    return STBOOL_SUCCESS;
}

stbool proc_member_var(
    struct member_var* memb,
    struct processing_context* ctx
)
{
    nassert(scope_is_global(ctx->curr_scope));
    return proc_id(&memb->id, ctx)
        && proc_dt(&memb->dt, ctx);
}

//====================================//
//      MEMBER FUNCTION               //
//====================================//
stbool prod_member_func(
    struct member_func* memb,
    struct token** next
)
{
    struct token* save = *next;
    memb->srctok = save;

    // 'func' <identifier> '=' <identifier> ';'
    if (!match_terminal(next, TOKEN_KEYWORD_FUNC))
    {
        return STBOOL_FAILURE;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&memb->id, next),
        save,
        STR_EXPECTED(STR_ID),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_EQUAL),
        save,
        STR_EXPECTED(STR_TOKEN_OPERATOR_ASSIGN),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        prod_id(&memb->target.id, next),
        save,
        STR_EXPECTED(STR_ID),
        return STBOOL_FAILURE;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
        save,
        STR_EXPECTED(STR_SEMICOLON),
        return STBOOL_FAILURE;
    );

    return STBOOL_SUCCESS;
}

stbool proc_member_func(
    struct member_func* memb,
    struct processing_context* ctx
)
{
    nassert(scope_is_global(ctx->curr_scope));
    if (!proc_id(&memb->id, ctx))
    {
        return STBOOL_FAILURE;
    }
    if (!proc_id(&memb->target.id, ctx))
    {
        return STBOOL_FAILURE;
    }

    struct id* const target_id = &memb->target.id;
    struct symbol* target =
        symbol_table_find(ctx->curr_scope->symbol_table, target_id->start, target_id->length);
    if (NULL == target)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            memb->srctok,
            FMT_MEMBER_FUNC_NO_TARGET,
            (int)memb->target.id.length,
            memb->target.id.start
        );
        return STBOOL_FAILURE;
    }
    if (SYMBOL_FUNCTION != target->tag)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            memb->srctok,
            FMT_MEMBER_FUNC_TARGET_NOT_FUNC,
            (int)memb->target.id.length,
            memb->target.id.start
        );
        return STBOOL_FAILURE;
    }

    dt_assign(&memb->dt, target->dt, true);
    nassert(DT_FUNCTION == memb->dt.tag);

    return STBOOL_SUCCESS;
}

//====================================//
//      STRUCT DECLARATION            //
//====================================//
int prod_struct_decl(
    struct struct_decl* decl,
    struct token** next
)
{
    struct token* save = *next;
    decl->srctok = save;

    // [ 'export' ] [ 'extern' ] 'struct' <identifier> ';'
    // [ 'export' ]              'struct' <identifier>  <struct-definition>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXPORT)
    )
    {
        decl->is_export = true;
        save = *next;
    }
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXTERN)
    )
    {
        decl->is_extern = true;
        save = *next;
    }
    if (
        *next = save,
        !match_terminal(next, TOKEN_KEYWORD_STRUCT)
    )
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&decl->id, next),
        save,
        STR_EXPECTED(STR_ID),
        return PROD_FAILURE_HARD;
    );
    save = *next;

    if (decl->is_extern)
    {
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            return PROD_FAILURE_HARD;
        );
    }
    else if (!decl->is_extern)
    {
        if (!match_terminal(next, TOKEN_OPERATOR_SEMICOLON))
        {
            *next = save;
            struct struct_def struct_def;
            struct_def_init(&struct_def);
            MATCH_EXPECTED(
                PROD_SUCCESS == prod_struct_def(&struct_def, next),
                save,
                STR_EXPECTED(STR_STRUCT_DEF),
                {
                    struct_def_fini(&struct_def);
                    return PROD_FAILURE_HARD;
                }
            );
            decl->def = nalloc(sizeof(struct struct_def));
            *decl->def = struct_def;
        }
    }
    else
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            save,
            STR_EXPECTED(STR_SEMICOLON STR_SEPARATOR_OR STR_STRUCT_DEF)
        );
        return PROD_FAILURE_HARD;
    }

    return PROD_SUCCESS;
}

stbool proc_struct_decl(
    struct struct_decl* decl,
    struct processing_context* ctx
)
{
    if (!proc_id(&decl->id, ctx))
    {
        return STBOOL_FAILURE;
    }
    if (!scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            decl->srctok,
            STR_STRUCT_DECL_ONLY_ALLOWED_AT_GLOBAL_SCOPE
        );
        return STBOOL_FAILURE;
    }

    struct symbol new_symbol;
    symbol_init_struct(&new_symbol, decl);

    struct symbol* symbol =
        symbol_table_insert(ctx->curr_scope->symbol_table, &new_symbol);
    if (NULL != symbol)
    {
        if (decl->is_export)
        {
            nlogf(
                LOG_DEBUG,
                FMT_ADDING_SYMBOL_TO_MODULE_EXPORTS,
                (int)decl->id.length,
                decl->id.start,
                ctx->module->src_path.data
            );
            module_add_latest_global_symbol_to_exports(ctx->module);
        }

        if (NULL != decl->def)
        {
            if (!proc_struct_def(decl->def, ctx))
            {
                return STBOOL_FAILURE;
            }
            symbol->dt->struct_ref = decl;
            symbol->is_defined = true;
        }
    }
    else
    {
        symbol_fini(&new_symbol);
        struct id* const decl_id = &decl->id;
        symbol = symbol_table_find(ctx->curr_scope->symbol_table, decl_id->start, decl_id->length);
        switch (symbol->tag)
        {
        case SYMBOL_VARIABLE: /* intentional fallthrough */
        case SYMBOL_FUNCTION: /* intentional fallthrough */
        case SYMBOL_ALIAS:
            log_redeclaration_of_defined_identifier(symbol, &decl->id);
            return STBOOL_FAILURE;
        case SYMBOL_STRUCT:
            // Redeclaration of a struct is fine as long as the struct is not
            // yet defined.
            if (symbol->is_defined)
            {
                log_redeclaration_of_defined_identifier(symbol, &decl->id);
                return STBOOL_FAILURE;
            }
            // This declaration should automatically inherit a set export
            // attribute from its previous declaration.
            if (symbol->is_export)
            {
                decl->is_export = true;
            }
            break;
        default:
            NPANIC_DFLT_CASE();
        }

        if (NULL != decl->def)
        {
            if (!proc_struct_def(decl->def, ctx))
            {
                return STBOOL_FAILURE;
            }
            // Add the definition of this struct to the symbol.
            nassert(NULL != symbol);
            nassert(NULL != symbol->dt);
            nassert(NULL == symbol->dt->struct_ref->def);
            symbol->dt->struct_ref = decl;
            symbol->is_defined = true;
        }
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      CONTROL CONSTRUCTS                                                    //
//============================================================================//
//====================================//
//      LOOP                          //
//====================================//
int prod_loop(
    struct loop* loop,
    struct token** next
)
{
    struct token* save = *next;
    loop->srctok = save;

    // 'loop' ...
    if (!match_terminal(next, TOKEN_KEYWORD_LOOP))
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;

    // ... <expr> <scope>
    struct expr cond_expr;
    expr_init(&cond_expr);
    if (!prod_expr(&cond_expr, next))
    {
        expr_fini(&cond_expr);
        goto try_prod_loop_range;
    }
    int prod_scope_status = prod_scope(loop->body, next);
    if (PROD_FAILURE_SOFT == prod_scope_status)
    {
        expr_fini(&cond_expr);
        goto try_prod_loop_range;
    }
    else if (PROD_FAILURE_HARD == prod_scope_status)
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            loop->body->srctok,
            STR_EXPECTED(STR_LOOP_BODY)
        );
        return PROD_FAILURE_HARD;
    }
    loop->tag = LOOP_CONDITIONAL;
    loop->cond.expr = nalloc(sizeof(struct expr));
    *loop->cond.expr = cond_expr;
    return PROD_SUCCESS;

try_prod_loop_range:;
    // ... <identifier> : <data-type> 'in' '[' <expr> ',' <expr> ']' <scope>
    // ... <identifier> : <data-type> 'in' '[' <expr> ',' <expr> ')' <scope>
    // ... <identifier> : <data-type> 'in' '(' <expr> ',' <expr> ']' <scope>
    // ... <identifier> : <data-type> 'in' '(' <expr> ',' <expr> ')' <scope>
    struct var_decl var_decl;
    struct expr lower_bound;
    struct expr upper_bound;
    var_decl_init(&var_decl);
    expr_init(&lower_bound);
    expr_init(&upper_bound);
    #define LOOP_RANGE_FAILURE()                                               \
        {                                                                      \
            var_decl_fini(&var_decl);                                          \
            expr_fini(&lower_bound);                                           \
            expr_fini(&upper_bound);                                           \
            return PROD_FAILURE_HARD;                                          \
        }

    *next = save;
    MATCH_EXPECTED(
        prod_id(&var_decl.id, next),
        save,
        STR_EXPECTED(STR_ID),
        LOOP_RANGE_FAILURE();
    );
    var_decl.srctok = save;
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_COLON),
        save,
        STR_EXPECTED_AFTER(STR_COLON, STR_ID),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    MATCH_EXPECTED(
        prod_dt(&var_decl.dt, next),
        save,
        STR_EXPECTED_AFTER(STR_DT, STR_COLON),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_KEYWORD_IN),
        save,
        STR_EXPECTED(STR_IN),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    if (match_terminal(next, TOKEN_OPERATOR_LEFTBRACKET))
    {
        loop->range.lower_clusivity = LOWER_INCLUSIVE;
    }
    else if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_LEFTPARENTHESIS)
    )
    {
        loop->range.lower_clusivity = LOWER_EXCLUSIVE;
    }
    else
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            loop->srctok,
            STR_EXPECTED(STR_SQUARE_BRACKET_L STR_SEPARATOR_OR STR_PAREN_L)
        );
        LOOP_RANGE_FAILURE();
    }
    save = *next;
    MATCH_EXPECTED(
        prod_expr(&lower_bound, next),
        save,
        STR_EXPECTED(STR_EXPR),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_COMMA),
        save,
        STR_EXPECTED(STR_COMMA),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    MATCH_EXPECTED(
        prod_expr(&upper_bound, next),
        save,
        STR_EXPECTED(STR_EXPR),
        LOOP_RANGE_FAILURE();
    );
    save = *next;
    if (match_terminal(next, TOKEN_OPERATOR_RIGHTBRACKET))
    {
        loop->range.upper_clusivity = UPPER_INCLUSIVE;
    }
    else if (
        *next = save,
        match_terminal(next, TOKEN_OPERATOR_RIGHTPARENTHESIS)
    )
    {
        loop->range.upper_clusivity = UPPER_EXCLUSIVE;
    }
    else
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            loop->srctok,
            STR_EXPECTED(STR_SQUARE_BRACKET_R STR_SEPARATOR_OR STR_PAREN_R)
        );
        LOOP_RANGE_FAILURE();
    }
    save = *next;
    MATCH_EXPECTED(
        PROD_SUCCESS == prod_scope(loop->body, next),
        save,
        STR_EXPECTED(STR_LOOP_BODY),
        LOOP_RANGE_FAILURE();
    );
    loop->tag = LOOP_RANGE;
    loop->range.var_decl = var_decl;
    loop->range.lower_bound = nalloc(sizeof(struct expr));
    *loop->range.lower_bound = lower_bound;
    loop->range.upper_bound = nalloc(sizeof(struct expr));
    *loop->range.upper_bound = upper_bound;

    return PROD_SUCCESS;
}

stbool proc_loop(
    struct loop* loop,
    struct processing_context* ctx
)
{
    if (scope_is_global(ctx->curr_scope))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            loop->srctok,
            STR_LOOP_NOT_ALLOWED_AT_GLOBAL_SCOPE
        );
        return STBOOL_FAILURE;
    }

    if (!preproc_scope(loop->body, ctx))
    {
        return STBOOL_FAILURE;
    }
    typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
    bool const save_is_within_loop =
        ctx->curr_scope->is_within_loop;
    bool const save_is_within_loop_primary =
        ctx->curr_scope->is_within_loop_primary;
    ctx->curr_scope = loop->body;
    ctx->curr_scope->is_within_loop = true;
    ctx->curr_scope->is_within_loop_primary = true;

    switch (loop->tag)
    {
    case LOOP_CONDITIONAL:
        if (!proc_expr(loop->cond.expr, ctx)){return STBOOL_FAILURE;}
        break;
    case LOOP_RANGE:
        {
            nassert(
                (LOWER_EXCLUSIVE == loop->range.lower_clusivity
                || LOWER_INCLUSIVE == loop->range.lower_clusivity)
                &&
                (UPPER_EXCLUSIVE == loop->range.upper_clusivity
                || UPPER_INCLUSIVE == loop->range.upper_clusivity)
            );
            struct dt* const var_dt = &loop->range.var_decl.dt;
            struct dt* const lower_dt = &loop->range.var_decl.dt;
            struct dt* const upper_dt = &loop->range.var_decl.dt;
            if (!proc_var_decl(&loop->range.var_decl, true, ctx)){return STBOOL_FAILURE;}
            if (!proc_expr(loop->range.lower_bound, ctx)){return STBOOL_FAILURE;}
            if (!proc_expr(loop->range.upper_bound, ctx)){return STBOOL_FAILURE;}
            if (!dt_can_implicitly_convert(var_dt, lower_dt))
            {
                DECLARE_DT_NSTR(var_type_nstr, var_dt);
                DECLARE_DT_NSTR(range_type_nstr, lower_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    loop->srctok,
                    FMT_CANNOT_IMPLICITLY_LOOP_VAR_TO_LOOP_RANGE_LOWER,
                    var_type_nstr.data,
                    range_type_nstr.data
                );
                return STBOOL_FAILURE;
            }
            if (!dt_can_implicitly_convert(var_dt, upper_dt))
            {
                DECLARE_DT_NSTR(var_type_nstr, var_dt);
                DECLARE_DT_NSTR(range_type_nstr, upper_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    loop->srctok,
                    FMT_CANNOT_IMPLICITLY_LOOP_VAR_TO_LOOP_RANGE_UPPER,
                    var_type_nstr.data,
                    range_type_nstr.data
                );
                return STBOOL_FAILURE;
            }

            stbool var_type_status = STBOOL_SUCCESS;
            if (!dt_is_integer(var_dt))
            {
                DECLARE_DT_NSTR(var_type_nstr, var_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    var_dt->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(STR_INTEGER),
                    var_type_nstr.data
                );
                var_type_status = STBOOL_FAILURE;
            }
            if (!(var_dt->qualifiers.is_mut))
            {
                DECLARE_DT_NSTR(var_type_nstr, var_dt);
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    var_dt->srctok,
                    FMT_EXPR_HAS_TYPE_X_EXPECTED_Y(STR_MUTABLE),
                    var_type_nstr.data
                );
                var_type_status = STBOOL_FAILURE;
            }
            if (STBOOL_SUCCESS != var_type_status)
            {
                return var_type_status;
            }
        }
        break;
    case LOOP_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    if (!proc_scope(loop->body, ctx))
    {
        return STBOOL_FAILURE;
    }
    ctx->curr_scope = save_curr_scope;
    ctx->curr_scope->is_within_loop = save_is_within_loop;
    ctx->curr_scope->is_within_loop_primary = save_is_within_loop_primary;

    return STBOOL_SUCCESS;
}

//============================================================================//
//      ALIAS                                                                 //
//============================================================================//
int prod_alias(
    struct alias* alias,
    struct token** next
)
{
    struct token* save = *next;
    alias->srctok = save;

    // [ 'export' ] 'alias' <identifier> = <data-type>
    if (
        *next = save,
        match_terminal(next, TOKEN_KEYWORD_EXPORT)
    )
    {
        alias->is_export = true;
        save = *next;
    }
    if (
        *next = save,
        !match_terminal(next, TOKEN_KEYWORD_ALIAS)
    )
    {
        return PROD_FAILURE_SOFT;
    }
    save = *next;
    MATCH_EXPECTED(
        prod_id(&alias->new_id, next),
        save,
        STR_EXPECTED(STR_ID),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    MATCH_EXPECTED(
        match_terminal(next, TOKEN_OPERATOR_EQUAL),
        save,
        STR_EXPECTED(STR_TOKEN_OPERATOR_ASSIGN),
        return PROD_FAILURE_HARD;
    );
    save = *next;
    struct dt dt;
    dt_init(&dt);
    MATCH_EXPECTED(
        prod_dt(&dt, next),
        save,
        STR_EXPECTED(STR_DT),
        {
            dt_fini(&dt);
            return PROD_FAILURE_HARD;
        }
    );

    alias->tag = ALIAS_DT;
    alias->aliased.dt = dt;
    return PROD_SUCCESS;
}

stbool proc_alias(
    struct alias* alias,
    struct processing_context* ctx
)
{
    nassert(NULL != alias);
    nassert(NULL != alias->srctok);
    // Aliases are only ever allowed at global scope, which should already be
    // checked in proc_stmt, so there's no need to recheck whether we're in the
    // global scope.
    // Along these lines, if the alias is exported we know the export is valid
    // since we are at global scope.

    if (!proc_id(&alias->new_id, ctx))
    {
        return STBOOL_FAILURE;
    }

    switch (alias->tag)
    {
    case ALIAS_DT:
        {
            if (!proc_dt(&alias->aliased.dt, ctx))
            {
                return STBOOL_FAILURE;
            }

            struct dt const* const adt = &alias->aliased.dt;
            // The outermost modifier/base type of the data type must be
            // unqualified so that future uses of the alias can qualify the
            // type appropriately for the situation.
            if (!qualifiers_is_default(&adt->qualifiers))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    alias->srctok,
                    STR_ALIAS_OUTERMOST_MUST_BE_UNQUALIFIED
                );
                return STBOOL_FAILURE;
            }

            struct id* const new_id = &alias->new_id;
            struct symbol* existing_symbol =
                symbol_table_find(ctx->curr_scope->symbol_table, new_id->start, new_id->length);
            if (NULL != existing_symbol)
            {
                log_redeclaration_of_defined_identifier(
                    existing_symbol,
                    &alias->new_id
                );
                return STBOOL_FAILURE;
            }

            struct symbol alias_symbol;
            symbol_init_alias(&alias_symbol, alias);
            struct symbol* added = symbol_table_insert(
                ctx->curr_scope->symbol_table,
                &alias_symbol
            );
            (void) added;
            nassert(NULL != added);

            if (alias->is_export)
            {
                nlogf(
                    LOG_DEBUG,
                    FMT_ADDING_SYMBOL_TO_MODULE_EXPORTS,
                    (int)alias->new_id.length,
                    alias->new_id.start,
                    ctx->module->src_path.data
                );
                module_add_latest_global_symbol_to_exports(ctx->module);
            }
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
stbool prod_stmt(struct stmt* stmt, struct token** next)
{
    struct token* save = *next;
    stmt->srctok = save;

    // ';'
    {
        if (match_terminal(next, TOKEN_OPERATOR_SEMICOLON))
        {
            stmt->tag = STMT_EMPTY;
            return STBOOL_SUCCESS;
        }
    }

    // <variable-declaration>
    {
        struct var_decl var_decl;
        var_decl_init(&var_decl);

        *next = save;
        int status = prod_var_decl(&var_decl, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_var_decl_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_var_decl_failure__hard;
        }
        stmt->tag = STMT_VARIABLE_DECLARATION;
        stmt->var_decl = nalloc(sizeof(struct var_decl));
        *stmt->var_decl = var_decl;
        return STBOOL_SUCCESS;

prod_var_decl_failure__hard:
        var_decl_fini(&var_decl);
        return STBOOL_FAILURE;

prod_var_decl_failure__soft:
        var_decl_fini(&var_decl);
    }

    // <function-declaration>
    {
        struct func_decl func_decl;
        func_decl_init(&func_decl);

        *next = save;
        int const status = prod_func_decl(&func_decl, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_func_decl_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_func_decl_failure__hard;
        }
        stmt->tag = STMT_FUNCTION_DECLARATION;
        stmt->func_decl = nalloc(sizeof(struct func_decl));
        *stmt->func_decl = func_decl;
        return STBOOL_SUCCESS;

prod_func_decl_failure__hard:
        func_decl_fini(&func_decl);
        return STBOOL_FAILURE;

prod_func_decl_failure__soft:
        func_decl_fini(&func_decl);
    }

    // <struct-declaration>
    {
        struct struct_decl struct_decl;
        struct_decl_init(&struct_decl);

        *next = save;
        int const status = prod_struct_decl(&struct_decl, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_struct_decl_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_struct_decl_failure__hard;
        }
        stmt->tag = STMT_STRUCT_DECLARATION;
        stmt->struct_decl = nalloc(sizeof(struct struct_decl));
        *stmt->struct_decl = struct_decl;
        return STBOOL_SUCCESS;

prod_struct_decl_failure__hard:
        struct_decl_fini(&struct_decl);
        return STBOOL_FAILURE;

prod_struct_decl_failure__soft:
        struct_decl_fini(&struct_decl);
    }

    // <if-elif-else-stmt>
    {
        struct token* if_elif_else_save = save;

        // 'if' <expr> <scope>
        if (
            *next = if_elif_else_save,
            !match_terminal(next, TOKEN_KEYWORD_IF)
        )
        {
            goto prod_if_elif_else_stmt_failure__soft;
        }
        if_elif_else_save = *next;
        if_elif_else_save = *next;
        struct indicative_conditional ind_cond;
        indicative_conditional_init(&ind_cond);
        MATCH_EXPECTED(
            prod_expr(&ind_cond.condition, next),
            if_elif_else_save,
            STR_EXPECTED(STR_EXPR),
            goto prod_if_elif_else_stmt_failure__fini_ind_cond;
        );
        if_elif_else_save = *next;
        int const status = prod_scope(ind_cond.body, next);
        if (PROD_SUCCESS != status)
        {
            goto prod_if_elif_else_stmt_failure__fini_ind_cond;
        }
        if_elif_else_save = *next;

        stmt->tag = STMT_IF_ELIF_ELSE;
        stmt->if_elif_else = nalloc(sizeof(struct if_elif_else));
        if_elif_else_init(stmt->if_elif_else);
        stmt->if_elif_else->if_elif_ind_cond =
            narr_push(stmt->if_elif_else->if_elif_ind_cond, &ind_cond);

        // ... ('elif' <expr> <scope>)* ...
        LOOP_FOREVER
        {
            if (
                *next = if_elif_else_save,
                !match_terminal(next, TOKEN_KEYWORD_ELIF)
            )
            {
                // No more `elif`s exist in the if-elif-else.
                break;
            }
            if_elif_else_save = *next;
            if_elif_else_save = *next;
            indicative_conditional_init(&ind_cond);
            MATCH_EXPECTED(
                prod_expr(&ind_cond.condition, next),
                if_elif_else_save,
                STR_EXPECTED(STR_EXPR),
                goto prod_if_elif_else_stmt_failure__fini_ind_cond;
            );
            if_elif_else_save = *next;
            int const status = prod_scope(ind_cond.body, next);
            if (PROD_SUCCESS != status)
            {
                goto prod_if_elif_else_stmt_failure__fini_ind_cond;
            }
            if_elif_else_save = *next;
            stmt->if_elif_else->if_elif_ind_cond =
                narr_push(stmt->if_elif_else->if_elif_ind_cond, &ind_cond);
        }

        // ... ('else' <scope>)?
        if (
            *next = if_elif_else_save,
            match_terminal(next, TOKEN_KEYWORD_ELSE)
        )
        {
            struct scope else_body;
            scope_init(&else_body, UNDEFINED_SCOPE_PARENT);
            int const status = prod_scope(&else_body, next);
            if (PROD_SUCCESS != status)
            {
                scope_fini(&else_body);
                goto prod_if_elif_else_stmt_failure__hard;
            }
            stmt->if_elif_else->else_body = nalloc(sizeof(struct scope));
            *stmt->if_elif_else->else_body = else_body;
            if_elif_else_save = *next;
        }

        *next = if_elif_else_save;
        return STBOOL_SUCCESS;

prod_if_elif_else_stmt_failure__fini_ind_cond:
        indicative_conditional_fini(&ind_cond);
prod_if_elif_else_stmt_failure__hard:
        return STBOOL_FAILURE;

prod_if_elif_else_stmt_failure__soft:
        /*label statement*/;
    }

    // <loop>
    {
        struct loop loop;
        loop_init(&loop);
        *next = save;
        int const status = prod_loop(&loop, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_loop_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_loop_failure__hard;
        }
        stmt->tag = STMT_LOOP;
        stmt->loop = nalloc(sizeof(struct loop));
        *stmt->loop = loop;
        return STBOOL_SUCCESS;

prod_loop_failure__hard:
        loop_fini(&loop);
        return STBOOL_FAILURE;

prod_loop_failure__soft:
        loop_fini(&loop);
    }

    // <defer-stmt>
    {
        struct token* defer_stmt_save = save;

        // 'defer' <scope>
        if (
            *next = defer_stmt_save,
            !match_terminal(next, TOKEN_KEYWORD_DEFER)
        )
        {
            goto prod_defer_stmt_failure__soft;
        }
        defer_stmt_save = *next;
        struct scope scope;
        scope_init(&scope, UNDEFINED_SCOPE_PARENT);
        int const status = prod_scope(&scope, next);
        if (PROD_SUCCESS != status)
        {
            goto prod_defer_stmt_failure__hard;
        }

        stmt->tag = STMT_DEFER;
        stmt->defer.scope = nalloc(sizeof(struct scope));
        *stmt->defer.scope = scope;
        return STBOOL_SUCCESS;

prod_defer_stmt_failure__hard:
        scope_fini(&scope);
        return STBOOL_FAILURE;

prod_defer_stmt_failure__soft:
        /*label statement*/;
    }

    // <break-stmt>
    {
        struct token* break_save = save;

        if (
            *next = break_save,
            !match_terminal(next, TOKEN_KEYWORD_BREAK)
        )
        {
            goto prod_break_stmt_failure__soft;
        }
        break_save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            break_save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_break_stmt_failure__hard;
        );

        stmt->tag = STMT_BREAK;
        return STBOOL_SUCCESS;

prod_break_stmt_failure__hard:
        return STBOOL_FAILURE;

prod_break_stmt_failure__soft:
        /*label statement*/;
    }

    // <continue-stmt>
    {
        struct token* continue_save = save;

        if (
            *next = continue_save,
            !match_terminal(next, TOKEN_KEYWORD_CONTINUE)
        )
        {
            goto prod_continue_stmt_failure__soft;
        }
        continue_save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            continue_save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_continue_stmt_failure__hard;
        );

        stmt->tag = STMT_CONTINUE;
        return STBOOL_SUCCESS;

prod_continue_stmt_failure__hard:
        return STBOOL_FAILURE;

prod_continue_stmt_failure__soft:
        /*label statement*/;
    }

    // <return-stmt> ';'
    {
        struct token* rtn_save = save;

        // Required 'return'.
        if (
            *next = rtn_save,
            !match_terminal(next, TOKEN_KEYWORD_RETURN)
        )
        {
            goto prod_return_stmt_failure__soft;
        }
        rtn_save = *next;

        // Optional return expression.
        stmt->rtn_expr = NULL;
        struct expr rtn_expr;
        expr_init(&rtn_expr);
        if (
            *next = rtn_save,
            prod_expr(&rtn_expr, next)
        )
        {
            rtn_save = *next;
            stmt->rtn_expr = nalloc(sizeof(struct expr));
            *stmt->rtn_expr = rtn_expr;
        }
        else
        {
            expr_fini(&rtn_expr);
        }

        // Required ';'.
        *next = rtn_save;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_return_stmt_failure__hard;
        );

        stmt->tag = STMT_RETURN;
        return STBOOL_SUCCESS;

prod_return_stmt_failure__hard:
        if (NULL != stmt->rtn_expr)
        {
            expr_fini(stmt->rtn_expr);
            nfree(stmt->rtn_expr);
        }
        return STBOOL_FAILURE;

prod_return_stmt_failure__soft:
        /*label statement*/;
    }

    // <import> ';'
    {
        if (
            *next = save,
            !match_terminal(next, TOKEN_KEYWORD_IMPORT)
        )
        {
            goto prod_import_failure__soft;
        }
        save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_LITERAL_STRING),
            save,
            STR_EXPECTED(STR_LITERAL_STR),
            goto prod_import_failure__hard;
        );
        struct token* save_import_string = save;
        save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_import_failure__hard;
        );
        nstr_init(&stmt->import_string);
        parse_n_astr_literal(save_import_string->start, &stmt->import_string);
        stmt->tag = STMT_IMPORT;
        return STBOOL_SUCCESS;

prod_import_failure__hard:
        return STBOOL_FAILURE;

prod_import_failure__soft:
        /*label statement*/;
    }

    // <alias> ';'
    {
        struct alias alias;
        alias_init(&alias);

        *next = save;
        int const status = prod_alias(&alias, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_alias_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_alias_failure__hard;
        }
        save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_alias_failure__hard;
        );

        stmt->tag = STMT_ALIAS;
        stmt->alias = nalloc(sizeof(struct alias));
        *stmt->alias = alias;
        return STBOOL_SUCCESS;

prod_alias_failure__hard:
        alias_fini(&alias);
        return STBOOL_FAILURE;

prod_alias_failure__soft:
        alias_fini(&alias);
    }

    // <expr> ';'
    {
        struct expr expr;
        expr_init(&expr);
        if (
            *next = save,
            !prod_expr(&expr, next)
        )
        {
            goto prod_expr_failure__soft;
        }
        save = *next;
        MATCH_EXPECTED(
            match_terminal(next, TOKEN_OPERATOR_SEMICOLON),
            save,
            STR_EXPECTED(STR_SEMICOLON),
            goto prod_expr_failure__hard;
        );

        stmt->tag = STMT_EXPR;
        stmt->expr = nalloc(sizeof(struct expr));
        *stmt->expr = expr;
        return STBOOL_SUCCESS;

prod_expr_failure__hard:
        expr_fini(&expr);
        return STBOOL_FAILURE;

prod_expr_failure__soft:
        expr_fini(&expr);
    }

    // <scope>
    {
        struct scope scope;
        scope_init(&scope, UNDEFINED_SCOPE_PARENT);
        *next = save;
        int const status = prod_scope(&scope, next);
        if (PROD_FAILURE_SOFT == status)
        {
            goto prod_scope_failure__soft;
        }
        else if (PROD_FAILURE_HARD == status)
        {
            goto prod_scope_failure__hard;
        }

        stmt->tag = STMT_SCOPE;
        stmt->scope = nalloc(sizeof(struct scope));
        *stmt->scope = scope;
        return STBOOL_SUCCESS;

prod_scope_failure__hard:
        scope_fini(&scope);
        return STBOOL_FAILURE;

prod_scope_failure__soft:
        scope_fini(&scope);
    }

    return STBOOL_FAILURE;
}

stbool proc_stmt(struct stmt* stmt, struct processing_context* ctx)
{
    switch (stmt->tag)
    {
    case STMT_EMPTY:
        /* nothing */
        break;

    case STMT_VARIABLE_DECLARATION:
        {
            if (!proc_var_decl(stmt->var_decl, true, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    case STMT_FUNCTION_DECLARATION:
        {
            if (!proc_func_decl(stmt->func_decl, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    case STMT_STRUCT_DECLARATION:
        {
            if (!proc_struct_decl(stmt->struct_decl, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    case STMT_IF_ELIF_ELSE:
        {
            if (scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_CONDITIONAL_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            for (
                size_t i = 0;
                i < narr_length(stmt->if_elif_else->if_elif_ind_cond);
                ++i
            )
            {
                if (!proc_expr(&stmt->if_elif_else->if_elif_ind_cond[i].condition, ctx))
                {
                    return STBOOL_FAILURE;
                }

                if (!preproc_scope(stmt->if_elif_else->if_elif_ind_cond[i].body, ctx))
                {
                    return STBOOL_FAILURE;
                }
                typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
                ctx->curr_scope = stmt->if_elif_else->if_elif_ind_cond[i].body;
                if (!proc_scope(stmt->if_elif_else->if_elif_ind_cond[i].body, ctx))
                {
                    return STBOOL_FAILURE;
                }
                ctx->curr_scope = save_curr_scope;
            }

            if (stmt->if_elif_else->else_body != NULL)
            {
                if (!preproc_scope(stmt->if_elif_else->else_body, ctx))
                {
                    return STBOOL_FAILURE;
                }
                typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
                ctx->curr_scope = stmt->if_elif_else->else_body;
                if (!proc_scope(stmt->if_elif_else->else_body, ctx))
                {
                    return STBOOL_FAILURE;
                }
                ctx->curr_scope = save_curr_scope;
            }

            return STBOOL_SUCCESS;
        }
        break;

    case STMT_LOOP:
        {
            if (!proc_loop(stmt->loop, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    case STMT_DEFER:
        {
            if (scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_DEFER_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            if (!preproc_scope(stmt->defer.scope, ctx))
            {
                return STBOOL_FAILURE;
            }
            typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
            ctx->curr_scope = stmt->defer.scope;
            ctx->curr_scope->is_within_defer = true;
            ctx->curr_scope->is_within_defer_primary = true;
            if (!proc_scope(stmt->defer.scope, ctx))
            {
                return STBOOL_FAILURE;
            }
            ctx->curr_scope = save_curr_scope;
            ctx->curr_scope->deferred_scopes =
                narr_push(ctx->curr_scope->deferred_scopes, &stmt->defer.scope);
            stmt->defer.idx = narr_length(ctx->curr_scope->deferred_scopes) - 1;
        }
        break;

    case STMT_BREAK:
        {
            if (!ctx->curr_scope->is_within_loop)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_BREAK_OUTSIDE_OF_LOOP
                );
                return STBOOL_FAILURE;
            }
            if (ctx->curr_scope->is_within_defer)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_BREAK_WITHIN_DEFERRED_SCOPE
                );
                return STBOOL_FAILURE;
            }
            return STBOOL_SUCCESS;
        }
        break;

    case STMT_CONTINUE:
        {
            if (!ctx->curr_scope->is_within_loop)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_CONTINUE_OUTSIDE_OF_LOOP
                );
                return STBOOL_FAILURE;
            }
            if (ctx->curr_scope->is_within_defer)
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_CONTINUE_WITHIN_DEFERRED_SCOPE
                );
                return STBOOL_FAILURE;
            }
            return STBOOL_SUCCESS;
        }
        break;

    case STMT_RETURN:
        {
            if (NULL == ctx->curr_func)
            {
                nassert(scope_is_global(ctx->curr_scope));
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    FMT_STMT_RETURN_NOT_WITHIN_FUNCTION
                );
                return STBOOL_FAILURE;
            }

            if (NULL == stmt->rtn_expr)
            {
                // Return statement without an expression should only be valid
                // if the return type of the current function is void.
                if (DT_VOID != ctx->curr_func->rtn_dt.tag)
                {
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        stmt->srctok,
                        FMT_STMT_RETURN_LACKS_EXPR
                    );
                    return STBOOL_FAILURE;
                }
            }
            else
            {
                if (!proc_expr(stmt->rtn_expr, ctx))
                {
                    return STBOOL_FAILURE;
                }
                // The type of the return expression must be convertible to the
                // return type of the function.
                if (
                    !dt_can_implicitly_convert(
                        &stmt->rtn_expr->dt,
                        &ctx->curr_func->rtn_dt
                    )
                )
                {
                    DECLARE_DT_NSTR(rtn_expr_type_nstr, &stmt->rtn_expr->dt);
                    DECLARE_DT_NSTR(func_rtn_type_nstr, &ctx->curr_func->rtn_dt);
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        stmt->srctok,
                        FMT_CANNOT_IMPLICITLY_CONVERT_RTN_TYPE,
                        rtn_expr_type_nstr.data,
                        func_rtn_type_nstr.data
                    );
                    return STBOOL_FAILURE;
                }
            }
        }
        break;

    case STMT_EXPR:
        {
            if (scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_STANDALONE_EXPR_STMT_NOT_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            if (!proc_expr(stmt->expr, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    case STMT_SCOPE:
        {
            if (scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_STANDALONE_SCOPE_NOT_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            if (!preproc_scope(stmt->scope, ctx))
            {
                return STBOOL_FAILURE;
            }
            typeof(ctx->curr_scope) save_curr_scope = ctx->curr_scope;
            ctx->curr_scope = stmt->scope;
            if (!proc_scope(stmt->scope, ctx))
            {
                return STBOOL_FAILURE;
            }
            ctx->curr_scope = save_curr_scope;
        }
        break;

    case STMT_IMPORT:
        {
            if (!scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_IMPORT_ONLY_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            nstr_t nstr_defer_fini import_path;
            nstr_init_nstr(&import_path, &stmt->import_string);
            if (!resolve_module_path(&import_path, ctx->module->src_path.data))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    FMT_FAILED_RESOLVE_PATH,
                    stmt->import_string.data
                );
                return STBOOL_FAILURE;
            }

            // Index of the imported module.
            // If the module exists in the module cache already this variable
            // will eventually get set to that variable.
            // If the module does not exist in the module cache, this variable
            // will be set to the index of the next open slot in the module
            // cache.
            ssize_t import_idx = -1;

            // Check if module is already in the cache.
            bool module_already_in_cache = false;
            nlogf(
                LOG_DEBUG,
                "Checking if module '%s' has already been compiled.",
                import_path.data
            );
            for (size_t i = 0; i < module_cache_len; ++i)
            {
                nlogf(
                    LOG_DEBUG,
                    INDENT_S "Checking against module '%s'.",
                    module_cache[i].src_path.data
                );
                if (nstr_eq(&import_path, &module_cache[i].src_path))
                {
                    nlogf(
                        LOG_DEBUG,
                        "Module '%s' has already been compiled.",
                        import_path.data
                    );
                    module_already_in_cache = true;
                    import_idx = i;
                    break;
                }
            }

            if (!module_already_in_cache)
            {
                nlogf(
                    LOG_DEBUG,
                    "Module '%s' has not already been compiled. Compiling now.",
                    import_path.data
                );
                size_t const save_current_module_idx = current_module_idx;
                import_idx = module_cache_len;
                current_module_idx = import_idx;
                struct module imported_module;
                module_init(&imported_module);
                int const compile_result =
                    compile(import_path.data, &imported_module);
                if (COMPILE_SUCCESS != compile_result)
                {
                    NLOGF_FLC_TOK(
                        LOG_ERROR,
                        stmt->srctok,
                        STR_IMPORT_FAILED " Return status (%d).",
                        compile_result
                    );
                    return STBOOL_FAILURE;
                }
                // Restore current module index.
                current_module_idx = save_current_module_idx;
            }
            nassert(import_idx >= 0); /* FIXME: could be -1 */
            struct module* const compiled_import = &module_cache[import_idx];

            // Add the module's exports to both this scope's symbol table as
            // well as it's exports.
            nlogf(
                LOG_DEBUG,
                "Importing %zu symbols from module '%s' into module '%s'.",
                narr_length(compiled_import->exports),
                compiled_import->src_path.data,
                ctx->module->src_path.data
            );
            size_t const exports_len = narr_length(compiled_import->exports);
            for (size_t i = 0; i < exports_len; ++i)
            {
                struct id* const symbol_id = &compiled_import->exports[i];
                struct symbol* export = symbol_table_find(
                    compiled_import->global_scope.symbol_table,
                    symbol_id->start,
                    symbol_id->length
                );
                nassert(NULL != export);
                nlogf(
                    LOG_DEBUG,
                    INDENT_S "Importing symbol '%.*s'.",
                    (int)export->id->length,
                    export->id->start
                );

                struct symbol* existing_symbol =
                    symbol_table_find(ctx->curr_scope->symbol_table, export->id->start, export->id->length);
                if (NULL != existing_symbol)
                {
                    nassert(NULL != export->srctok);
                    nassert(NULL != existing_symbol->srctok);
                    if (
                        nstr_eq(
                            &export->srctok->module->src_path,
                            &existing_symbol->srctok->module->src_path
                        )
                    )
                    {
                        nlogf(
                            LOG_DEBUG,
                            INDENT_S "Symbol '%.*s' already imported.",
                            (int)export->id->length,
                            export->id->start
                        );
                        continue;
                    }

                    log_redeclaration_of_defined_identifier(existing_symbol, export->id);
                    return STBOOL_FAILURE;
                }

                // Symbol table.
                struct symbol* symbol =
                    symbol_table_insert(ctx->curr_scope->symbol_table, export);
                nassert(NULL != symbol); SUPPRESS_UNUSED(symbol);

                // Module exports.
                nassert(ctx->curr_scope == &ctx->module->global_scope);
                module_add_latest_global_symbol_to_exports(ctx->module);
            }
        }
        break;

    case STMT_ALIAS:
        {
            if (!scope_is_global(ctx->curr_scope))
            {
                NLOGF_FLC_TOK(
                    LOG_ERROR,
                    stmt->srctok,
                    STR_ALIAS_ONLY_ALLOWED_AT_GLOBAL_SCOPE
                );
                return STBOOL_FAILURE;
            }

            if (!proc_alias(stmt->alias, ctx))
            {
                return STBOOL_FAILURE;
            }
        }
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      SCOPE                                                                 //
//============================================================================//
int prod_scope(struct scope* scope, struct token** next)
{
    struct token* save = *next;
    scope->srctok = save;

    // Parse opening curly '{'
    if (!scope_is_global(scope) && !match_terminal(next, TOKEN_OPERATOR_LEFTBRACE))
    {
        return PROD_FAILURE_SOFT;
    }

    // Parse statements within the scope.
    // Since any number of statements can exist within a scope, we have to check
    // the next token before each statement is produced to see if it is viable
    // for statement production, that way if the statement production does
    // fail then we know it was for a legitimate reason and not just because
    // the next sequence of tokens were syntactically trying to exit the scope.
    LOOP_FOREVER
    {
        if (
            (scope_is_global(scope) && (*next)->tag == TOKEN_EOF)
            ||
            (!scope_is_global(scope) && (*next)->tag == TOKEN_OPERATOR_RIGHTBRACE)
        )
        {
            // Scope has parsed through all statements successfully. Break out
            // of the statement parsing portion of the scope.
            break;
        }
        struct stmt stmt;
        stmt_init(&stmt);
        // The next token indicates that further statements can be produced
        // within this scope.
        if (!prod_stmt(&stmt, next))
        {
            stmt_fini(&stmt);
            return PROD_FAILURE_HARD;
        }
        scope->stmts = narr_push(scope->stmts, &stmt);
    }

    // Parse closing curly '}'
    if (!scope_is_global(scope) && !match_terminal(next, TOKEN_OPERATOR_RIGHTBRACE))
    {
        NLOGF_FLC_TOK(
            LOG_ERROR,
            save,
            STR_EXPECTED(STR_CURLY_BRACKET_R)
        );
        return PROD_FAILURE_HARD;
    }

    return PROD_SUCCESS;
}

stbool preproc_scope(
    struct scope* scope,
    struct processing_context* ctx
)
{
    /**
     * FIXME: This is a bug; accessing member of ctx->curr_scope
     * is illegal. It occurs when ctx is initialized with the
     * 'processing_context_init()' routine and is not modified
     * between then and its intended usage here.
     */

    if (!ctx->curr_scope) { return STBOOL_SUCCESS; }

    /**
     * FIXME: In re: to the above, this check should be enabled
     * after the previous issue is fixed.
     */

    /*nassert(NULL != ctx->curr_scope);*/

    scope->parent = ctx->curr_scope;
    scope->is_within_defer =
        scope_is_global(scope) ? false : scope->parent->is_within_defer;
    scope->is_within_loop =
        scope_is_global(scope) ? false : scope->parent->is_within_loop;
    return STBOOL_SUCCESS;
}

stbool proc_scope(struct scope* scope, struct processing_context* ctx)
{
    scope->uid = module_generate_uid(&module_cache[current_module_idx]);
    for (size_t i = 0; i < narr_length(scope->stmts); ++i)
    {
        if (!proc_stmt(&scope->stmts[i], ctx))
        {
            return STBOOL_FAILURE;
        }
    }
    return STBOOL_SUCCESS;
}

stbool dophase_syntax_analysis(struct module* module)
{
    struct token* next = module->tokens;
    return PROD_SUCCESS == prod_scope(&module->global_scope, &next);
}

stbool dophase_semantic_analysis(struct module* module)
{
    struct processing_context ATTR_DEFER_FINI(processing_context_fini) ctx;
    processing_context_init(&ctx, module);

    if (!preproc_scope(&module->global_scope, &ctx))
    {
        return STBOOL_FAILURE;
    }

    ctx.curr_scope = &module->global_scope;

    if (!proc_scope(&module->global_scope, &ctx))
    {
        return STBOOL_FAILURE;
    }


    processing_context_fini(&ctx);

    return STBOOL_SUCCESS;
}
