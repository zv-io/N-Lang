/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/ndict.h>

uint32_t const dict_primes[DICT_PRIMES_LENGTH] = {
/*  prime      // splits 2^n and 2^(n-1) for n == ? */
/*  =============================================== */
    53,        // 6
    97,        // 7
    193,       // 8
    389,       // 9
    769,       // 10
    1543,      // 11
    3079,      // 12
    6151,      // 13
    12289,     // 14
    24593,     // 15
    49157,     // 16
    98317,     // 17
    196613,    // 18
    393241,    // 19
    786433,    // 20
    1572869,   // 21
    3145739,   // 22
    6291469,   // 23
    12582917,  // 24
    25165843,  // 25
    50331653,  // 26
    100663319, // 27
    201326611, // 28
    402653189, // 29
    805306457, // 30
    1610612741 // 31
};

double dict_slot_load_factor(uint32_t slots_in_use, uint32_t slots_total)
{
    return ((double)slots_in_use)/slots_total;
}

#define HASH_CHAR_SLICE_MAGIC_PRIME_START 7
#define HASH_CHAR_SLICE_MAGIC_PRIME_MULT 101
uint32_t hash_char_slice(char const* start, size_t length)
{
    uint32_t hash = HASH_CHAR_SLICE_MAGIC_PRIME_START;
    while (length--)
    {
DIAGNOSTIC_PUSH
DIAGNOSTIC_IGNORE("-Wsign-conversion")
        hash = (hash * HASH_CHAR_SLICE_MAGIC_PRIME_MULT) + (uint32_t)(*start++);
DIAGNOSTIC_POP
    }
    return hash;
}
